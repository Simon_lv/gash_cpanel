<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="org.springside.modules.security.springsecurity.SpringSecurityUtils" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ include file="/common/taglibs.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/chili-1.7.pack.js"></script>
<script type="text/javascript" src="js/jquery.easing.js"></script>
<script type="text/javascript" src="js/jquery.dimensions.js"></script>
<script type="text/javascript" src="js/jquery.accordion.js"></script>
<script language="javascript">
    jQuery().ready(function(){  
    	$("ul li ul").each(function( index ) {
    		  if (0 == $( this ).text().trim().length) {
    			  $( this ).parent().hide();
    		  }
    	});
    	
/*        jQuery('#navigation').accordion({
            header: '.head',
            navigation1: true, 
            event: 'click',
            fillSpace: true,
            animated: 'bounceslide'
        });  */
    });
    //用於選中高亮
    var temp = null;
	function highlight(id) {
		if(temp == null) {
			var link = document.getElementById(id);
			link.className="active";
			temp = id;
			return true;
		}
		if(temp.trim() == id) {
			var link = document.getElementById(id);
			link.className="active";
		} else {
			var link = document.getElementById(id);
			link.className="active";
			var linkOld = document.getElementById(temp);
			linkOld.className="";
		}
		
		temp = id;
	}
</script>
<style type="text/css">
<!--
body {
    margin:0px;
    padding:0px;
    font-size: 12px;
}
#navigation {
    margin:0px;
    padding:0px;
    width:147px;
}
#navigation a.head {
    cursor:pointer;
    background:url(images/main_34.gif) no-repeat scroll;
    display:block;
    font-weight:bold;
    margin:0px;
    padding:5px 0 5px;
    text-align:center;
    font-size:12px;
    text-decoration:none;
}
#navigation ul {
    border-width:0px;
    margin:0px;
    padding:0px;
    text-indent:0px;
}
#navigation li {
    list-style:none; display:inline;
}
#navigation li li a {
    display:block;
    font-size:12px;
    text-decoration: none;
    text-align:center;
    padding:3px;
}
#navigation li li a:hover {
    background:url(images/tab_bg.gif) repeat-x;
        border:solid 1px #adb9c2;
}
.active {
	background:url(images/tab_bg.gif) repeat-x;
/*	background:rgb(226,242,250) repeat-x;*/
	border:solid 1px #adb9c2;
}
-->
</style>
</head>



<%
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date now = new Date(System.currentTimeMillis());
	String startDate = simpleDateFormat.format(now);
	Calendar   calendar   =   new   GregorianCalendar(); 
	calendar.setTime(now);    
	calendar.add(Calendar.DAY_OF_MONTH, 1);
	String endDate = simpleDateFormat.format(calendar.getTime());

%>
<body>
<div  style="height:100%;">
  <ul id="navigation">  
    <li><a class="head">GASH點卡</a>
      <ul>
      	   <security:authorize ifAnyGranted="/driver/driver!list.action">
          		<li><a href="${ctx}/driver/driver!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="1">司機管理</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/banner/banner!list.action">
          		<li><a href="${ctx}/banner/banner!list.action" target="rightFrame" onclick="highlight(id)" id="2">Banner管理</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/payment/payment!list.action">
         		 <li><a href="${ctx}/payment/payment!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="3">購買紀錄</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/payment/payment!listThird.action">
         		 <li><a href="${ctx}/payment/payment!listThird.action?reset=true" target="rightFrame" onclick="highlight(id)" id="8">購買紀錄</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/invoice/invoice!list.action">
          		<li><a href="${ctx}/invoice/invoice!list.action" target="rightFrame" onclick="highlight(id)" id="7">發票號碼</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/restore/restore!list.action">
          		<li><a href="${ctx}/restore/restore!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="4">回存紀錄</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/card/card!list.action">
          		<li><a href="${ctx}/card/card!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="5">點卡數量管理</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/smsrecord/smsrecord!list.action">
          		<li><a href="${ctx}/smsrecord/smsrecord!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="6">簡訊發送明細表</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/gamegift/gamegift!list.action">
                <li><a href="${ctx}/gamegift/gamegift!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="12">活動虛寶發送</a></li>
           </security:authorize>
           <security:authorize ifAnyGranted="/gamegift/gamegift!manage.action">
                <li><a href="${ctx}/gamegift/gamegift!manage.action" target="rightFrame" onclick="highlight(id)" id="20">活動虛寶管理</a></li>
           </security:authorize>			
		   <security:authorize ifAnyGranted="/smsbackrecord/smsbackrecord!list.action">
		   		<li><a href="${ctx}/smsbackrecord/smsbackrecord!list.action" target="rightFrame" onclick="highlight(id)" id="13">司機回覆明細</a></li>
		   </security:authorize>	
		   <security:authorize ifAnyGranted="${ctx}/soldpoint/soldpoint!list.action">
          		<li><a href="${ctx}/soldpoint/soldpoint!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="22">Gash點卡申請</a></li>
           </security:authorize>	   
      </ul>
    </li>
 	<li><a class="head">到府收款</a>
      <ul>
      <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!invoice.action">
          		<li><a href="${ctx}/tccpaypayment/tccpaypayment!invoice.action" target="rightFrame" onclick="highlight(id)" id="7">發票號碼</a></li>
           </security:authorize>
      	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!list.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="10">購買紀錄</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面  gash -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listGash.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listGash.action?reset=true" target="rightFrame" onclick="highlight(id)" id="14">購買紀錄（GASH）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 大車隊-->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listTcc.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listTcc.action?reset=true" target="rightFrame" onclick="highlight(id)" id="15">購買紀錄（大車隊）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 CTRL Media -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listCM.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listCM.action?reset=true" target="rightFrame" onclick="highlight(id)" id="16">購買紀錄（CTRL Media）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 p2g -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listP2G.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listP2G.action?reset=true" target="rightFrame" onclick="highlight(id)" id="17">購買紀錄（完美對子）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 賞金獵人 -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listBonushunter.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listBonushunter.action?reset=true" target="rightFrame" onclick="highlight(id)" id="18">購買紀錄（賞金獵人）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 RC -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listRC.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listRC.action?reset=true" target="rightFrame" onclick="highlight(id)" id="19">購買紀錄（RC）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 SNSPlus -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listSnsplus.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listSnsplus.action?reset=true" target="rightFrame" onclick="highlight(id)" id="20">購買紀錄（SNSPlus）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 牛蕃茄 -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listTomato.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listTomato.action?reset=true" target="rightFrame" onclick="highlight(id)" id="21">購買紀錄（牛蕃茄）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 隆中 -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listLz.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listLz.action?reset=true" target="rightFrame" onclick="highlight(id)" id="21">購買紀錄（ 隆中）</a></li>
     	  </security:authorize>
     	  <!-- 合作廠商界面 博雅 -->
     	  <security:authorize ifAnyGranted="/tccpaypayment/tccpaypayment!listBoyaa.action">
      	  		<li><a href="${ctx}/tccpaypayment/tccpaypayment!listBoyaa.action?reset=true" target="rightFrame" onclick="highlight(id)" id="21">購買紀錄（ 博雅）</a></li>
     	  </security:authorize>
     	  <security:authorize ifAnyGranted="/tccpaydriver/tccpaydriver!list.action">
          		<li><a href="${ctx}/tccpaydriver/tccpaydriver!list.action?reset=true" target="rightFrame" onclick="highlight(id)" id="11">司機管理</a></li>
      	  </security:authorize>
      </ul>
    </li>
    <li> <a class="head" >系統管理</a>
      <ul>
      <security:authorize ifAnyGranted="/position/position!list.action">
          <li><a href="${ctx}/position/position!list.action"  target="rightFrame" onclick="highlight(id)" id="8">權限管理</a></li>
          </security:authorize>
          <security:authorize ifAnyGranted="/admin/admin!list.action">
          <li><a href="${ctx}/admin/admin!list.action"  target="rightFrame" onclick="highlight(id)" id="9">帳號管理</a></li>
          </security:authorize>
          <security:authorize ifAnyGranted="/authorities/authorities!list.action">
          <li><a href="${ctx}/authorities/authorities!list.action"  target="rightFrame" onclick="highlight(id)" id="10">菜單管理</a></li>
          </security:authorize>
       
      </ul>
    </li>
  </ul>
</div>
</body>
</html>
