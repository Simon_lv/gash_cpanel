<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@ page import="org.springside.modules.security.springsecurity.SpringSecurityUtils" %>
<%@ include file="/common/taglibs.jsp"%>
<div id="menu">
<div>
<%
	//if ("lam".equals(SpringSecurityUtils.getCurrentUserName()) || "wilson".equals(SpringSecurityUtils.getCurrentUserName()))
	//{
%>
<security:authorize ifAnyGranted="A_MODIFY_USER">
	<a href="${ctx}/user/user.action">後臺帳號列表</a>&nbsp;
	<a href="${ctx}/user/user_invalid.action">後臺停權帳號列表</a>&nbsp;
	<a href="${ctx}/user/role.action">角色列表</a>&nbsp;
</security:authorize>
<%
	//}
%>
您好,<%=SpringSecurityUtils.getCurrentUserName()%>.&nbsp;
	<a href="${ctx}/j_spring_security_logout">登出</a>&nbsp;
	<a href="${ctx}/user/passwd.action">修改密碼</a>
</div>
</div>
