<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  司機回覆明細</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/sisyphus.min.js"></script>

<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	$(function(){
		$("#search").click(function(){
			$("form[name='search']").attr('action','${ctx}/smsbackrecord/smsbackrecord!list.action');
			var mobileNo = $("input[name='mobileNo']").val();
			var startTime = $("input[name='startTime']").val();
			var endTime = $("input[name='endTime']").val();
			var memberId = $("#memberId option:selected").val();
			/*
			if($.trim(mobileNo)=="" && $.trim(startTime)=="" && $.trim(endTime)=="") {
				alert("請輸入查詢條件");
				return false;
			}
			*/
			document.forms[0].submit();
		});
	});
	$( function() { 
        $("#exportExcel").click(function(){
            $("form[name='search']").attr('action','${ctx}/smsbackrecord/smsbackrecord!export.action');
            var mobileNo = $("input[name='mobileNo']").val();
            var startTime = $("input[name='startTime']").val();
            var endTime = $("input[name='endTime']").val();
            var memberId = $("#memberId option:selected").val();
            document.forms[0].submit();
        });
        $( "form" ).sisyphus(); 
    } );
    <c:if test="${not empty map.reset}"> 
        $( function() { 
            $( "form" ).sisyphus().manuallyReleaseData();
            $("form[name='search']")[0].reset();
            //alert("reset");
        } );
    </c:if>
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">
<form action="${ctx}/smsbackrecord/smsbackrecord!list.action" name="search" method="post">
   
    <div class="itemDiv" > 手機號碼：<input type="text" name="mobileNo" value="${map['mobileNo']}" /></div>
    
    <div class="itemDiv">
	
	時間：<input type="text" id="startTime" name="startTime" value="${map['startTime']}" size="20" style="vertical-align:middle"/>
	<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" id="trigger1" 
																						style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																						title="選擇日期" onmouseover="this.style.background='red';"
																						onmouseout="this.style.background=''" align="middle"
																						onmousemove="javascript:Calendar.setup({inputField	: 'startTime',ifFormat: '%Y-%m-%d 00:00:00',button: 'trigger1' });" /> ～
	<input type="text" id="endTime" name="endTime" value="${map['endTime']}" size="20" style="vertical-align:middle"/>
	<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" name="trigger2" align="middle" id="trigger2"
																						style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																						title="選擇日期" onmouseover="this.style.background='red';"
																						onmousemove="javascript:Calendar.setup({inputField	: 'endTime',ifFormat: '%Y-%m-%d 23:59:59',button: 'trigger2' });"
																						onmouseout="this.style.background=''"/>&nbsp;&nbsp;&nbsp;&nbsp;時間格式：2010-01-15 08:00:00   &nbsp;&nbsp;&nbsp;&nbsp;<!-- span style="color:red;font-weight:bold">溫馨提示：查詢時間不能超過35天！</span-->
	</div>
    
    <div class="itemDiv">
	  <input type="button"  value="搜尋" id="search"/> &nbsp;&nbsp;
	  <input type="button"  value="匯出EXCEL" id="exportExcel"/>
	</div>
	
</form>
</div> 
<h3>&nbsp;&nbsp;司機回覆明細</h3>

<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>帳務編號</td>
		<td>手機號碼</td>
		<td>簡訊內容</td>
		<td>訂單編號</td>
		<td>時間</td>
		
	</tr>
	<s:iterator value="page.result">	
		<tr align="center" valign="middle">
			<td>${messageId}</td>
			<td>${mobileNo}</td>
			<td>${smsMessage}</td>
			<td>${orderIds}</td>
			<td>${createTime}</td>
		</tr>
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/smsbackrecord/smsbackrecord!list.action?page.pageNo=${page.prePage}&mobileNo=${map['mobileNo']}&startTime=${map['startTime']}&endTime=${map['endTime']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/smsbackrecord/smsbackrecord!list.action?page.pageNo=${page.nextPage}&mobileNo=${map['mobileNo']}&startTime=${map['startTime']}&endTime=${map['endTime']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div>
</div>
</div>
</div>
</body>
</html>
