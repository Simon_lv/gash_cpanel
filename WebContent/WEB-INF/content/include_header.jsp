<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>

<script language="javascript" type="text/javascript" src="<%=request.getContextPath() %>/js/util.js"></script>
<script>
function page(pageValue){
	var pageN = document.getElementById("pageNo");
	pageN.value = pageValue;
	document.search.submit();
}
function sort(orderBy,order){
	document.getElementById("orderBy").value = orderBy;
	document.getElementById("order").value = order;
	document.search.submit();
}
</script>
