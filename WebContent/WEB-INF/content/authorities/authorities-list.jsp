<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技管理系統 菜單管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css"rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript"src="${ctx}/js/jquery.js"></script>
	<script type="text/javascript">
	function doChange(type,id){
		  if(type=="" || id==""){
			  alert("操作錯誤！");
		  }else{
			  if(window.confirm("是否確定刪除？")){
				  $.ajax({ 
				   		url: "${ctx}/authorities/authorities!operation.action",
				   		type: "POST",
				   		data:"type="+type+"&id="+id,
				   		success: function(data){	
				   		   if(!!data){
				   				window.location.href = "${ctx}/authorities/authorities!list.action";
				   		   }else{
				   			   alert("發生錯誤!");
				   		   }
					    },
					    error:function(){
					  	  alert("發生錯誤，請聯繫系管理員！");
					    }   
					});	
			  }
		  }	
	}
	
	
	
		  
		   function newAuthorities(){
			   window.location.href = "${ctx}/authorities/authorities!toAdd.action";
		   }
    </script>

<style>
	.itemDiv{height:30px;line-height:30px;}
</style>
</head>

<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="message"><s:actionmessage theme="mytheme"/></div>
<div id="filter">
<br/>
<h3>功能管理</h3>
</div> 
<input type="button" value="新增菜單" onclick="newAuthorities()"/>

<table width="80%">
	<tr>
		<td>菜單ID</td>
		<td>URI鏈接</td>
		<td>顯示名稱</td>
		<td colspan="2">管理</td>
	</tr>

	
	    <c:forEach items="${listParent}" var="pa">
		     <tr>
		        <c:set var="key" value="${pa.id}"/>
		     	<td colspan="3" style="background: #ADD2DA">${pa.displayName}</td>
		     	<td><a href="${ctx}/authorities/authorities!edit.action?id=${pa.id}">修改</a></td>
			     <td><a href="#" onclick="doChange(4,'${pa.id}')" >刪除</a></td>
		     </tr>
		     <c:forEach items="${authoritDisplayMap[key]}" var="authorities">
		     	<tr>
			     	<td>${authorities.id}</td>
			     	<td>${authorities.name}</td>
			     	<td>${authorities.displayName}</td>
			     	<td><a href="${ctx}/authorities/authorities!edit.action?id=${authorities.id}">修改</a></td>
			     	<td><a href="#" onclick="doChange(4,'${authorities.id}')" >刪除</a></td>
		     	</tr>
		     </c:forEach>

	    </c:forEach>
</table>
</div>
</div>
<jsp:include page="../include_footer.jsp"/>
</div>
</body>
</html>
