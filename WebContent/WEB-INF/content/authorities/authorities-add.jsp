<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>伊凡達科技管理系統 新增菜單</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$("#add").click(function(){
				var name = $("input[name='name']").val();
				var displayName = $("input[name='displayName']").val();
				if($.trim(name) == '') {
					alert("請填寫URI鏈接，模組請填寫【/parent】");
					return;
				}
				if($.trim(displayName) == '') {
					alert("請填寫顯示名稱");
					return;
				}
				$("form:first").submit();
			});
		});
	</script>
</head>

<body>
<div id="container">
<div id="myContent">
<h3>新增菜單</h3>
<div id="inputContent">
<form id="checkFrom" action="/authorities/authorities!save.action" method="post">

<table width="40%">
	<tr>
		<td>所屬模組:</td>
		<td>
			<select name="pid">
				<option value="">--請選擇--</option>
				<c:forEach items="${parentList}" var="pa">
					<option value="${pa.id}">${pa.displayName}</option>
				</c:forEach>
			</select>
		</td>
	</tr>
	<tr>
		<td>URI鏈接</td>
		<td><input type="text" name="name"/><font color="red">必填，模組請填寫【/parent】</font></td>
	</tr>
	<tr>
		<td>顯示名稱</td>
		<td><input type="text" name="displayName"/><font color="red">必填</font></td>
	</tr>
	<tr>
		<td colspan="2">
            <input type="button" value="取消" onclick="javascript:history.go(-1);" /> &nbsp;&nbsp;
			<input type="button" value="送出" id="add"/>
		</td>
	</tr>
</table>
</form>

</div>
</div>

</div>
</body>
</html>