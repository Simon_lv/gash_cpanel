<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="org.springside.modules.security.springsecurity.SpringSecurityUtils" %>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
<style>
.itemDiv{height:30px;line-height:30px;}
</style>
<script type="text/javascript">
    /*submit form*/
	function Checkform(){
		var pwd1 = $("#pwd1").val();
		var pwd2 = $("#pwd2").val();
		
		if(pwd1.length<6){
			alert("密碼長度必須大於6位");
			return false;
		}
		
		if(pwd1!=pwd2){
			alert("你兩次輸入的密碼不一致!");
			return false;
		}
		
		$.ajax({ 
	   		url: "/admin/admin!changePassword.action", 
	   		dataType:"json",
	   		data:"&password="+pwd1,
	   		success: function(data){
	   			if(data==1){
	   				alert("修改成功！");
	   			  window.location.href = "${ctx}/index/index.action";
	   				
	   			}else{
	   				alert("修改失敗！");
	   				
	   			}
      },
      error:function(){
    	  alert("程式發生錯誤，請聯系管理員！");
      }   
   });

	
 }
  </script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<h3>&nbsp;&nbsp;修改密碼</h3>
<form name="edit" method="post">
	<div class="itemDiv">&nbsp;&nbsp;&nbsp;&nbsp;新密碼：<input type="password" id="pwd1" name="new" size="25" /> 
			<br/>重複新密碼：<input type="password" id="pwd2" size="25" /> 
			<br/>
	<input type="button" onclick="Checkform()" value="保存修改" />&nbsp;&nbsp;
	<input type="button" onclick="" value="取消" />
   	<br/>
</div>
</form>
</div>
</div> 
</div>
</body>
</html>