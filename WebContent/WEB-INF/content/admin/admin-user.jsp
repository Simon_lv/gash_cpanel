<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="org.springside.modules.security.springsecurity.SpringSecurityUtils" %>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
<style>
.itemDiv{height:30px;line-height:30px;}
</style>

</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<h3>&nbsp;&nbsp;帳號基本信息</h3>
<form name="edit" action="${ctx}/admin/admin!editAdmin.action" method="post">
	<div class="itemDiv"><input type="hidden" id="id" name="id" value="${admin.id }" />&nbsp;&nbsp;登陸名：<span>${admin.username }</span>
		<br/>&nbsp;&nbsp;用戶名：<input type="text" id="name" name="name" size="25" value="${admin.name }"/> 
			<br/>&nbsp;&nbsp;Email：<input type="text" id=email name="email" size="25" value="${admin.email }"/> 
			<br/>
			創建時間：<span>${admin.create_time}</span><br/>
	<input type="submit" value="修改" />&nbsp;&nbsp;
   	<br/>
</div>
</form>
</div>
</div> 
</div>
<script>
    if(window.location.toString().indexOf("result") > 0) {
    	alert("修改成功!");
    }
</script>
</body>
</html>