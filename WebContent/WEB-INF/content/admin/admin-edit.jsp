<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/validate/jquery.validate.css"rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript"src="${ctx}/js/jquery.js"></script>
	<script language="javascript" type="text/javascript"src="${ctx}/js/validate/jquery.validate.js"></script>
	<script language="javascript" type="text/javascript"src="${ctx}/js/validate/messages_cn.js"></script>
	<script type="text/javascript">
	  $(document).ready(function(){
			$("#checkFrom").validate();
	  });  
	</script>
		
<style>
   .itemDiv{height:30px;line-height:30px;}
   .activation{display: block;}
   .inactivation{display: none;}
</style>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent"> 
<h3>&nbsp;&nbsp;編輯用戶</h3>
<form action="${ctx}/admin/admin!updateUserInfo.action" id="checkFrom">
   <input name="id" value="${u.id}" type="hidden"/>
   <input type="hidden" name="csNum" value="${u.csNum}" id="csNum" />
   <input type="hidden" name="username" value="${u.username}" id="username" />
<table width="98%">
     <tr>
        <td>職務權限</td>
        <td>
            <select name="positionId" id="conditionPositionId">
               <c:forEach items="${list}" var="p">
                  <option value="${p.id}">${p.name}</option>
               </c:forEach>
            </select>
            <script>
              var optionVal = '${u.positionId}';
              $("#conditionPositionId").val(optionVal);
            </script>
        </td>
     </tr>
     <tr>
    	<td>姓名</td>
    	<td><input type="text" name="name" value="${u.name}" id="name" class="required"/></td>
     </tr>
     <tr>
    	<td>帳號</td>
    	<td>${u.username}</td>
     </tr>
     <tr>
    	<td>密碼</td>
    	<td><input type="password" name="password" id="pwd" value="" />有輸入時才修改</td>
    </tr>
     <tr>
    	<td>Email</td>
    	<td><input type="text" name="email" value="${u.email}" class="email"/></td>
    </tr>

    <tr>
    	<td colspan="2">
          <input type="button" value="取消" onclick="javascript:history.go(-1);" />
          <input type="submit" value="確定" class="submit"/>
        </td>
    </tr>
</table>
</form>
</div>
</div>
    
</div>
</body>
</html>