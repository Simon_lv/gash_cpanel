<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/validate/jquery.validate.css"rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript"src="${ctx}/js/jquery.js"></script>
	<script language="javascript" type="text/javascript"src="${ctx}/js/validate/jquery.validate.js"></script>
	<script language="javascript" type="text/javascript"src="${ctx}/js/validate/messages_cn.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#checkFrom").validate();
		});
		
		function checkSameUser(){
		
			var login_name = $("#login").val();
			$.ajax({
				url:"/admin/admin!checkSameUser.action",
				dataType:"json",
				data:"login_name="+login_name,
				success:function(data){
					if(data>0){
						alert("该用户名已经存在！");
						$("#login").val("");
					}
				},
				error:function(){
					alert("發生錯誤，請聯系管理員！");
				}
			
			});
			
		}
		
	</script>
		
<style>
   .itemDiv{height:30px;line-height:30px;}
   .activation{display: block;}
   .inactivation{display: none;}
</style>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent"> 
<h3>&nbsp;&nbsp;新增用戶</h3>
<form action="${ctx}/admin/admin!saveUser.action" id="checkFrom">
<table width="98%">
     <tr>
         <td>職務權限</td>
         <td>
             <select name="positionId">
                 <c:forEach items="${list}" var="p">
                     <option value="${p.id}">${p.name}</option>
                 </c:forEach>
             </select>
         </td>
      </tr>
      <tr>
    	<td>姓名</td>
    	<td><input type="text" name="name" id="name"/></td>
      </tr>
      <tr>
    	<td>帳號</td>
    	<td><input type="text" name="login_name" id="login" onkeyup="value=value.replace(/[\W]/g,'')" onbeforepaste="clipboardData.setData('text',clipboardData.getData('text').replace(/[^\d]/g,''))" class="required" onblur="checkSameUser()"/></td>
    </tr>
     <tr>
    	<td>密碼</td>
    	<td><input type="password" name="password" id="pwd" class="required"/></td>
    </tr>
     <tr>
    	<td>Email</td>
    	<td><input type="text" name="email" class="email"/></td>
    </tr>
    <tr>
       <td colspan="2">
          <input type="button" value="取消" onclick="javascript:history.go(-1);" />
          <input type="submit" value="確定" class="submit"/>
       </td>
    </tr>
</table>
</form>
</div>
</div>
    
</div>
</body>
</html>