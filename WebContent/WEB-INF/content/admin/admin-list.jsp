<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css"
			rel="stylesheet" type="text/css" />
		<script language="javascript" type="text/javascript"
			src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
			<script language="javascript" type="text/javascript"
			src="${ctx}/js/jquery.js"></script>
		<script language="javascript" type="text/javascript"
			src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
		<script language="javascript" type="text/javascript"
			src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>

<style>
.itemDiv{height:30px;line-height:30px;}
.deleted{color: red;}
.active{color: black;}
</style>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
	<script>
	function deleteTask(id,status){
		   $.ajax({ 
			   		url: "/admin/admin!updateUserStatus.action", 
			   		dataType:"json",
			   		data:"id="+id+"&status="+status,
			   		success: function(data){
			   			if(data==2){
			   				alert("操作成功！");
			   				document.getElementById("del_"+id).innerHTML="<span id='del_"+id+"'><a href='javascript:deleteTask("+id+",1)'>啟用</a></span>";
			   			}else if(data==1){
			   				alert("操作成功！");
			   				document.getElementById("del_"+id).innerHTML="<span id='del_"+id+"'><a href='javascript:deleteTask("+id+",2)'>停權</a></span>";
			   			}else{
			   				alert("操作失敗！");
			   			}
			   			$('#searchFormButton').trigger('click');
		      },
		      error:function(){
		    	  alert("發生錯誤，請聯系管理員！");
		      }   
		   });
	}
	
	 function newItem(){
		   window.location.href = "${ctx}/admin/admin!addUser.action";
	   }
	</script>

<div id="mainContent">
<div id="myContent">
<div id="message"><s:actionmessage theme="mytheme"/></div>
<div id="filter">
<form action="${ctx}/admin/admin!list.action" name="search" method="post">
	<div class="itemDiv" >
      賬號：<input type="text" name="login_name" size="25" value="${map['login_name']}"/>&nbsp;&nbsp;
     <%--  姓名：<input type="text" name="uname" size="25" value="${map['uname']}"/>&nbsp;&nbsp;
      客服編號：<input type="text" name="cs_num" size="25" value="${map['cs_num'] }"/> --%>
    </div>
<div class="itemDiv">
   <input type="submit"  value="搜尋" id="searchFormButton"/> &nbsp;&nbsp;
   <input type="button" value="新增" onclick="newItem()"/>     
</div>
</form>
</div> 
<h3>&nbsp;&nbsp;賬號列表</h3>

<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>職務權限</td>
		<td>帳號</td>
		<td>姓名</td>
		<td>Email</td>
        <td>編輯</td>
		<td>管理</td>
	</tr>

	<s:iterator value="page.result">
		<tr align="center" valign="middle">
			<td>${positionName}</td>
			<td>${username}</td>
			<td>${name}</td>
			<td>${email}</td>
			<td>
			    <a href="${ctx}/admin/admin!edit.action?id=${id}">編輯</a>
            </td>
            <td>
 		        <c:if test="${status==1}"><span id="del_${id}"><a href="javascript:deleteTask('${id}',2)">停權</a></span></c:if>
		        <c:if test="${status==2}"><span id="del_${id}"><a href="javascript:deleteTask('${id}',1)">啟用</a></span></c:if>
			</td>
		</tr>
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/admin/admin!list.action?page.pageNo=${page.prePage}&login_name=${map['login_name']}&uname=${map['uname']}&cs_num=${map['cs_num']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/admin/admin!list.action?page.pageNo=${page.nextPage}&login_name=${map['login_name']}&uname=${map['uname']}&cs_num=${map['cs_num']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount} ,每頁${page.pageSize}
</div>
</div>
</div>
   
</div>
</body>
</html>
