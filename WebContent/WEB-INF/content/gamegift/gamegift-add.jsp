<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>&nbsp;&nbsp;活動虛寶管理</title>
<%@ include file="/common/meta.jsp"%>
<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
<script type="text/javascript">
$(function(){
	$("#release").click(function() {
		var name = $("#name").val();
		var description = $("#description").val();
		var imgUrl = $("#imgUrl").val();
		var deadline = $("#deadline").val();
		
		if($.trim(name)=='') {
			alert("請填寫活動名稱");
			$("input[name='name']").focus();
			return false;
		}
		
		if($.trim(description)=='') {
			alert("請填寫活動內容");
			$("input[name='description']").focus();
			return false;
		}
		
		if($.trim(imgUrl)=='') {
			alert("請填寫圖片檔名");
			$("input[name='imgUrl']").focus();
			return false;
		}
		
		if($.trim(deadline)=='') {
			alert("請填寫活動期限");
			$("input[name='deadline']").focus();
			return false;
		}
		
		document.forms[0].submit();
	});
	
	var price = "${gameGift.price}";
	
	if(price == "1") {
		$("#price").attr("checked", true);
	} 
});	
</script>	
<style>
.itemDiv {
	height: 30px;
	line-height: 30px;
}

.activation {
	display: block;
}

.inactivation {
	display: none;
}
</style>
</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">活動虛寶管理-新增</h3>
				<form action="${ctx}/gamegift/gamegift!save.action" method="post">				
					<table width="80%">
						<tr>
							<td>活動名稱：</td>
							<td>
								<input type="text" id="name" name="name" size="100" value="${gameGift.name}"/> <br>
								<input type="checkbox" id="price" name="price" value="1">面額區分</input>
							</td>
						</tr>
						<tr>
							<td>活動內容：</td>
							<td><textarea id="description" name="description" cols="100" rows="20" style="width:700px;height:300px;">${gameGift.description}</textarea></td>
						</tr>	
						<tr>
							<td>圖片檔名：</td>
							<td><input type="text" id="imgUrl" name="imgUrl" size="100" value="${gameGift.imgUrl}"/></td>
						</tr>	
						<tr>
							<td>活動期限：</td>
							<td>
								<input type="text" id="deadline" name="deadline" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${gameGift.deadline}"/>" size="18" style="vertical-align:middle"/>
								<img src="${ctx}/js/jscalendar-1.0/img.gif" name="trigger2" align="middle" id="trigger2"
									style="cursor: pointer; border: 1px solid red;vertical-align:middle"
									title="選擇日期" onmouseover="this.style.background='red';"
									onmousemove="javascript:Calendar.setup({inputField:'deadline',ifFormat:'%Y-%m-%d',button:'trigger2'});"
									onmouseout="this.style.background=''"/>
								&nbsp;&nbsp;&nbsp;&nbsp;時間格式：2010-01-15
							</td>
						</tr>						
						<tr>
							<td colspan="2">
								<input type="button" value="取消" onclick="javascript:history.go(-1);" />
								<input type="button" id="release" value="確定" />								
							</td>
						</tr>
					</table>
					<input type="hidden" id="id" name="id" value="${gameGift.id}"/>
				</form>
			</div>
		</div>
	</div>
</body>
</html>