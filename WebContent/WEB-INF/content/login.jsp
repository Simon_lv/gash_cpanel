<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="org.springframework.security.ui.AbstractProcessingFilter"%>
<%@ page import="org.springframework.security.ui.webapp.AuthenticationProcessingFilter"%>
<%@ page import="org.springframework.security.AuthenticationException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>傾國科技網站管理後台</title>
<link rel="stylesheet" type="text/css" href="${ctx}/css/style.css"/>
<script type="text/javascript" src="${ctx}/js/js.js"></script>
<script>
  		function form_submit()
  		{
  	  		var username = document.loginForm.username.value;
  	  		document.loginForm.j_username.value = username + "/pm";
  	  		var password = document.loginForm.j_password.value;
  	  	
	  		if (username == "")
  			{
  	  			alert("帳號不能為空！");
  	  			return false;
  			}
	  		if (password == "")
	  		{
	  	  		alert("密碼不能為空！");
	  	  		return false;
	  		}
  	  		//return true;
			document.loginForm.submit();
  		}
  	</script>
</head>
<body>
<div id="top"> </div>

<form name="loginForm" id="loginForm" action="${ctx}/j_spring_security_check" method="post" onsubmit="return formcheck();">
  <div id="center">
    <div id="center_left"></div>
    <div id="center_middle">
    <div class="user">
    <%
		if (session.getAttribute(AbstractProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY) != null) {
	%>
  <span style="color:red"> 登入失敗，請重試.</span>
  <%
		}
	%>
    </div>
      <div class="user">
        <label>帳    號：
        <input type="text" name="username" value="" />
					<input type="hidden" name="j_username" class="required"
					<s:if test="not empty param.error"> value='<%=session.getAttribute(AuthenticationProcessingFilter.SPRING_SECURITY_LAST_USERNAME_KEY)%>'</s:if>/>
        </label>
      </div>
      <div class="user">
        <label>密     碼：
        <input type='password' name='j_password' class="required" />
        </label>
      </div>
      
    </div>
    <div id="center_middle_right"></div>
    <div id="center_submit">
      <div class="button"> <img src="${ctx}/images/dl.gif" width="57" height="20" onclick="form_submit()" > </div>
      <!--div class="button"> <img src="${ctx}/images/cz.gif" width="57" height="20" onclick="form_reset()"> </div-->
    </div>
    <div id="center_right"></div>
  </div>
</form>
<div id="footer"></div>
</body>
</html>
