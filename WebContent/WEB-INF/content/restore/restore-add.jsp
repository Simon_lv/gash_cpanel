<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  回存紀錄</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>

<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	$(function(){
		$("#searchNeed").click(function(){
			var carId = $("input[name='carId']").val();
			if($.trim(carId) == "") {
				alert("请先輸入隊編");
				return false;
			}
			$("#searchForm").submit();
		});
	});
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<h3>&nbsp;&nbsp;回存紀錄-新增</h3>
<div id="filter">
<form action="${ctx}/restore/restore!toAdd.action" id="searchForm">
     <div class="itemDiv" > 
              隊編：<input type="text" name="carId" value="${map['carId'] }" />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <input type="button"  value="[確定查詢]" id="searchNeed"/> &nbsp;&nbsp;
     </div>
</form>
</div> 

<table width="98%">
	<c:if test="${not empty paymentList}">
	    <div class="itemDiv">應回存購買紀錄：</div>
	    <tr style="text-align: center;font-weight: bold;">
		        <td>訂單編號</td>
		        <td>訂單時間</td>
		        <td>面額</td>
		        <td>訂單狀態</td>
		</tr>
		
            <c:forEach items="${paymentList }" var="payment">
                <tr align="center" valign="middle">	
                    <td>${payment.orderId }</td>
                    <td>${payment.createTime }</td>
                    <td>${payment.price }</td>
                   <td>
						<c:if test="${payment.orderStatus==1}">用戶請求</c:if>
						<c:if test="${payment.orderStatus==2}">收到司機回覆</c:if>
						<c:if test="${payment.orderStatus==3}">發送點卡</c:if>
						<c:if test="${payment.orderStatus==4}">司機事實並未收款</c:if>
						<c:if test="${payment.orderStatus==5}">用戶未收到點卡簡訊</c:if>
						<c:if test="${payment.orderStatus==6}">用戶已收到點卡簡訊</c:if>
					</td>
                </tr>
            </c:forEach>
	</c:if>
</table>
<c:if test="${not empty none}">
      <div class="itemDiv">
		該隊編應已回存所有現金總額
	  </div>
</c:if>
<c:if test="${not empty paymentList}">
    <div class="itemDiv">
		&nbsp;&nbsp;應回存現金總額：${sum}
	</div>
    <div class="itemDiv">
        <form action="${ctx}/restore/restore!batchAddRestore.action">
            <input type="submit" value="[確定新增回存紀錄]" />
        </form>
    </div>
</c:if>
</div>
</div>
</div>
</body>
</html>
