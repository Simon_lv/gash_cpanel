<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  回存紀錄</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/sisyphus.min.js"></script>

<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	$(function(){
		$("#search").click(function(){
			$("form[name='search']").attr('action','${ctx}/restore/restore!list.action');
			var carId = $("input[name='carId']").val();
			var startTime = $("input[name='startTime']").val();
			var endTime = $("input[name='endTime']").val();
			//var canSell = $("#select option:selected").val();
			if($.trim(carId)=="" && $.trim(startTime)=="" && $.trim(endTime)=="") {
			//if($.trim(carId)=="" && $.trim(startTime)=="" && $.trim(endTime)=="" && $.trim(canSell)=="") {
				alert("請輸入查詢條件");
				return false;
			}
			document.forms[0].submit();
		});
	});
	/*
	function doChange(type,id){
		  if(type=="" || id==""){
			  alert("操作錯誤！");
		  }else{
			  if(window.confirm("是否確定刪除？")){
				  $.ajax({ 
				   		url: "${ctx}/driver/driver!delete.action",
				   		type: "POST",
				   		data:"type="+type+"&id="+id,
				   		success: function(data){	
				   		   if(data>0){
				   				window.location.href = "${ctx}/driver/driver!list.action?page.pageNo=${page.prePage}&carId=${map['carId']}&name=${map['name']}&mobile=${map['mobile']}&canSell=${map['canSell']}";
				   		   }else{
				   			   alert("發生錯誤!");
				   		   }
					    },
					    error:function(){
					  	  alert("發生錯誤，請聯繫系管理員！");
					    }   
					});	
			  }
		  }	
	}
	*/
	
	function toAdd(){
		window.location.href = "${ctx}/restore/restore!toAdd.action";
	}
	$( function() { 
        $("#exportExcel").click(function(){
            $("form[name='search']").attr('action','${ctx}/restore/restore!export.action');
            var carId = $("input[name='carId']").val();
            var startTime = $("input[name='startTime']").val();
            var endTime = $("input[name='endTime']").val();
            //var canSell = $("#select option:selected").val();
            document.forms[0].submit();
        });
        $( "form" ).sisyphus(); 
    } );
    <c:if test="${not empty map.reset}"> 
        $( function() { 
            $( "form" ).sisyphus().manuallyReleaseData();
            $("form[name='search']")[0].reset();
            //alert("reset");
        } );
    </c:if>
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<h3>&nbsp;&nbsp;回存紀錄</h3>
<div id="filter">
<form action="${ctx}/restore/restore!list.action" name="search" method="post">
   <div class="itemDiv" > 隊編：<input type="text" name="carId" value="${map['carId'] }" /></div>
     <div class="itemDiv">
        回存時間：<input type="text" id="receiveStartTime" name="startTime" value="${map['startTime']}" size="20" style="vertical-align:middle"/>
<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" id="trigger1" 
																style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																title="選擇日期" onmouseover="this.style.background='red';"
																onmouseout="this.style.background=''" align="middle"
																onmousemove="javascript:Calendar.setup({inputField	: 'receiveStartTime',ifFormat: '%Y-%m-%d 00:00:00',button: 'trigger1' });" /> ～
<input type="text" id="receiveEndTime" name="endTime" value="${map['endTime']}" size="20" style="vertical-align:middle"/>
<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" name="trigger2" align="middle" id="trigger2"
																style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																title="選擇日期" onmouseover="this.style.background='red';"
																onmousemove="javascript:Calendar.setup({inputField	: 'receiveEndTime',ifFormat: '%Y-%m-%d 23:59:59',button: 'trigger2' });"
																onmouseout="this.style.background=''"/>&nbsp;&nbsp;&nbsp;&nbsp;時間格式：2010-01-15 08:00:00   &nbsp;&nbsp;&nbsp;&nbsp;<!-- span style="color:red;font-weight:bold">溫馨提示：查詢時間不能超過35天！</span-->
  </div>
<!-- <div class="itemDiv" > 回存方式：
    <select name="method" id="select">
        <option value=""></option>
        <option value="0" <c:if test="${map['method'] =='0'}">selected</c:if>>全選</option>
        <option value="1" <c:if test="${map['method'] =='1'}">selected</c:if>>隊上回存</option>
        <option value="2" <c:if test="${map['method'] =='2'}">selected</c:if>>OK便利店</option>
    </select>
</div> -->
<div class="itemDiv">
  <input type="button"  value="[查詢]" id="search"/> &nbsp;&nbsp;
  <input type="button" value="[新增]" onclick="javascript:toAdd();"/>
  <input type="button"  value="匯出EXCEL" id="exportExcel"/>
</div>
</form>
</div> 

<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>隊編</td>
		<td>回存現金</td>
		<!-- <td>回存方式</td>  -->
		<td>回存時間</td>
		<td>後臺操作人員帳號</td>
	</tr>
	<s:iterator value="page.result">	
		<tr align="center" valign="middle">
			<td>${carId}</td>
			<td>${money}</td>
			<!-- <td>
			    <c:if test="${method == '1'}">
			        隊上回存
			    </c:if>
			    <c:if test="${method == '2'}">
			    OK便利店
			    </c:if>
			</td>  -->
			<td><fmt:formatDate value="${restoreTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></td>
			<td>${adminName}</td>
		</tr>
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/restore/restore!list.action?page.pageNo=${page.prePage}&carId=${map['carId']}&startTime=${map['startTime']}&endTime=${map['endTime']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/restore/restore!list.action?page.pageNo=${page.nextPage}&carId=${map['carId']}&startTime=${map['startTime']}&endTime=${map['endTime']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div>
</div>
</div>
</div>
</body>
</html>
