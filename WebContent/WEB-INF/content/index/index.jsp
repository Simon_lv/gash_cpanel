<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%@ include file="/common/title.jsp"%></title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="container">
<div id="myContent">
<div style="text-align: center;height: 600px; line-height: 550px; font-size: 45px;font-weight: bold;">歡迎傾國科技網站管理後台</div>
</div>
   <jsp:include page="../include_footer.jsp"/>
</div>
</body>
</html>