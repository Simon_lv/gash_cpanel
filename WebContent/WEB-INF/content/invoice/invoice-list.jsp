<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台 發票號碼</title>
<%@ include file="/common/meta.jsp"%>
<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jquery.js"></script>

<style type="text/css">
.itemDiv {
	height: 30px;
	line-height: 30px;
}

.deleted {
	color: red;
}

.active {
	color: black;
}
}
</style>
<script type="text/javascript">
	function fillFileName(val) {
		$('#excelFileName').val(val);
	}
	$(function() {
		$("#put").click(function() {
			var excelFileName = $('#excelFileName').val();
			if (excelFileName == "") {
				alert("請選擇上傳檔！");
				return false;
			}
			$("form").submit();
		});
	});
</script>
</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">
				<div id="filter">
					<form name="input" action="${ctx}/invoice/invoice!save.action" enctype="multipart/form-data" method="post">
						<input type="hidden" id="excelFileName" name="excelFileName"
							value="" />
						<div class="itemDiv">
							<input type="file" name="excelFile"
								onchange="fillFileName(this.value)" />
						</div>
						<div class="itemDiv">
							<input type="button" value="導入" id="put" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
