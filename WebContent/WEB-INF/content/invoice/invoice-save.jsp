<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台 發票號碼</title>
<%@ include file="/common/meta.jsp"%>
<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jquery.js"></script>

<style type="text/css">
.itemDiv {
	height: 30px;
	line-height: 30px;
}

.deleted {
	color: red;
}

.active {
	color: black;
}
}
</style>
</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">
				<div id="filter">
					<p>共${total}筆，成功${success}筆，失敗${fn:length(hasInvoceNo) + fn:length(notExistOrder)}筆。</p>
					<p>失敗單號如下：</p>
					<c:forEach items="${hasInvoceNo}" var="o">
						<p>${o}(失敗原因：該訂單號已有發票號碼存在)</p>
					</c:forEach>
					<c:forEach items="${notExistOrder}" var="o">
						<p>${o}(失敗原因：無此訂單號)</p>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
