<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  Banner管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	function doChange(type,id){
		  if(type=="" || id==""){
			  alert("操作錯誤！");
		  }else{
			  if(type==4) {
				 if(window.confirm("確定刪除嗎？")){
					 
				 }else {
					  return false;
				  }
			  }
			  $.ajax({ 
			   		url: "${ctx}/banner/banner!operation.action",
			   		type: "POST",
			   		data:"type="+type+"&id="+id,
			   		success: function(data){	
			   		   if(data>0){
			   				window.location.href = "${ctx}/banner/banner!list.action?page.pageNo=${page.pageNo}";
			   		   }else{
			   			   alert("發生錯誤!");
			   		   }
				    },
				    error:function(){
				  	  alert("發生錯誤，請聯繫系管理員！");
				    }   
				});		  
		  }	
	}
	function toAdd(){
		window.location.href = "${ctx}/banner/banner!toAdd.action";
	}
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">


</div> 
<h3>&nbsp;&nbsp;Banner管理</h3>
<div class="itemDiv">
  <input type="button"  value="新增" onclick="toAdd()" />
</div>
<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>圖片ID</td>
		<td>圖片網址</td>
		<td colspan="3">操作</td>		
	</tr>
	<s:iterator value="page.result" status="obj">	
		<tr align="center" valign="middle">
			<td>${id}</td>
			<td><a href="${bannerUrl}" target="_blank">${bannerUrl}</a></td>
			<td>&nbsp;
				<c:if test="${!obj.first}">
					<a href="#" onclick="doChange(2,${id})">上移</a>
				</c:if>			
			</td>
			<td>&nbsp;
				<c:if test="${!obj.last}">
					<a href="#" onclick="doChange(3,${id})">下移</a>
				</c:if>						
			</td>
			<td>
				<a href="#" onclick="doChange(4,${id})">刪除</a>				
			</td>
		</tr>
	</s:iterator>
</table>
<%-- <div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/banner/banner!list.action?page.pageNo=${page.prePage}&type=${map['type']}&language=${map['language']}&name=${map['name']}&app_status=${map['app_status']}&startTime=${map['startTime']}&endTime=${map['endTime']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/banner/banner!list.action?page.pageNo=${page.nextPage}&type=${map['type']}&language=${map['language']}&name=${map['name']}&app_status=${map['app_status']}&startTime=${map['startTime']}&endTime=${map['endTime']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div> --%>
</div>
</div>
</div>
</body>
</html>
