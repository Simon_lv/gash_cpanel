<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台  Banner管理新增</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<script type="text/javascript">
	function fillImgFileName(val){		
		$('#imgFileName').val(val);
	}
	$(function(){
		$("#comit_img").click(function(){
			var imgFileName = $('#imgFileName').val();
			if(imgFileName =="") {
				alert("請選擇上傳圖片！");
				return false;
			}
			document.forms[0].submit();
		});
	});
	</script>	
	<style>
	.itemDiv {
		height: 30px;
		line-height: 30px;
	}
	
	.activation {
		display: block;
	}
	
	.inactivation {
		display: none;
	}
	</style>
</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">Banner新增</h3>
				<form action="${ctx}/banner/banner!addBanner.action" method="post" enctype="multipart/form-data">
				<input type="hidden" id="imgFileName" name="imgFileName"  value=""/>
					<table width="50%">
						
					    <tr>
							<td>圖片上傳</td>
							<td><input type="file" name="imgFile" size="50" onchange="fillImgFileName(this.value)" /></td>
						</tr>
					
						<tr>
							<td colspan="3">
								<input type="button" value="取消" onclick="javascript:history.go(-1);" />
								<input type="button"  id="comit_img" value="確定" />								
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>

	</div>
</body>
</html>
