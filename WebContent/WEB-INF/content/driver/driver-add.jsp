<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台  司機管理-新增</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#comit_driver").click(function(){
			var carId = $("input[name='carId']").val();
			var name = $("input[name='name']").val();
			var mobile = $("input[name='mobile']").val();
			var credit = $("input[name='credit']").val();
			var shortCode = $("input[name='shortCode']").val();
			if($.trim(carId)=="") {
				alert("請輸入隊編");
				return false;
			}
			if( $.trim(name)=="") {
				alert("請輸入姓名");
				return false;
			}
			if($.trim(mobile)==""){
				alert("請輸入手機號");
				return false;
			}
			if(!/^[0-9]{10}$/.test(mobile)) {
				alert("請輸入正確的手機號");
				return false;
			}
			
			if($.trim(credit)=="") {
				alert("請輸入信用額度");
				return false;
			}
			if($.trim(shortCode)=="") {
                alert("請輸入司機簡碼");
                return false;
            }
			var reg = /^[0-9]+$/;
			if(!reg.test(carId)) {
				alert("隊編只能輸入數字");
				return false;
			}
			document.forms[0].submit();
		});
	});
	<c:if test="${!empty message}">
		alert("隊編對應的資料已經錄入！");
	</c:if>
</script>	
	<style>
	.itemDiv {
		height: 30px;
		line-height: 30px;
	}
	
	.activation {
		display: block;
	}
	
	.inactivation {
		display: none;
	}
	</style>
</head>
<body>

	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent"><h3>司機管理-新增</h3>
				<form action="${ctx}/driver/driver!save.action" method="post" id="form">
					<table width="80%">
						<tr>
							<td>隊編：</td>
							<td><input type="text" name="carId" size="50" /></td>
						</tr>
						<tr>
							<td>姓名：</td>
							<td><input type="text" name="name" size="50" /></td>
						</tr>	
						<tr>
							<td>手機號碼：</td>
							<td><input type="text" name="mobile" size="50" /></td>
						</tr>	
						<tr>
							<td>信用額度：</td>
							<td><input type="text" name="credit" size="50" /></td>
						</tr>						
						<tr>
                            <td>司機簡碼</td>
                            <td><input type="text" name="shortCode" size="50" /></td>
                        </tr>
						<tr>
							<td colspan="2">
								<input type="button" value="取消" onclick="javascript:history.go(-1);" />
								<input type="button"  id="comit_driver" value="確定" />								
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>

	</div>
</body>
</html>
