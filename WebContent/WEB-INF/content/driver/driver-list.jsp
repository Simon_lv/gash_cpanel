<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  司機管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/sisyphus.min.js"></script>

<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	$(function(){
		$("#search").click(function(){
			$("form[name='search']").attr('action','${ctx}/driver/driver!list.action');
			var carId = $("input[name='carId']").val();
			var name = $("input[name='name']").val();
			var mobile = $("input[name='mobile']").val();
			var canSell = $("#select option:selected").val();
			if($.trim(carId)=="" && $.trim(name)=="" && $.trim(mobile)=="" && $.trim(canSell)=="") {
				alert("請輸入查詢條件");
				return false;
			}
			document.forms[0].submit();
		});
	});
	function doChange(type,id){
		  if(type=="" || id==""){
			  alert("操作錯誤！");
		  }else{
			  if(window.confirm("是否確定刪除？")){
				  $.ajax({ 
				   		url: "${ctx}/driver/driver!delete.action",
				   		type: "POST",
				   		data:"type="+type+"&id="+id,
				   		success: function(data){	
				   		   if(!!data){
				   				window.location.href = "${ctx}/driver/driver!list.action?page.pageNo=${page.prePage}&carId=${map['carId']}&name=${map['name']}&mobile=${map['mobile']}&canSell=${map['canSell']}";
				   		   }else{
				   			   alert("發生錯誤!");
				   		   }
					    },
					    error:function(){
					  	  alert("發生錯誤，請聯繫系管理員！");
					    }   
					});	
			  }
		  }	
	}
	function toAdd(){
		window.location.href = "${ctx}/driver/driver!toAdd.action";
	}
	$( function() { 
		$("#exportExcel").click(function(){
            $("form[name='search']").attr('action','${ctx}/driver/driver!export.action');
            var carId = $("input[name='carId']").val();
            var name = $("input[name='name']").val();
            var mobile = $("input[name='mobile']").val();
            var canSell = $("#select option:selected").val();
            document.forms[0].submit();
        });
	    $( "form" ).sisyphus(); 
	} );
	<c:if test="${not empty map.reset}"> 
		$( function() { 
		    $( "form" ).sisyphus().manuallyReleaseData();
		    $("form[name='search']")[0].reset();
		    //alert("reset");
		} );
	</c:if>
	
</script>
</head>
<body>        
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">
<form action="${ctx}/driver/driver!list.action" name="search" method="post" id="searchForm">
   <div class="itemDiv" > 隊編：<input type="text" name="carId"/>
   </div>
    <div class="itemDiv" > 姓名：<input type="text" name="name"/>					 
   </div>
   <div class="itemDiv" > 手機號碼：<input type="text" name="mobile"/>					 
   </div>
   <div class="itemDiv" > 是否具販售資格：
   						 <select name="canSell" id="select">
					        <option value="">--請選擇--</option>
			    			<option value="1" >可販售</option>	
			    			<option value="2" >不可販售</option>	
    					  </select>				 
   </div>

<div class="itemDiv">
  <input type="button"  value="搜尋" id="search"/> &nbsp;&nbsp;
  <input type="button"  value="新增" onclick="toAdd()" />
  <input type="button"  value="匯出EXCEL" id="exportExcel"/>
</div>
</form>
</div> 
<h3>&nbsp;&nbsp;司機管理</h3>

<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>隊編</td>
		<td>姓名</td>
		<td>手機號碼</td>
		<td>信用額度</td>
		<td>具販售資格</td>
		<td>司機簡碼</td>
		<td  colspan="2">管理</td>
		
	</tr>
	<s:iterator value="page.result">	
		<tr align="center" valign="middle">
			<td>${carId}</td>
			<td>${name}</td>
			<td>${mobile}</td>
			<td>${credit}</td>
			<td>
				<c:if test="${canSell==1}">是</c:if>
				<c:if test="${canSell==2}"><font color="red">否</font></c:if>
			</td>
			<td>${shortCode}</td>
			<security:authorize ifAnyGranted="/driver/driver!edit.action">
			<td>
			    <a href="${ctx}/driver/driver!edit.action?carId=${carId}">修改</a>	
			</td>
			</security:authorize>
			<td>
				<a href="#" onclick="doChange(4,'${carId}')">刪除</a>				
			</td>
		</tr>
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/driver/driver!list.action?page.pageNo=${page.prePage}&carId=${map['carId']}&name=${map['name']}&mobile=${map['mobile']}&canSell=${map['canSell']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/driver/driver!list.action?page.pageNo=${page.nextPage}&carId=${map['carId']}&name=${map['name']}&mobile=${map['mobile']}&canSell=${map['canSell']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div>
</div>
</div>
</div>
</body>
</html>
