<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台 點卡數量管理</title>
<%@ include file="/common/meta.jsp"%>
<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jquery.js"></script>
<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css"
	rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/sisyphus.min.js"></script>

<style type="text/css">
.itemDiv {
	height: 30px;
	line-height: 30px;
}

.deleted {
	color: red;
}

.active {
	color: black;
}
}
</style>
<script type="text/javascript">
	$(function() {
		$("#search").click(
				function() {
					$("form[name='search']").attr('action','${ctx}/card/card!list.action');
					var startTime = $("input[name='startTime']").val();
					var endTime = $("input[name='endTime']").val();
					if ($.trim(startTime) == "" && $.trim(endTime) == "" ) {
						alert("請輸入查詢條件");
						return false;
					}
					if (!verifyDate()) {
						return false;
					}
					document.forms[0].submit();
				});
	});
	function verifyDate() {
		var startTime = $("#receiveStartTime").val();
		var endTime = $("#receiveEndTime").val();
		var startDateTime;
		var endDateTime;
		if (startTime && endTime) {
			startDateTime = new Date(startTime.replace(/-/g, '/'));
			endDateTime = new Date(endTime.replace(/-/g, '/'));
			var d = 31 * 24 * 60 * 60 * 1000;
			if ((endDateTime.getTime() - startDateTime.getTime()) > d) {
				alert('賣出時間起迄不得超過一個月');
				return false;
			}
		}
		if (endTime) {
			endDateTime = new Date(endTime.replace(/-/g, '/'));
			if (endDateTime.getTime() > new Date().getTime()) {
				alert("不可選擇未發生的時間點");
				return false;
			}
		}
		return true;
	}
	$( function() { 
        $("#exportExcel").click(function(){
            $("form[name='search']").attr('action','${ctx}/card/card!export.action');
            var startTime = $("input[name='startTime']").val();
            var endTime = $("input[name='endTime']").val();
            if (!verifyDate()) {
                return false;
            }
            document.forms[0].submit();
        });
        $( "form" ).sisyphus(); 
    } );
    <c:if test="${not empty map.reset}"> 
        $( function() { 
            $( "form" ).sisyphus().manuallyReleaseData();
            $("form[name='search']")[0].reset();
            //alert("reset");
        } );
    </c:if>
</script>
</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">
				<h3>&nbsp;&nbsp;點卡數量管理</h3>
				<div id="filter">
					<form action="${ctx}/card/card!list.action" name="search"
						method="post">
						<table>
							<tr align="center" valign="middle">
								<td>面額</td>
								<td>總數</td>
								<td>車上賣</td>
								<td><a href="${ctx}/card/invoice!list.action">其它</a></td>
								<td>剩餘張數</td>
								<td>剩餘百分比</td>
							</tr>
							<c:forEach items="${remains }" var="rm">
								<tr align="center" valign="middle">
									<td>${rm.price }</td>
									<td>${rm.notSoldCount + rm.soldCount }</td>
									<td>${rm.carSoldCount }</td>
									<td>${rm.otherSoldCount }</td>
									<td>${rm.notSoldCount }</td>
									<td><fmt:formatNumber type="percent"
											value="${rm.notSoldCount/(rm.notSoldCount+rm.soldCount)}" /></td>
								</tr>
							</c:forEach>
						</table>
						<br />
						<div class="itemDiv">
							賣出時間：<input type="text" id="receiveStartTime" name="startTime"
								value="${map['startTime']}" size="20"
								style="vertical-align: middle" /> <img
								src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif"
								id="trigger1"
								style="cursor: pointer; border: 1px solid red; vertical-align: middle"
								title="選擇日期" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''" align="middle"
								onmousemove="javascript:Calendar.setup({inputField	: 'receiveStartTime',ifFormat: '%Y-%m-%d 00:00:00',button: 'trigger1' });" />
							～ <input type="text" id="receiveEndTime" name="endTime"
								value="${map['endTime']}" size="20"
								style="vertical-align: middle" /> <img
								src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif"
								name="trigger2" align="middle" id="trigger2"
								style="cursor: pointer; border: 1px solid red; vertical-align: middle"
								title="選擇日期" onmouseover="this.style.background='red';"
								onmousemove="javascript:Calendar.setup({inputField	: 'receiveEndTime',ifFormat: '%Y-%m-%d 23:59:59',button: 'trigger2' });"
								onmouseout="this.style.background=''" />&nbsp;&nbsp;&nbsp;&nbsp;時間格式：2010-01-15
							08:00:00 &nbsp;&nbsp;&nbsp;&nbsp;
							<!-- span style="color:red;font-weight:bold">溫馨提示：查詢時間不能超過35天！</span-->
						</div>
						<div class="itemDiv">
							<input type="button" value="[查詢]" id="search" /> &nbsp;&nbsp;
							<input type="button"  value="匯出EXCEL" id="exportExcel"/>
						</div>
					</form>
				</div>

				<table width="98%">
					<tr style="text-align: center; font-weight: bold;">
						<td>面額</td>						
						<td>訂單狀態</td>
						<td>賣出張數</td>
						<td>已回存金額</td>
						<td>未回存金額</td>
						<td>是否退款</td>
					</tr>
					<c:forEach items="${results}" var="rs">
						<tr align="center" valign="middle">
							<td>${rs.price}</td>
							<td>
								<c:if test="${rs.orderStatus==1}">用戶請求</c:if>
								<c:if test="${rs.orderStatus==2}">發送司機簡訊</c:if>
								<c:if test="${rs.orderStatus==3}">收到司機回覆</c:if>
								<c:if test="${rs.orderStatus==4}">發送點卡</c:if>
								<c:if test="${rs.orderStatus==5}">司機事實並未收款</c:if>
								<c:if test="${rs.orderStatus==6}">用戶未收到點卡簡訊</c:if>
								<c:if test="${rs.orderStatus==7}">用戶已收到點卡簡訊</c:if>
								<c:if test="${rs.orderStatus==9}">退款</c:if>
							</td>
							<td>${rs.count}</td>
							<td>${rs.restoreSum}</td>
							<td>${rs.notRestoreSum}</td>
							<td><c:if test="${rs.orderStatus == 9 }">是</c:if> <c:if
									test="${rs.orderStatus != 9 }">否</c:if></td>
						</tr>
					</c:forEach>
				</table>

			</div>
		</div>
	</div>
</body>
</html>
