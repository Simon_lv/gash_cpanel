<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台 購買紀錄-詳情</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#comit_driver").click(function(){
			document.forms[0].submit();
		});
	});
	
	function send(id) {
		if(id==""){
			  alert("操作錯誤！");
			  return;
		  }
		  if(window.confirm("是否確定重新發送？")){
			  $.ajax({ 
			   		url: "${ctx}/payment/payment!sendSms.action",
			   		type: "POST",
			   		data:"id="+id,
			   		success: function(data){	
			   		   if(!!data){
			   			   $("#sendSms").val("已重新發送");
			   		   }else{
			   			   alert("發生錯誤!");
			   		   }
				    },
				    error:function(){
				  	  alert("發生錯誤，請聯繫系管理員！");
				    }   
				});	
		  }
	}
	
	function changeInvoice() {
		var select = document.getElementById("invoiceType");
		var selectValue = select.options[select.selectedIndex].value;
		
		if(selectValue == 2) {
			var obj = document.getElementById("invoiceAddress");
			obj.removeAttribute("readOnly",false);
			
			var obj2 = document.getElementById("userEmail");
			obj2.removeAttribute("readOnly",false);
			
			var obj3 = document.getElementById("invoiceNo");
			obj3.removeAttribute("readOnly",false);
		}
		else {
			var obj = document.getElementById("invoiceAddress");
			obj.setAttribute("readOnly",true);
			
			var obj2 = document.getElementById("userEmail");
			obj2.setAttribute("readOnly",true);
			
			var obj3 = document.getElementById("invoiceNo");
			obj3.setAttribute("readOnly",true);
		}
	}
	
	</script>	
	<style>
	.itemDiv {
		height: 30px;
		line-height: 30px;
	}
	
	.activation {
		display: block;
	}
	
	.inactivation {
		display: none;
	}
	</style>
</head>
<body>

	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent"><h3>購買紀錄-詳情</h3>
				<form action="${ctx}/payment/payment!updatePayment.action" method="post" id="form">
				<input type="hidden" name="id" value="${payment.id}"/>
					<table width="80%">
						  <tr align="center" valign="middle">
							<td>訂單編號：</td>
							<td>${payment.orderId}</td>
							<td>隊編：</td>
							<td>${payment.carId}</td>
						</tr>
						  <tr align="center" valign="middle">
							<td>用戶手機：</td>
							<td>${payment.userMobile}</td>
							<td>司機手機：</td>
							<td>${payment.driverMobile}</td>
						</tr>	
						  <tr align="center" valign="middle">
							<td>GASH編號：</td>
							<td >${payment.sn}</td>
							<td>面額：</td>
							<td>${payment.price }</td>
						</tr>	
						<tr align="center" valign="middle">
							<td>訂單時間(用戶請求時間)：</td>
							<td><fmt:formatDate value="${payment.createTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>司機回覆收款時間：</td>
							<td><fmt:formatDate value="${payment.payTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></td>
						</tr>	
						  <tr align="center" valign="middle">
							<td>發送簡訊時間：</td>
							<td> 
							   <fmt:formatDate value="${payment.smsTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/>
							   <c:if test="${resend == 1}">
							   <input id="sendSms" type="button" value="重新發送" onclick="send('${payment.id}')" />   
							   </c:if>
							</td>
							<td>訂單狀態：</td>
							<td>
								<c:if test="${payment.orderStatus==1}">用戶請求</c:if>
								<c:if test="${payment.orderStatus==2}">發送司機簡訊</c:if>
								<c:if test="${payment.orderStatus==3}">收到司機回覆</c:if>
								<c:if test="${payment.orderStatus==4}">發送點卡</c:if>
								<c:if test="${payment.orderStatus==5}">司機事實並未收款</c:if>
								<c:if test="${payment.orderStatus==6}">用戶未收到點卡簡訊</c:if>
								
								<c:if test="${payment.orderStatus==7}">
								    <select name="orderStatus">
								        <option value="7" selected>用戶已收到點卡簡訊</option>
								        <option value="9">退款</option>
								    </select>
								</c:if>
								
								<c:if test="${payment.orderStatus==9}">退款</c:if>
							</td>
						</tr>
						  <tr align="center" valign="middle">
							<td>回存狀態：</td>
							<td>
								<c:if test="${payment.restoreStatus==1}">未回存</c:if>
								<c:if test="${payment.restoreStatus==2}">已回存</c:if>
							</td>
							<td>回存時間：</td>
							<td><fmt:formatDate value="${payment.restoreTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/></td>
						</tr>
						<tr>
							<td>發票方式：</td>
							<td colspan="3">
								<select id="invoiceType" name="invoiceType" onchange="changeInvoice();">
									<option <c:if test="${payment.invoiceType==1}">selected</c:if> value="1" >捐贈</option>	
			    					<option <c:if test="${payment.invoiceType==2}">selected</c:if>  value="2" >紙本寄送</option>
								</select>
							</td>
							
						</tr>
						<tr>
							<td>地址：</td>
							<td colspan="3"><input <c:if test="${payment.invoiceType==1}">readOnly=true</c:if> type="text" id="invoiceAddress" name="invoiceAddress" size="80" value="${payment.invoiceAddress}"/></td>
						</tr>
						<tr>
							<td>Email：</td>
							<td colspan="3"><input <c:if test="${payment.invoiceType==1}">readOnly=true</c:if> type="text" id="userEmail" name="userEmail" size="80" value="${payment.userEmail}"/></td>
						</tr>
						<tr>
							<td>發票號碼：</td>
							<td colspan="3"><input <c:if test="${payment.invoiceType==1}">readOnly=true</c:if> type="text" id="invoiceNo" name="invoiceNo" size="80" value="${payment.invoiceNo}"/></td>
						</tr>					
						
						<tr>
							<td colspan="4">
								<input type="button" value="[取消]" onclick="javascript:history.go(-1);" />
								<input type="button"  id="comit_driver" value="[確定修改]" />								
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>

	</div>
</body>
</html>
