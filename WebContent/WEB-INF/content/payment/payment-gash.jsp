<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  購買紀錄</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">

	$(function(){
		$("#search").click(function(){
			$("form[name='search']").attr('action','${ctx}/payment/payment!list.action');
			var startTime = $("input[name='startTime']").val();
			var endTime = $("input[name='endTime']").val();
			if($.trim(startTime)==""&& $.trim(endTime)=="") {
				alert("請輸入查詢條件");
				return false;
			}
			document.forms[0].submit();
		});
		
	});
	
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">
<form action="${ctx}/payment/payment!list.action" name="search" method="post">
   <div class="itemDiv">
	訂單時間：<input type="text" id="startTime" name="startTime" value="${map['startTime']}" size="20" style="vertical-align:middle"/>
			<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" id="trigger1" 
																						style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																						title="選擇日期" onmouseover="this.style.background='red';"
																						onmouseout="this.style.background=''" align="middle"
																						onmousemove="javascript:Calendar.setup({inputField	: 'startTime',ifFormat: '%Y-%m-%d 00:00:00',button: 'trigger1' });" /> ～
	<input type="text" id="endTime" name="endTime" value="${map['endTime']}" size="20" style="vertical-align:middle"/>
	<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" name="trigger2" align="middle" id="trigger2"
																						style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																						title="選擇日期" onmouseover="this.style.background='red';"
																						onmousemove="javascript:Calendar.setup({inputField	: 'endTime',ifFormat: '%Y-%m-%d 23:59:59',button: 'trigger2' });"
																						onmouseout="this.style.background=''"/>&nbsp;&nbsp;&nbsp;&nbsp;時間格式：2010-01-15 08:00:00   &nbsp;&nbsp;&nbsp;&nbsp;<!-- span style="color:red;font-weight:bold">溫馨提示：查詢時間不能超過35天！</span-->
	</div>
<div class="itemDiv">
  <input type="button"  value="搜尋" id="search"/>
</div>
</form>
</div> 
<h3>面額總計：${priceSum}</h3>
<h3>&nbsp;&nbsp;購買紀錄</h3>

<table width="40%">
	<tr style="text-align: center;font-weight: bold;">
		<td>訂單時間</td>
		<td>面額</td>
	</tr>
	<s:iterator value="page.result">	
		<tr align="center" valign="middle">
			<td>
				<fmt:formatDate value="${createTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/>
			</td>
			<td>${price}</td>
		</tr>
		
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/payment/payment!list.action?page.pageNo=${page.prePage}&orderId=${map['orderId']}&vatNo=${map['vatNo']}&carId=${map['carId']}&startTime=${map['startTime']}&endTime=${map['endTime']}&userMobile=${map['userMobile']}&orderStatus=${map['orderStatus']}&restoreStatus=${map['restoreStatus']}&invoiceType=${map['invoiceType']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/payment/payment!list.action?page.pageNo=${page.nextPage}&orderId=${map['orderId']}&vatNo=${map['vatNo']}&carId=${map['carId']}&startTime=${map['startTime']}&endTime=${map['endTime']}&userMobile=${map['userMobile']}&orderStatus=${map['orderStatus']}&restoreStatus=${map['restoreStatus']}&invoiceType=${map['invoiceType']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div>

</div>

</div>
</div>
</body>
</html>
