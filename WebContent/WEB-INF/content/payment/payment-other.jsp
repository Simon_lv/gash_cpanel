<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  購買紀錄</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
function doGiftPoint() {
	$("#giftPoint").attr("disabled", true);
	$("#giftPoint").text("傳送中");
	var isZeroAvailableCount = true;
	if($("#selectedPaymentId").val().length==0) {
		alert("訂單編號為空");
		return;
	}
	if($("#selectedUserMobile").val().length==0) {
		alert("用戶手機為空");
		return;
	}
	inputData = "selectedPaymentId="+$("#selectedPaymentId").val()+"&selectedUserMobile="+$("#selectedUserMobile").val();
	$("div table tr td input").each(function () {
		inputData=inputData+"&"+$(this).attr('id')+"="+$(this).val();
		isZeroAvailableCount = isZeroAvailableCount && ($(this).val() == 0);
    });
	if(isZeroAvailableCount) {
		alert("請輸入贈送張數");
		return;
	}
	$.ajax({
   		url: "${ctx}/payment/payment!giftPoints.action", 
   		dataType:"json",
   		data:inputData,
   		success: function(data){
   			$("div table tr td input").each(function () {
   				$(this).val(0);
   		    });
   			$("td[id^='availableCount']").each(function () {
   				var a = data[$(this).attr('id')];
   				if(typeof a == 'undefined') {
   					$(this).text(0);
   				} else {
   					$(this).text(a);
   				}
   		    });
   			$("#giftPoint"+$("#selectedPaymentId").val()).hide();
   			$("#giftPoint").hide();
   			
   			alert(data["msg"]);
  		},
  		error:function(){
			alert("發生錯誤，請聯系管理員！");
  		}
	});
}

	$(function(){
		$("#search").click(function(){
			$("form[name='search']").attr('action','${ctx}/payment/payment!list.action');
			var orderId = $("input[name='orderId']").val();
			//var vatNo = $("input[name='vatNo']").val();
			var carId = $("input[name='carId']").val();
			var startTime = $("input[name='startTime']").val();
			var endTime = $("input[name='endTime']").val();
			var userMobile = $("input[name='userMobile']").val();
			var orderStatus = $("#select option:selected").val();
			var invoiceType = $("#invoiceSelect option:selected").val();
			var restoreStatus = $("#restoreSelect option:selected").val();
			if($.trim(orderId)=="" && $.trim(carId)=="" && $.trim(userMobile)=="" 
					&& $.trim(orderStatus)==""&& $.trim(startTime)==""&& $.trim(endTime)==""&& $.trim(invoiceType)==""&& $.trim(restoreStatus)=="") {
				alert("請輸入查詢條件");
				return false;
			}
			document.forms[0].submit();
		});
		$("#exportExcel").click(function(){
			$("form[name='search']").attr('action','${ctx}/payment/payment!export.action');
			var orderId = $("input[name='orderId']").val();
			//var vatNo = $("input[name='vatNo']").val();
			var carId = $("input[name='carId']").val();
			var startTime = $("input[name='startTime']").val();
			var endTime = $("input[name='endTime']").val();
			var userMobile = $("input[name='userMobile']").val();
			var orderStatus = $("#select option:selected").val();
			var invoiceType = $("#invoiceSelect option:selected").val();
			var restoreStatus = $("#restoreSelect option:selected").val();
			if($.trim(orderId)=="" && $.trim(carId)=="" && $.trim(userMobile)=="" 
					&& $.trim(orderStatus)==""&& $.trim(startTime)==""&& $.trim(endTime)==""&& $.trim(invoiceType)==""&& $.trim(restoreStatus)=="") {
				alert("請輸入查詢條件");
				return false;
			}
			document.forms[0].submit();
		});
	});
	function chargeback(orderId, id) {
		if(confirm("是否確定退單?訂單編號:"+orderId)) {
			$.ajax({
		   		url: "${ctx}/payment/payment!chargeback.action", 
		   		dataType:"json",
		   		data:"id="+id,
		   		success: function(data){
		   			alert("操作成功！");
		   			$("#p"+id).text("退單");
		   			$("#chargebackBtn"+id).hide(0);
	      		},
	      		error:function(){
	    			alert("發生錯誤，請聯系管理員！");
	      		}
	   		});
		}
		
	}
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">
<form action="${ctx}/payment/payment!list.action" name="search" method="post">
   <div class="itemDiv" > 訂單編號：<input type="text" name="orderId"/>  &nbsp;&nbsp;&nbsp;&nbsp;隊編：<input type="text" name="carId"/>
   </div>
   <div class="itemDiv" > 用戶手機：<input type="text" name="userMobile"/>					 
   </div>
   <div class="itemDiv">
	訂單時間：<input type="text" id="startTime" name="startTime" value="${map['startTime']}" size="20" style="vertical-align:middle"/>
			<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" id="trigger1" 
																						style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																						title="選擇日期" onmouseover="this.style.background='red';"
																						onmouseout="this.style.background=''" align="middle"
																						onmousemove="javascript:Calendar.setup({inputField	: 'startTime',ifFormat: '%Y-%m-%d 00:00:00',button: 'trigger1' });" /> ～
	<input type="text" id="endTime" name="endTime" value="${map['endTime']}" size="20" style="vertical-align:middle"/>
	<img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" name="trigger2" align="middle" id="trigger2"
																						style="cursor: pointer; border: 1px solid red;vertical-align:middle"
																						title="選擇日期" onmouseover="this.style.background='red';"
																						onmousemove="javascript:Calendar.setup({inputField	: 'endTime',ifFormat: '%Y-%m-%d 23:59:59',button: 'trigger2' });"
																						onmouseout="this.style.background=''"/>&nbsp;&nbsp;&nbsp;&nbsp;時間格式：2010-01-15 08:00:00   &nbsp;&nbsp;&nbsp;&nbsp;<!-- span style="color:red;font-weight:bold">溫馨提示：查詢時間不能超過35天！</span-->
	</div>
   <div class="itemDiv" > 訂單狀態：
   						 <select name="orderStatus" id="select">
					        <option value="">--請選擇--</option>
			    			<option value="1" >用戶請求</option>	
			    			<option value="2" >發送司機簡訊</option>	
			    			<option value="3" >收到司機回覆</option>
			    			<option value="4" >發送點卡</option>
			    			<option value="5" >司機事實並未收款</option>
			    			<option value="6" >用戶未收到點卡簡訊</option>
			    			<option value="7" >用戶已收到點卡簡訊</option>
			    			<option value="9" >退款</option>
    					  </select>				 
   </div>
	<div class="itemDiv" > 發票方式：<select name="invoiceType" id="invoiceSelect">
					        <option value="">--請選擇--</option>
			    			<option value="1" >捐贈</option>	
			    			<option value="2" >紙本寄送</option>
    					  </select>					 
   </div>
   <div class="itemDiv" > 回存狀態：<select name="restoreStatus" id="restoreSelect">
					        <option value="">--請選擇--</option>
			    			<option value="1" >未回存</option>	
			    			<option value="2" >已回存</option>
    					  </select>						 
   </div>
   <div class="itemDiv" > 是否測試賬務：<select name="isTest" id="isTest">
			    			<option value="0" >否</option>	
			    			<option value="1" >是</option>
    					  </select>						 
   </div>
<div class="itemDiv">
  <input type="button"  value="搜尋" id="search"/>
</div>
</form>
</div> 
<h3>面額總計：${priceSum}</h3>
<h3>&nbsp;&nbsp;購買紀錄</h3>

<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>訂單編號</td>
		<td>隊編</td>
		<td>司機手機</td>
		<td>用戶手機</td>
		<td>訂單時間</td>
		<td>面額</td>
		<td>訂單狀態</td>
		<td>回存狀態</td>
		<td>統一發票號碼</td>
		<td>是否測試賬務</td>
		<td>詳情</td>
		
	</tr>
	<s:iterator value="page.result">	
		<tr align="center" valign="middle">
			<td>${orderId}</td>
			<td>${carId}</td>
			<td>${driverMobile}</td>
			<td>${userMobile}</td>
			<td>
				<fmt:formatDate value="${createTime}" type="both" pattern="yyyy-MM-dd HH:mm:ss"/>
			</td>
			<td>${price}</td>
			<td>
				<p id="p${id}">
					<c:if test="${orderStatus==1}">用戶請求</c:if>
					<c:if test="${orderStatus==2}">發送司機簡訊</c:if>
					<c:if test="${orderStatus==3}">收到司機回覆</c:if>
					<c:if test="${orderStatus==4}">發送點卡</c:if>
					<c:if test="${orderStatus==5}">司機事實並未收款</c:if>
					<c:if test="${orderStatus==6}">用戶未收到點卡簡訊</c:if>
					<c:if test="${orderStatus==7}">用戶已收到點卡簡訊</c:if>
					<c:if test="${orderStatus==9}">退款</c:if>
				</p>	
			</td>
			<td>
				<c:if test="${restoreStatus==1}"><font color="red">未回存</font></c:if>
				<c:if test="${restoreStatus==2}">已回存</c:if>
			</td>
			<td>${invoiceNo}</td>			
			<td>
			<c:if test="${isTest==0}">否</c:if>
			<c:if test="${isTest==1}">是</c:if>
			</td>
			<td>
				<a href="${ctx}/payment/payment!detail.action?id=${id}">進入</a>
				<c:if test="${map['canChargeback'] == 1}">
					<c:choose>
						<c:when test="${orderStatus==4 || orderStatus==7}">
							<button id="chargebackBtn${id}" type="button" onclick="chargeback('${orderId}','${id}')">退單</button>
						</c:when>
					</c:choose>
				</c:if>				
			</td>
			
		</tr>
		
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/payment/payment!list.action?page.pageNo=${page.prePage}&orderId=${map['orderId']}&vatNo=${map['vatNo']}&carId=${map['carId']}&startTime=${map['startTime']}&endTime=${map['endTime']}&userMobile=${map['userMobile']}&orderStatus=${map['orderStatus']}&restoreStatus=${map['restoreStatus']}&invoiceType=${map['invoiceType']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/payment/payment!list.action?page.pageNo=${page.nextPage}&orderId=${map['orderId']}&vatNo=${map['vatNo']}&carId=${map['carId']}&startTime=${map['startTime']}&endTime=${map['endTime']}&userMobile=${map['userMobile']}&orderStatus=${map['orderStatus']}&restoreStatus=${map['restoreStatus']}&invoiceType=${map['invoiceType']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div>

</div>

</div>
</div>
</body>
</html>
