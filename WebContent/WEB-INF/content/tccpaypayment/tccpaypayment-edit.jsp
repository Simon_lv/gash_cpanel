<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台 商品詳細頁(新增頁、編輯頁)</title>
<%@ include file="/common/meta.jsp"%>
<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">
.itemDiv {
	height: 30px;
	line-height: 30px;
}

.deleted {
	color: red;
}

.active {
	color: black;
}
}
</style>

</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">
				<div id="filter">
					<div class="itemDiv">訂單詳細頁</div>

					<form action="${ctx}/tccpaypayment/tccpaypayment!save.action" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
						<table width="98%">
							<tr style="text-align: center; font-weight: bold;">
								<td>訂單編號：</td>
								<td>${paymentForView.orderId}</td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>金流方訂單號：</td>
								<td>${paymentForView.payOrderId}</td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>金流方：</td>
								<td>${paymentForView.payFrom}</td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>金流方用戶ID：</td>
								<td>${paymentForView.uid}</td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>遊戲名稱：</td>
								<td>${paymentForView.gameName}</td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>儲值金額：</td>
								<td>${paymentForView.money}&nbsp;${paymentForView.currency}</td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>玩家姓名：</td>
								<td><input name="name" style="width: 400px" value="${paymentForView.name}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>用戶手機：</td>
								<td><input name="mobile" style="width: 400px" value="${paymentForView.userMobile}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>用戶地址：</td>
								<td><input name="address" style="width: 400px" value="${paymentForView.userAddress}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>用戶認證狀態：</td>

								<c:choose>
									<c:when test="${paymentForView.memberStatus == 0}">
										<td><span id="auth">未認證</span><input type="button" id="authenticate" value="認證" onclick="doAuthenticate()" /></td>
									</c:when>
									<c:when test="${paymentForView.memberStatus == 1}">
										<td id="auth">認證</td>
									</c:when>
									<c:when test="${paymentForView.memberStatus == 2}">
										<td><span id="auth">停權</span><input type="button" id="authenticate" value="認證" onclick="doAuthenticate()" /></td>
									</c:when>
								</c:choose>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>收款地址：</td>
								<td><input type="text" name="colAddr" style="width: 400px" value="${paymentForView.colAddr}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>收款狀況：</td>
								<td>
									<c:choose>
										<c:when test="${paymentForView.payStatus == 0}">
											<span id="showPayStatus">待接單</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 1}">
											<span id="showPayStatus">不接單</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 2}">
											<span id="showPayStatus">已接單</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 3}">
											<span id="showPayStatus">已派遣</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 4}">
											<span id="showPayStatus">已收款</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 5}">
											<span id="showPayStatus">已發點</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 6}">
											<span id="showPayStatus">發點失敗</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 7}">
											<span id="showPayStatus">交易進行中取消</span>
										</c:when>
										<c:when test="${paymentForView.payStatus == 9}">
											<span id="showPayStatus">退單</span>
										</c:when>
									</c:choose> 
									<input type="hidden" id="payStatus" value="${paymentForView.payStatus}" />
									
									<c:choose>
										<c:when test="${paymentForView.payStatus == 0}">
											<input type="button" id="changePayStatus" onclick="doChangePayStatus()" value="已接單" />
										</c:when>
										<c:when test="${paymentForView.payStatus == 2}">
											<input type="button" id="changePayStatus" onclick="doChangePayStatus()" value="已派遣" />
										</c:when>
										<c:when test="${paymentForView.payStatus == 3}">
											<input type="button" id="changePayStatus" onclick="doChangePayStatus()" value="已收款" />
										</c:when>
										<c:when test="${paymentForView.payStatus == 4 && sendPoint == 1}">
											<input type="button" id="changePayStatus" onclick="doChangePayStatus()" value="發點" />
										</c:when>
										<c:when test="${paymentForView.payStatus == 6}">
											<input type="button" id="changePayStatus" onclick="doChangePayStatus()" value="發點" />
										</c:when>
									</c:choose>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="button" id="cancelPayStatus" onclick="doCancelPayStatus()" value="" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>是否測試：</td>
								<td><select name="isTest">
										<option value="0" <c:if test="${0 == paymentForView.isTest}">selected</c:if>>否</option>
										<option value="1" <c:if test="${1 == paymentForView.isTest}">selected</c:if>>是</option>
								</select></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>渠道：</td>
								<td><label><input type="radio" name="channel" value="0" <c:if test="${0 == paymentForView.channel}">checked="checked"</c:if> />牛番茄</label> 
									<label><input type="radio" name="channel" value="1" <c:if test="${1 == paymentForView.channel}">checked="checked"</c:if> />大車隊</label> 
									<label><input type="radio" name="channel" value="2" <c:if test="${2 == paymentForView.channel}">checked="checked"</c:if> />CTRL Media</label>
									<label><input type="radio" name="channel" value="3" <c:if test="${3 == paymentForView.channel}">checked="checked"</c:if> />傾國</label>
									<label><input type="radio" name="channel" value="4" <c:if test="${4 == paymentForView.channel}">checked="checked"</c:if> />GAIN RIGHT</label></td>
							</tr>
							<tr>
								<td style="text-align: center; font-weight: bold;">發票種類:</td>
								<td style="text-align: center; font-weight: bold;"><select name="invoiceType">
										<option value="-1"></option>
										<option value="0" <c:if test="${paymentForView.invoiceType == 0}">selected="selected"</c:if>>捐贈</option>
										<option value="2" <c:if test="${paymentForView.invoiceType == 2}">selected="selected"</c:if>>二聯</option>
										<option value="3" <c:if test="${paymentForView.invoiceType == 3}">selected="selected"</c:if>>三聯</option>
								</select></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>發票姓名：</td>
								<td><input type="text" name="invoiceName" maxlength="10" style="width: 400px" value="${paymentForView.invoiceName}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>發票聯繫電話：</td>
								<td><input type="text" name="invoicePhone" maxlength="10" style="width: 400px" value="${paymentForView.invoicePhone}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>發票寄送地址：</td>
								<td><input type="text" name="invoiceAddress" maxlength="200" style="width: 400px" value="${paymentForView.invoiceAddress}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>發票抬頭：</td>
								<td><input type="text" name="invoiceTitle" maxlength="10" style="width: 400px" value="${paymentForView.invoiceTitle}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>統一編號：</td>
								<td><input type="text" name="taxId" maxlength="10" style="width: 400px" value="${paymentForView.taxId}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>發票號碼：</td>
								<td><input type="text" name="invoiceNo" maxlength="10" style="width: 400px" value="${paymentForView.invoiceNo}" /></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>回存狀態:</td>
								<td><select name="restoreStatus">
										<c:choose>
											<c:when test="${paymentForView.restoreStatus == 2}">
												<option value="2" selected="selected">已回存</option>
												<option value="1">未回存</option>
											</c:when>
											<c:otherwise>
												<option value="2">已回存</option>
												<option value="1" selected="selected">未回存</option>
											</c:otherwise>
										</c:choose>
								</select></td>
							</tr>
							<tr style="text-align: center; font-weight: bold;">
								<td>備註：</td>
								<td><textarea rows="4" cols="50" name="remark">${paymentForView.remark}</textarea></td>
							</tr>
						</table>
						<input type="hidden" id="id" name="id" value="${paymentForView.id}" /> <input type="submit" value="確定修改" />
					</form>
				</div>
			</div>

		</div>
	</div>

	<script type="text/javascript">
		if ("${paymentForView.payStatus}" == 0) {
			$("#cancelPayStatus").val("不接單");
		} else if ("${paymentForView.payStatus}" == 1
				|| "${paymentForView.payStatus}" == 7) {
			$("#cancelPayStatus").hide();
		} else if ("${paymentForView.payStatus}" == 2
				|| "${paymentForView.payStatus}" == 3) {
			$("#cancelPayStatus").val("交易進行中取消");
		} else if ("${paymentForView.payStatus}" == 4 && "${sendPoint}" == 1) {
			$("#cancelPayStatus").val("交易進行中取消");
		} else if ("${paymentForView.payStatus}" == 4) {
			$("#cancelPayStatus").val("退單");
		} else if ("${paymentForView.payStatus}" > 4
				&& "${paymentForView.payStatus}" != 9) {
			$("#cancelPayStatus").val("退單");
		} else {
			$("#cancelPayStatus").hide();
		}

		function doAuthenticate() {
			var uid = "${paymentForView.uid}";
			var payFrom = "${paymentForView.payFrom}";

			$.ajax({
				url : "${ctx}/tccpaypayment/tccpaypayment!authenticate",
				type : "POST",
				dataType : "json",
				data : "uid=" + uid + "&payFrom=" + payFrom,
				async : false,
				success : function(data) {
					if (data['result'] == "not find") {
						alert("玩家不存在");
					} else {
						$("#auth").text("認證");
						$("#authenticate").hide();
						alert("修改成功");
					}
				},
				error : function() {
					alert("系統發生錯誤!請稍後重試或聯絡客服人員");
				}
			});
		}

		function doCancelPayStatus() {
			
			if($("#cancelPayStatus").val() == "退單"){
				var result = confirm("你確定要退單嗎?");
				
				if(!result){
					return;
				}
			}
			
			var id = "${paymentForView.id}";
			var payStatus = $("#payStatus").val();
			var newPayStatus;
			if ($("#cancelPayStatus").val() == "不接單") {
				newPayStatus = 1;
			} else if ($("#cancelPayStatus").val() == "交易進行中取消") {
				newPayStatus = 7;
			} else {
				newPayStatus = 9;
			}

			$.ajax({
				url : "${ctx}/tccpaypayment/tccpaypayment!cancelPayStatus",
				type : "POST",
				dataType : "json",
				data : "id=" + id + "&nowPayStatus=" + payStatus
						+ "&newPayStatus=" + newPayStatus,
				async : false,
				success : function(data) {
					var message = data['message'];
					if (message != null) {
						alert(message);
					} else if (data['result']) {
						$("#payStatus").hide();
						if (newPayStatus == 1) {
							$("#showPayStatus").text("不接單");
						} else if (newPayStatus == 7) {
							$("#showPayStatus").text("交易進行中取消");
						} else if (newPayStatus == 9) {
							$("#showPayStatus").text("退單");
						}
						$("#changePayStatus").hide();
						$("#cancelPayStatus").hide();
						alert("修改成功");
					}
				},
				error : function() {
					alert("系統發生錯誤!請稍後重試或聯絡客服人員");
				}
			});
		}

		function doChangePayStatus() {
			var id = "${paymentForView.id}";
			var payStatus = $("#payStatus").val();
			var payOrderId = "${paymentForView.payOrderId}";
			var carId = "";

			if (payStatus == 2) {
				var c = $('input[name="channel"]:checked').val()
				if (c == "" || typeof c == "undefined") {
					alert("請先選擇渠道！");
					return;
				}
			}

			$.ajax({
				url : "${ctx}/tccpaypayment/tccpaypayment!updatePayStatus",
				type : "POST",
				dataType : "json",
				data : "id=" + id + "&nowPayStatus=" + payStatus
						+ "&payOrderId=" + payOrderId + "&carId=" + carId,
				async : false,
				success : function(data) {
					var newPayStatus = data['newPayStatus'];
					var message = data['message'];
					if (newPayStatus == 2) {
						$("#payStatus").val(2);
						$("#showPayStatus").text("接單");
						$("#changePayStatus").show();
						$("#changePayStatus").val("已派遣");
						$("#cancelPayStatus").show();
						$("#cancelPayStatus").val("交易進行中取消");
						alert("修改成功");
					} else if (newPayStatus == 3) {
						$("#payStatus").val(3);
						$("#showPayStatus").text("已派遣");
						$("#changePayStatus").val("已收款");
						$("#cancelPayStatus").val("交易進行中取消");
						alert("修改成功");
					} else if (newPayStatus == 4) {
						$("#payStatus").val(4);
						$("#showPayStatus").text("已收款");

						if ("${sendPoint}" == 1) {
							$("#changePayStatus").val("發點");
							$("#cancelPayStatus").val("交易進行中取消");
						} else {
							$("#changePayStatus").hide();
							$("#cancelPayStatus").val("退單");
						}
						alert("修改成功");
					} else if (newPayStatus == 5) {
						$("#payStatus").val(5);
						$("#showPayStatus").text("已發點");
						$("#cancelPayStatus").val("退單");
						$("#changePayStatus").hide();
						alert("修改成功");
					} else if (newPayStatus == 6) {
						$("#payStatus").val(6);
						$("#showPayStatus").text("發點失敗");
						$("#changePayStatus").text("發點");
						$("#cancelPayStatus").val("退單");
						alert(message);
					} else {
						alert(message);
					}
				},
				error : function() {
					alert("系統發生錯誤!請稍後重試或聯絡客服人員");
				}
			});
		}

		function validateForm() {
			if ("${paymentForView.invoiceType}" != "-1"
					&& $("select[name='invoiceType']").val() == -1) {
				return confirm("確定取消發票種類？");
			}

			if ($("select[name='invoiceType']").val() == -1) {
				if ($("input[name='invoiceName']").val() != ""
						|| $("input[name='invoicePhone']").val() != ""
						|| $("input[name='invoiceAddress']").val() != ""
						|| $("input[name='invoiceTitle']").val() != ""
						|| $("input[name='taxId']").val() != ""
						|| $("input[name='invoiceNo']").val() != "") {
					return confirm("有設定發票相關訊息，但無設定發票種類，確定要執行嗎？");
				}
			}
		}
	</script>

</body>
</html>
