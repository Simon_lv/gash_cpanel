<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台 訂單查詢</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/multiple-select/multiple-select.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="${ctx}/js/jquery-1.7.2.min.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/sisyphus.min.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/multiple-select/jquery.multiple.select.js"></script>  	
<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	function check() {
		var carId = $("#carId").val();
		
		if(isNaN(carId)) {
			alert("車輛隊編");
			return false;
		}
	}
	$( function() { 
		$("#search").click(function(){
            $("form[name='search']").attr('action','${ctx}/tccpaypayment/tccpaypayment!list.action');
            document.forms[0].submit();
        });
        $("#exportExcel").click(function(){
            $("form[name='search']").attr('action','${ctx}/tccpaypayment/tccpaypayment!export.action');
            document.forms[0].submit();
        });
        $("select[name='payStatus']").multipleSelect({
    		selectAllText : "全選",
    		allSelected : "全部",
    		countSelected : "#個已選擇",
    		minimumCountSelected : 1
    	});
        
        var payStatus = "${map['payStatus']}";
        
        if(payStatus != "") {
        	$("select[name='payStatus']").multipleSelect("setSelects", "${map['payStatus']}");
        }
        $( "form" ).sisyphus(); 
    } );
    <c:if test="${not empty map.reset}"> 
        $( function() { 
            $( "form" ).sisyphus().manuallyReleaseData();
            $("form[name='search']")[0].reset();
            //alert("reset");
        } );
    </c:if>
</script>

</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">
<form action="${ctx}/tccpaypayment/tccpaypayment!list.action" name="search" method="post" >
						<table>
							<tr>
								<td>金流方訂單編號：<input type="text" name="payOrderId" value="${map['payOrderId']}" /> &nbsp;&nbsp;&nbsp;&nbsp; 訂單狀態： <select
									name="payStatus" multiple="multiple">
										<option value="0">待接單</option>
										<option value="1">不接單</option>
										<option value="2">接單</option>
										<option value="3">已派遣</option>
										<option value="4">已收款</option>
										<option value="5">已發點</option>
										<option value="6">發點失敗</option>
										<option value="7">交易進行中取消</option>
										<option value="9">退單</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp; 回存狀態： <select name="restoreStatus" value="${map['restoreStatus']}">
										<option value="-1" <c:if test="${-1 == map['restoreStatus']}">selected</c:if>></option>
										<option value="1" <c:if test="${1 == map['restoreStatus']}">selected</c:if>>未回存</option>
										<option value="2" <c:if test="${2 == map['restoreStatus']}">selected</c:if>>已回存</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp;
								是否測試: <select name="isTest">
										<option value="-1" <c:if test="${-1 == map['isTest']}">selected</c:if>></option>
										<option value="0" <c:if test="${0 == map['isTest']}">selected</c:if>>否</option>
										<option value="1" <c:if test="${1 == map['isTest']}">selected</c:if>>是</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp; 金流方: <select name="payFrom">
										<option value="" selected="selected">全部</option>
										<c:forEach items="${payFromList}" var="p">
											<option value="${p}" <c:if test="${p == map['payFrom']}">selected="selected"</c:if>>${p}</option>
										</c:forEach>
								</select> &nbsp;&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
							<tr>
								<td>發票種類: <select name="invoiceType">
										<option value="-1" <c:if test="${-1 == map['invoiceType']}">selected="selected"</c:if>>全部</option>
										<option value="0" <c:if test="${0 == map['invoiceType']}">selected="selected"</c:if>>捐贈</option>
										<option value="2" <c:if test="${2 == map['invoiceType']}">selected="selected"</c:if>>二聯</option>
										<option value="3" <c:if test="${3 == map['invoiceType']}">selected="selected"</c:if>>三聯</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp; 發票編號：<input type="text" name="invoiceNo" value="${map['invoiceNo']}" /> &nbsp;&nbsp;&nbsp;&nbsp;
								渠道&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;：<select name="channel">
										<option value="" <c:if test="${empty map['channel']}">selected="selected"</c:if>></option>
										<option value="0" <c:if test="${'0' == map['channel']}">selected="selected"</c:if>>牛番茄</option>
										<option value="1" <c:if test="${'1' == map['channel']}">selected="selected"</c:if>>大車隊</option>
										<option value="2" <c:if test="${'2' == map['channel']}">selected="selected"</c:if>>CTRL Media</option>
										<option value="3" <c:if test="${'3' == map['channel']}">selected="selected"</c:if>>傾國</option>
										<option value="4" <c:if test="${'4' == map['channel']}">selected="selected"</c:if>>GAIN RIGHT</option>
								</select> &nbsp;&nbsp;&nbsp;&nbsp; 訂單時間： <input type="text" id="startCreateTime" name="startCreateTime" value="${map['startCreateTime']}"
									size="20" style="vertical-align: middle" /> <img src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" id="trigger1"
									style="cursor: pointer; border: 1px solid red; vertical-align: middle" title="選擇日期" onmouseover="this.style.background='red';"
									onmouseout="this.style.background=''" align="middle"
									onmousemove="javascript:Calendar.setup({inputField	: 'startCreateTime',ifFormat: '%Y-%m-%d',button: 'trigger1' });" /> ~ <input
									type="text" id="endCreateTime" name="endCreateTime" value="${map['endCreateTime']}" size="20" style="vertical-align: middle" /> <img
									src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif" id="trigger2"
									style="cursor: pointer; border: 1px solid red; vertical-align: middle" title="選擇日期" onmouseover="this.style.background='red';"
									onmouseout="this.style.background=''" align="middle"
									onmousemove="javascript:Calendar.setup({inputField	: 'endCreateTime',ifFormat: '%Y-%m-%d',button: 'trigger2' });" />
								</td>
							</tr>
						</table>
						<div class="itemDiv" >
  	<input type="submit" value="查詢" id="search" />
  	<input type="button"  value="匯出EXCEL" id="exportExcel"/>
  </div>
</form>
</div> 
<h3>TWD面額總計：<fmt:formatNumber type="number" pattern="###,###.##" value="${twdPriceSum}" /></h3>
<h3>HKD面額總計：<fmt:formatNumber type="number" pattern="###,###.##" value="${hkdPriceSum}" /></h3>
<br />
<h3>&nbsp;&nbsp;購買紀錄</h3>

<table width="98%">
	<tr style="text-align: center;font-weight: bold;">
		<td>ID</td>
		<td>傾國訂單號</td>
		<td>金流方訂單號</td>
		<td>金流方</td>
		<td>金流方用戶ID</td>
		
		<td>遊戲名稱</td>
		<td>渠道</td>
		<td>儲值金額</td>
		<td>幣別</td>
		<td>玩家姓名</td>
		<td>用戶手機</td>
		<td>用戶認證狀態</td>
		
		<td>用戶地址</td>
		<td>收款狀況</td>
		<td>回存狀態</td>
		<td>訂單時間</td>
		<td>發票種類</td>
		<td>發票號碼</td>
		<td>是否測試</td>
		<td width="10%">備註</td>
		<td>操作</td>
	</tr>
	
	<s:iterator value="page.result">	
		<tr align="center" valign="middle"  <c:if test="${payStatus == 0}">bgcolor="#FFFF00"</c:if>>
			<td>${id}</td>
			<td>${orderId}</td>
			<td>${payOrderId}</td>
			<td>${payFrom}</td>
			<td>${uid}</td>
			
			<td>${gameName}</td>
			<td><c:choose>
					<c:when test="${channel == 0}">牛番茄</c:when>
					<c:when test="${channel == 1}">大車隊</c:when>
					<c:when test="${channel == 2}">CTRL Media</c:when>
					<c:when test="${channel == 3}">傾國</c:when>
					<c:when test="${channel == 4}">GAIN RIGHT</c:when>
				</c:choose></td>
			<td>${money}</td>
			<td>${currency}</td>
			<td>${name}</td>
			<td>${userMobile}</td>
			<td>
				<c:choose>
					<c:when test="${memberStatus == 1}">認證</c:when>
					<c:otherwise>未認證</c:otherwise>
				</c:choose>
			</td>
			
			<td>${userAddress}</td>
			<td>
				<c:choose>
					<c:when test="${payStatus == 0}">待接單</c:when>
					<c:when test="${payStatus == 1}">不接單</c:when>
					<c:when test="${payStatus == 2}">接單</c:when>
					<c:when test="${payStatus == 3}">已派遣</c:when>
					<c:when test="${payStatus == 4}">已收款</c:when>
					<c:when test="${payStatus == 5}">已發點</c:when>
					<c:when test="${payStatus == 6}">發點失敗</c:when>
					<c:when test="${payStatus == 7}">交易進行中取消</c:when>
					<c:when test="${payStatus == 9}">退單</c:when>
				</c:choose>
			</td>
			<td>
				<c:choose>					
					<c:when test="${restoreStatus == 2}">已回存</c:when>
					<c:otherwise>未回存</c:otherwise>
				</c:choose>
			</td>
			<td>${createTime}</td>
			<td>
				<c:choose>
					<c:when test="${invoiceType == 0}">捐贈</c:when>
					<c:when test="${invoiceType == 2}">二聯</c:when>
					<c:when test="${invoiceType == 3}">三聯</c:when>
				</c:choose>
			</td>
			<td>${invoiceNo}</td>
			<td>
				<c:choose>					
					<c:when test="${isTest == 1}">是</c:when>
				</c:choose>
			</td>
			<td>${remark}</td>
			<td><a href="${ctx}/tccpaypayment/tccpaypayment!edit.action?id=${id}">詳細</a></td>
		</tr>
	</s:iterator>
</table>
<div class="filter">
 第${page.pageNo}頁, 共${page.totalPages}頁
	<s:if test="page.hasPre">
		<a href="${ctx}/tccpaypayment/tccpaypayment!list.action?page.pageNo=${page.prePage}&payOrderId=${map['payOrderId']}&payStatus=${map['payStatus']}&restoreStatus=${map['restoreStatus']}&isTest=${map['isTest']}&startCreateTime=${map['startCreateTime']}&endCreateTime=${map['endCreateTime']}&invoiceType=${map['invoiceType']}&invoiceNo=${map['invoiceNo']}&payFrom=${map['payFrom']}&channel=${map['channel']}">上一頁</a>
	</s:if>
	<s:if test="page.hasNext">
	    <a href="${ctx}/tccpaypayment/tccpaypayment!list.action?page.pageNo=${page.nextPage}&payOrderId=${map['payOrderId']}&payStatus=${map['payStatus']}&restoreStatus=${map['restoreStatus']}&isTest=${map['isTest']}&startCreateTime=${map['startCreateTime']}&endCreateTime=${map['endCreateTime']}&invoiceType=${map['invoiceType']}&invoiceNo=${map['invoiceNo']}&payFrom=${map['payFrom']}&channel=${map['channel']}">下一頁</a>
	</s:if>
	, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
</div>
</div>

</div>
</div>
</body>
</html>
