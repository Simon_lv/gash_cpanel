<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>傾國科技網站管理後台 訂單查詢</title>
<%@ include file="/common/meta.jsp"%>
<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jquery.js"></script>
<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css"
	rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
<script language="javascript" type="text/javascript"
	src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
<script language="javascript" type="text/javascript"
	src="${ctx}/js/sisyphus.min.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">
.itemDiv {
	height: 30px;
	line-height: 30px;
}

.deleted {
	color: red;
}

.active {
	color: black;
}
}
</style>
<script type="text/javascript">
	function check() {
		var carId = $("#carId").val();

		if (isNaN(carId)) {
			alert("車輛隊編");
			return false;
		}
	}
	$(function() {
		$("#search").click(
				function() {
					$("form[name='search']").attr('action',
							'${ctx}/tccpaypayment/tccpaypayment!listBonushunter.action');
					document.forms[0].submit();
				});
		$("#exportExcel")
				.click(
						function() {
							$("form[name='search']")
									.attr('action',
											'${ctx}/tccpaypayment/tccpaypayment!exportThird.action');
							document.forms[0].submit();
						});
		$("form").sisyphus();
	});
	<c:if test="${not empty map.reset}">
	$(function() {
		$("form").sisyphus().manuallyReleaseData();
		$("form[name='search']")[0].reset();
	});
	</c:if>
</script>

</head>
<body>
	<div id="container">
		<jsp:include page="../include_header.jsp" />
		<div id="mainContent">
			<div id="myContent">
				<div id="filter">
					<form action="${ctx}/tccpaypayment/tccpaypayment!listBonushunter.action"
						name="search" method="post">
						<div class="itemDiv">
							訂單編號：<input type="text" name="payOrderId"
								value="${map['payOrderId']}" /> &nbsp;&nbsp;&nbsp;&nbsp; 訂單狀態： <select
								name="payStatus">
								<option value="-1"
									<c:if test="${-1 == map['payStatus']}">selected</c:if>></option>
								<option value="0"
									<c:if test="${0 == map['payStatus']}">selected</c:if>>待接單</option>
								<option value="1"
									<c:if test="${1 == map['payStatus']}">selected</c:if>>不接單</option>
								<option value="2"
									<c:if test="${2 == map['payStatus']}">selected</c:if>>接單</option>
								<option value="3"
									<c:if test="${3 == map['payStatus']}">selected</c:if>>已派遣</option>
								<option value="4"
									<c:if test="${4 == map['payStatus']}">selected</c:if>>已收款</option>
								<option value="5"
									<c:if test="${5 == map['payStatus']}">selected</c:if>>已發點</option>
								<option value="6"
									<c:if test="${6 == map['payStatus']}">selected</c:if>>發點失敗</option>
								<option value="7" 
									<c:if test="${7 == map['payStatus']}">selected</c:if>>交易進行中取消</option>
								<option value="9"
									<c:if test="${9 == map['payStatus']}">selected</c:if>>退單</option>
							</select> &nbsp;&nbsp;&nbsp;&nbsp; 
							<!-- 金流方: --><input type="hidden" name="payFrom" value="bonushunter" />
							
						</div>
						<div class="itemDiv">
							訂單時間： <input type="text" id="startCreateTime"
								name="startCreateTime" value="${map['startCreateTime']}"
								size="20" style="vertical-align: middle" /> <img
								src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif"
								id="trigger1"
								style="cursor: pointer; border: 1px solid red; vertical-align: middle"
								title="選擇日期" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''" align="middle"
								onmousemove="javascript:Calendar.setup({inputField	: 'startCreateTime',ifFormat: '%Y-%m-%d',button: 'trigger1' });" />

							~ <input type="text" id="endCreateTime" name="endCreateTime"
								value="${map['endCreateTime']}" size="20"
								style="vertical-align: middle" /> <img
								src="${pageContext.request.contextPath}/js/jscalendar-1.0/img.gif"
								id="trigger2"
								style="cursor: pointer; border: 1px solid red; vertical-align: middle"
								title="選擇日期" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''" align="middle"
								onmousemove="javascript:Calendar.setup({inputField	: 'endCreateTime',ifFormat: '%Y-%m-%d',button: 'trigger2' });" />
							是否測試:
					   		<select name="isTest">
					   			<option value="-1" <c:if test="${-1 == map['isTest']}">selected</c:if> ></option>
					   			<option value="0" <c:if test="${0 == map['isTest']}">selected</c:if> >否</option>
					   			<option value="1" <c:if test="${1 == map['isTest']}">selected</c:if> >是</option>
					   		</select>  &nbsp;&nbsp;&nbsp;&nbsp;
						</div>
						<div class="itemDiv">
							<input type="submit" value="查詢" id="search" /> <input
								type="button" value="匯出EXCEL" id="exportExcel" />
						</div>
					</form>
				</div>

				<table width="98%">
					<tr style="text-align: center; font-weight: bold;">
						<td>傾國訂單號</td>
						<td>訂單編號</td>
						<td>儲值金額</td>
						<td>幣別</td>
						<td>玩家姓名</td>
						<td>用戶地址</td>
						<td>訂單狀態</td>
						<td>訂單時間</td>
						<td>是否測試</td>
						<td width="10%">備註</td>
					</tr>

					<s:iterator value="page.result">
						<tr align="center" valign="middle"  <c:if test="${payStatus == 0}">bgcolor="#FFFF00"</c:if>>
							<td>${orderId}</td>
							<td>${payOrderId}</td>
							<td>${money}</td>
							<td>${currency}</td>
							<td>${name}</td>
							<td>${userAddress}</td>
							<td><c:choose>
									<c:when test="${payStatus == 0}">待接單</c:when>
									<c:when test="${payStatus == 1}">不接單</c:when>
									<c:when test="${payStatus == 2}">接單</c:when>
									<c:when test="${payStatus == 3}">已派遣</c:when>
									<c:when test="${payStatus == 4}">已收款</c:when>
									<c:when test="${payStatus == 5}">已發點</c:when>
									<c:when test="${payStatus == 6}">發點失敗</c:when>
									<c:when test="${payStatus == 7}">交易進行中取消</c:when>
									<c:when test="${payStatus == 9}">退單</c:when>
								</c:choose></td>
							<td>${createTime}</td>
							<td>
								<c:choose>					
									<c:when test="${isTest == 1}">是</c:when>
								</c:choose>
							</td>
							<td>${remark}</td>
						</tr>
					</s:iterator>
				</table>
				<div class="filter">
					第${page.pageNo}頁, 共${page.totalPages}頁
					<s:if test="page.hasPre">
						<a
							href="${ctx}/tccpaypayment/tccpaypayment!listBonushunter.action?page.pageNo=${page.prePage}&payOrderId=${map['payOrderId']}&payStatus=${map['payStatus']}&isTest=${map['isTest']}&startCreateTime=${map['startCreateTime']}&endCreateTime=${map['endCreateTime']}&payFrom=${map['payFrom']}">上一頁</a>
					</s:if>
					<s:if test="page.hasNext">
						<a
							href="${ctx}/tccpaypayment/tccpaypayment!listBonushunter.action?page.pageNo=${page.nextPage}&payOrderId=${map['payOrderId']}&payStatus=${map['payStatus']}&isTest=${map['isTest']}&startCreateTime=${map['startCreateTime']}&endCreateTime=${map['endCreateTime']}&payFrom=${map['payFrom']}">下一頁</a>
					</s:if>
					, 總計：${page.totalCount}筆 ,每頁${page.pageSize}筆
				</div>
			</div>

		</div>
	</div>
</body>
</html>
