<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理後台  Gash點卡申請</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jquery.js"></script>
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css" rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/calendar-setup.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/jscalendar-1.0/lang/calendar-big5-utf8.js"></script>
	<script language="javascript" type="text/javascript" src="${ctx}/js/sisyphus.min.js"></script>

<style type="text/css">
	.itemDiv{height:30px;line-height:30px;}
	.deleted{color: red;}
	.active{color: black;}
}
</style>
<script type="text/javascript">
	$(function(){
		$("#confirm").click(function(){
			  if('' == $("input[name='invoiceNo']").val()) {
				  alert("請輸入發票號碼");
				  return;
			  }
			  
			  if($("input[name='invoiceNo']").val().length < 10) {
				  alert("發票號碼至少10碼");
				  return;
			  }
			  
			  if('' == $("input[name='title']").val()) {
				  alert("請輸入發票用途");
				  return;
			  }
			  
			  var pointCount = 0;
			  for(var i = 0; i < $(".usedCount").length; i++) {
				  if($(".usedCount")[i].value > 0) {
				  	pointCount = pointCount + 1;
				  }
				  if(parseInt($(".usedCount")[i].value) > parseInt($(".usedCount")[i].max)) {
					  alert("面額:"+$(".usedCount")[i].name+"，銷售數量超過剩餘數量");
					  return;
				  }
			  }
			  
			  if(pointCount == 0) {
				  alert("請填入需要的銷售數量");
				  return;
			  }
			  
			  $.ajax({
					url:"${ctx}/soldpoint/soldpoint!checkInvoiceNo.action",
					dataType:"json",
					data:{
						"invoiceNo":$("input[name='invoiceNo']").val()
						<c:forEach items="${list}" var="c">
						,"${c.price}":$("input[name='${c.price}']").val()
						</c:forEach>
					},
					async:false,
					success:function(data){
						if(data.flag){
							alert("發票號碼已使用過！");
							return;
						} if(!data.flag2) {
							alert("點卡剩餘數量不足將重新整理畫面已顯示最新數量");
							window.location.href = "${ctx}/soldpoint/soldpoint!list.action?reset=true";
						} else {
							$("#search").submit();
						}
					},
					error:function(){
						alert("發生錯誤，請聯系管理員！");
					}
				
				});
			  
			  
			  
		});
		$("#reset").click(function(){
			window.location.href = "${ctx}/soldpoint/soldpoint!list.action?reset=true";
		});
	});
</script>
</head>
<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="filter">
<form action="${ctx}/soldpoint/soldpoint!saveInvoice.action" id="search" method="post">
   
    <div class="itemDiv" > 
    	發票號碼：<input type="text" name="invoiceNo" value="" />
    	發票用途：<input type="text" name="title" value="" style="width: 40%"/><br/>
    	<input id="confirm" value="確定" type="button" /><input id="reset" value="重新整理" type="button" />
    </div>
    
    <div class="itemDiv" >
    	<br />
    	<br />
	    <h3>&nbsp;&nbsp;選擇點卡</h3>
	
		<table width="98%">
			<tr style="text-align: center;font-weight: bold;">
				<td>面額</td>
				<td>剩餘數量</td>
				<td>銷售數量</td>
			</tr>
			<c:forEach items="${list}" var="c">
				<tr align="center" valign="middle">
					<td>${c.price}</td>
					<td>${c.count}</td>
					<td><input name="${c.price}" type="number" class="usedCount" min="0" max="${c.count}" /></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</form>
</div> 

</div>
</div>
</div>
</body>
</html>
