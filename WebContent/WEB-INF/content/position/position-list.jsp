<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理系統 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/jscalendar-1.0/calendar-win2k-1.css"rel="stylesheet" type="text/css" />
	<script language="javascript" type="text/javascript"src="${ctx}/js/jquery.js"></script>
	<script type="text/javascript">
    function deleteTask(id,status){
        $.ajax({ 
                 url: "/position/position!updatePositionStatus.action", 
                 dataType:"json",
                 data:"id="+id+"&status="+status,
                 success: function(data){
                     if(data==2){
                         alert("操作成功！");
                         document.getElementById("del_"+id).innerHTML="<span id='del_"+id+"'><a href='javascript:deleteTask("+id+",1)'><font color='red'>啟用</font></a></span>";
                     }else if(data==1){
                         alert("操作成功！");
                         document.getElementById("del_"+id).innerHTML="<span id='del_"+id+"'><a href='javascript:deleteTask("+id+",2)'><font color='green'>停權</font></a></span>";
                     }else{
                         alert("操作失敗！");
                     }
           },
           error:function(){
               alert("發生錯誤，請聯系管理員！");
           }   
        });
    }
	
	
	
		   function newPosition(){
			   window.location.href = "${ctx}/position/position!addPosition.action";
		   }
    </script>

<style>
	.itemDiv{height:30px;line-height:30px;}
</style>
</head>

<body>
<div id="container">
<jsp:include page="../include_header.jsp"/>
<div id="mainContent">
<div id="myContent">
<div id="message"><s:actionmessage theme="mytheme"/></div>
<div id="filter">
<br/>
<h3>權限管理</h3>
</div> 

<input type="button" value="新增" onclick="newPosition()"/>

<table width="60%">
	<tr style="text-align: center;background: #ADD2DA">
		<td>職務權限</td>
		<td>變更權限</td>
		<td>管理</td>
	</tr>

	
	      <c:forEach items="${list}" var="p">
			<tr>
				<td style="text-align: center;">${p.name}</td>
				<td align="center"><a href="${ctx}/position/position!queryPositionAuthInfo.action?id=${p.id}&rolesName=${p.name}">修改</a></td>
                <td align="center">
                    <c:if test="${p.status==1}"><span id="del_${p.id}"><a href="javascript:deleteTask('${p.id}',2)"><font color="green">停權</font></a></span></c:if>
                    <c:if test="${p.status==2}"><span id="del_${p.id}"><a href="javascript:deleteTask('${p.id}',1)"><font color="red">啟用</font></a></span></c:if>
                </td>
			</tr>
	    </c:forEach>
</table>
</div>
</div>
<jsp:include page="../include_footer.jsp"/>
</div>
</body>
</html>
