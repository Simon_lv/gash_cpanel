<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>傾國科技網站管理系統 帳號管理</title>
	<%@ include file="/common/meta.jsp"%>
	<link href="${ctx}/css/default.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/validate/jquery.validate.css" type="text/css" rel="stylesheet" />
	<link href="${ctx}/js/validate/jquery.validate.password.css" type="text/css" rel="stylesheet" />
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/jquery.validate.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/jquery.validate.password.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/messages_cn.js" type="text/javascript"></script>
	<script type="text/javascript">
	   /*选中已经选择的权限*/
		$(document).ready(function(){
			 <c:forEach items="${pa}" var="a">
			    var checkId = ${a.authorityId};
			    $("#"+checkId).attr("checked",true);
			 </c:forEach>
		});
	</script>
</head>

<body>
<div id="container">
<div id="myContent">
<h3>修改職務權限</h3>
<div id="inputContent">
<form id="roleForm" action="${ctx}/position/position!updatePositionAuthInfo.action" method="post">
<input type="hidden" name="id" value="${id}" />
<table class="inputView">
	<tr>
		<td width="100px">職務權限:</td>
		<td>${name}</td>
	</tr>
    <c:forEach items="${parentList}" var="parent">
    <tr>
      <td colspan="2" style="background: #ADD2DA"><c:out value="${parent.displayName}"/></td>
    </tr>
    <tr>
      <td colspan="2" >
      		<%
          		int index = 0;
      		%>
      <c:forEach items="${childList}" var="item" varStatus="counter">
      	  <c:if test="${item.pid==parent.id}">
          <input type="checkbox" value="${item.id}" id="${item.id}" name="authority"/>${item.displayName}
      		<%
          		index += 1;
      			if(index % 4 == 0) {
     		%>
      				 <br>
      		<%
      			}
      		%> 
          </c:if>
      </c:forEach>
      </td>
    </tr>
    </c:forEach>
	<tr>
		<td colspan="2">
            <input type="button" value="取消" onclick="javascript:history.go(-1);" /> &nbsp;&nbsp;
			<input type="submit" value="送出"/>
		</td>
	</tr>
</table>
</form>

</div>
</div>
<jsp:include page="../include_footer.jsp"/>
</div>
</body>
</html>