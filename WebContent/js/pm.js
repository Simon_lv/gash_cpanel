function PM ( objSelf )
{
	this.objSelf = objSelf;
	this.checkPMInterval = 0;
	this.objPM = null;
	this.pmId = 0;

	this.objCheckPM = null;

	// 初始化站内消息窗口
	this.initPM = function ( obj )
	{
		if ( typeof (obj) == 'string' )
		{
			obj = document.getElementById ( obj );
		}
		this.objPM = obj;
		if ( this.objCheckPM != null && this.objCheckPM.innerHTML != '' )
		{
			this.resetNewPM ();
			hideItem ( this.objCheckPM );
		}		
	}

	this.openPMBox = function ( action, queryString )
	{
		var nowTime = new Date ();
		var pmAjax = new Ajax ( '/main?mod=pm&act=' + action + '&timestamp=' + nowTime.getTime (), queryString, this.callbackPM, eval(this.objSelf) );
		pmAjax.get ();
	}

	// 删除消息
	this.removePM = function ( action, objForm )
	{
		if ( typeof (objForm) == 'string' )
		{
			objForm = document.getElementById ( objForm );
		}
		var pmAjax = new Ajax ( '/main?mod=pm&act=' + action + '&op=remove', objForm, this.callbackPM, eval(this.objSelf) );
		pmAjax.post ();
	}

	// 回调函數
	this.callbackPM = function ( Ret, obj )
	{
		var ErrorMsg = getError ( Ret );
		if ( ErrorMsg )
		{
			showMessage ( ErrorMsg );
		}
		else if ( Ret != '' )
		{
			showMessage ( Ret, obj.objPM );
			showItem ( obj.objPM );
			centerDiv ( obj.objPM );
		}
	}

	// 發送消息
	this.sendPM = function ( form )
	{
		if ( form.pm_to_username.value == '' )
		{
			showMessage ( "請填寫發送對象的用户名!" );
			form.pm_to_username.focus ();
			return false;
		}
		if ( form.pm_title.value == '' )
		{
			showMessage ( "請填寫消息标题!" );
			form.pm_title.focus ();
			return false;
		}
		if ( form.pm_content.value == '' )
		{
			showMessage ( "請填寫消息内容!" );
			form.pm_content.focus ();
			return false;
		}
		var pmAjax = new Ajax ( '/main?mod=pm&act=write', form, this.callbackSendPM, eval(this.objSelf) );
		pmAjax.post ();
	}

	// 發送消息回调函數
	this.callbackSendPM = function ( Ret, obj )
	{
		var ErrorMsg = getError ( Ret );
		if ( ErrorMsg )
		{
			showMessage ( ErrorMsg );
		}
		else if ( Ret != '' )
		{
			showMessage ( Ret );
			obj.openPMBox ( 'sendbox' );
		}
	}

	// 检查最新消息
	this.checkPM = function ( Interval, obj )
	{
		if ( Interval > 0 )
		{
			if ( typeof (obj) == 'string' )
			{
				obj = document.getElementById ( obj );
			}
			this.checkPMInterval = Interval;
			this.objCheckPM = obj;
			var checkPMAjax = new Ajax ( '/main?mod=pm&act=checkpm', 'obj=' + obj.id, this.callbackCheckPM, this.objSelf );
			checkPMAjax.post ();
		}
	}

	// 重置最新消息计數
	this.resetNewPM = function ()
	{
		var checkPMAjax = new Ajax ( '/main?mod=pm&act=resetnewpm' );
		checkPMAjax.post ();
	}

	// 检查最新消息回调函數
	this.callbackCheckPM = function ( Ret, objName )
	{
		var ErrorMsg = getError ( Ret );
		if ( !ErrorMsg )
		{
			var obj = eval(objName);
			if ( Ret != '' )
			{
				obj.objCheckPM.innerHTML = Ret;
				showItem ( obj.objCheckPM );
				centerDiv ( obj.objCheckPM );
			}
			if ( obj.checkPMInterval > 0 )
			{
				setTimeout ( objName + ".checkPM (" + obj.checkPMInterval + ", '" + obj.objCheckPM.id + "')", obj.checkPMInterval * 1000 );
			}
		}
	}

	// 接受入會邀請
	this.guildInviteAccept = function ( siteId, pmId )
	{
		this.pmId = pmId;
		var clsAjax = new Ajax ( '/main?mod=operation&act=dealinvite&op=accept', 'siteid=' + siteId + '&pmid=' + pmId, this.callbackDeal, eval(this.objSelf) );
		clsAjax.post ();
	}

	// 拒绝入會邀請
	this.guildInviteRefuse = function ( siteId, pmId )
	{
		this.pmId = pmId;
		var clsAjax = new Ajax ( '/main?mod=operation&act=dealinvite&op=refuse', 'siteid=' + siteId + '&pmid=' + pmId, this.callbackDeal, eval(this.objSelf) );
		clsAjax.post ();
	}
	
	// 通過加入申請
	this.guildJoinAccept = function ( siteId, userId, pmId )
	{
		this.pmId = pmId;
		var clsAjax = new Ajax ( '/main?mod=operation&act=dealjoins&op=accept', 'siteid=' + siteId + '&userid=' + userId + '&pmid=' + pmId, this.callbackDeal, eval(this.objSelf) );
		clsAjax.post ();
	}

	// 拒绝加入申請
	this.guildJoinRefuse = function ( siteId, userId, pmId )
	{
		this.pmId = pmId;
		var clsAjax = new Ajax ( '/main?mod=operation&act=dealjoins&op=refuse', 'siteid=' + siteId + '&userid=' + userId + '&pmid=' + pmId, this.callbackDeal, eval(this.objSelf) );
		clsAjax.post ();
	}

	// 短信附加操作處理回调函數
	this.callbackDeal = function ( Ret, obj )
	{
		ajaxCallback ( Ret );
		obj.openPMBox ( 'recvbox', 'pmid=' + obj.pmId );
	}
}

var clsPM = new PM ( 'clsPM' );