setCookie = function ( name, value, noExpire )
{
	if ( noExpire != null && noExpire )
	{
		var expires = new Date();
		expires.setTime(expires.getTime() + (1000 * 86400 * 365));
		var stringExpires = expires.toGMTString ();
	}
	document.cookie = name + "=" + escape(value) + "; expires=" + stringExpires +  "; path=/";
};

getCookie = function ( name )
{
	cookie_name = name + "=";
	cookie_length = document.cookie.length;
	cookie_begin = 0;
	while (cookie_begin < cookie_length)
	{
		value_begin = cookie_begin + cookie_name.length;
		if (document.cookie.substring(cookie_begin, value_begin) == cookie_name)
		{
			var value_end = document.cookie.indexOf (";", value_begin);
			if (value_end == -1)
			{
				value_end = cookie_length;
			}
			return unescape(document.cookie.substring(value_begin, value_end));
		}
		cookie_begin = document.cookie.indexOf(" ", cookie_begin) + 1;
		if (cookie_begin == 0)
		{
			break;
		}
	}
	return null;
};

delCookie = function ( name )
{
	var expireNow = new Date();
	document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT" +  "; path=/";
};

showTopHtml = function ()
{
	var html = '';
	html += '<style>';
	html += '.header { background:url(images/9wee_topbg.gif) repeat-x; width:100%; height:78px;}';
	html += '.header_box { width:1000px; margin:0 auto;}';
	html += '.header_left { float:left; width:450px; height:78px; background:url(images/9wee_topbgt.gif) no-repeat right;}';
	html += '.header_logo { float:left; width:195px; padding-top:16px;}';
	html += '.header_about { float:left; color:#676363; height:28px; line-height:22px; background:url(images/9wee_www.gif) no-repeat left bottom; padding-top:27px;}';
	html += '.header_right { float:left; width:550px; line-height:22px;}';
	html += '.header_right a { padding:0 4px;}';
	html += '.header_url { width:544px; color:#858484; padding:8px 0; text-align:right; padding-right:6px;}';
	html += '.header_login { width:550px; color:#181818;}';
	html += '.header_login li { float:left; padding-left:4px;}';
	html += '.header_input {border:1px solid #E8E3E9;  background:url(images/9wee_in_bg.gif) repeat-x top; padding:2px;}';
	html += '.nav { width:100%; height:35px; background:url(images/9wee_nav_b.gif) repeat-x;}';
	html += '.nav_box { width:1000px; margin:0 auto;}';
	html += '.nav_box li { float:left;}';
	html += '.nav_box a{ display:block; width:121px; height:35px; text-decoration:none; text-align:center; font-weight:bold; font-size:14px; line-height:35px;}';
	html += '.nav_on a:link,.nav_on a:visited{color:#4d4b4b; background:url(images/9wee_nav_c.gif) no-repeat bottom center;}';
	html += '.nav_on a:hover,.nav_on a:active{color:#920000; text-decoration:none;}';
	html += '.nav_off a:link,.nav_off a:visited{color:#FFF; background:url(images/9wee_nav_a.gif) no-repeat left 1px; }';
	html += '.nav_off a:hover,.nav_off a:active{color:#FFF; background:url(images/9wee_nav_d.gif) no-repeat left top; text-decoration:none;}';
	html += '</style>';

	html += '	<div class="header">' ;
	html += '		<div class="header_box">' ;
	html += '			<div class="header_left">' ;
	html += '				<div class="header_logo"><a href="#"><img src="../script/images/9wee_logo.gif" border="0" alt="九维互動首頁" /></a></div>' ;
	html += '				<div class="header_about">玩網頁遊戲 交知心朋友</div>' ;
	html += '			</div>' ;
	html += '			<div class="header_right">' ;
	html += '				<div class="header_url shi_a"><span style="color:#ff7c00">旗下網站：</span><a href="http://zq.9wee.com" target="_blank">武林足球</a>|<a href="http://sg.9wee.com" target="_blank">武林三國</a>|<a href="http://zq2.9wee.com" target="_blank">武林足球Ⅱ</a>|<a href="http://hero.9wee.com" target="_blank">武林英雄</a></div>' ;
	html += '				<div class="header_login" id="login_form">' ;
	html += '					用户數据载入中，請稍候...' ;
	html += '				</div>' ;
	html += '			</div>' ;
	html += '		</div>' ;
	html += '	</div>' ;
	html += '	<div class="nav">' ;
	html += '		<div class="nav_box">' ;
	html += '			<ul id="top_9wee_navi">' ;
	html += '				<li class="nav_off" id="top_9wee_navi_0"><a href="http://www.9wee.com/">網站首頁</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_7"><a href="http://www.9wee.com/game">遊戲中心</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_1"><a href="https://passport.9wee.com/">用户中心</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_2"><a href="https://passport.9wee.com/payment/index.php">支付中心</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_3"><a href="http://cs.9wee.com/" target="_blank">客服中心</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_4"><a href="http://u.9way.cn/" target="_blank">推廣聯盟</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_5"><a href="http://u.9wee.com/" target="_blank">聯合運營</a></li>' ;
	html += '				<li class="nav_off" id="top_9wee_navi_6"><a href="http://bbs.9wee.com/">遊戲论坛</a></li>' ;
	html += '			</ul>' ;
	html += '		</div>' ;
	html += '	</div>' ;
	document.write ( html );

	if ( typeof ( topNaviIndex ) == 'undefined' ) topNaviIndex = 0;
	topNaviIndex = parseInt ( topNaviIndex );
	if ( !isNaN ( topNaviIndex ) )
	{
		// var naviMenus = document.getElementById( 'top_9wee_navi' ).getElementsByTagName ( 'li' );
		// naviMenus[topNaviIndex].className = 'nav_on';
		var navObj = document.getElementById ( 'top_9wee_navi_' + topNaviIndex );
		if ( navObj ) navObj.className = 'nav_on';
	}	
}

initLoginForm = function ( token )
{
	eval ( 'var ret = ' + token + ';' );

	var html = '';
	html += '<span style="float: right; margin-right: 5px">';

	if ( ret && ret.loginFlag )
	{
		if ( typeof ( passportLogoutUrl ) != 'string' )
		{
			passportLogoutUrl = 'https://passport.9wee.com/logout';
		}

		html += '歡迎您，<span style="color:#ff6600">' + ret.nickname + '</span>   ';
		html += '<a href="https://passport.9wee.com/profile" target="_blank">填寫資料</a>|';
		html += '<a href="https://passport.9wee.com/profile?step=2" target="_blank">修改郵箱</a>|';
		html += '<a href="https://passport.9wee.com/profile?step=3" target="_blank">修改密碼</a>|';
		html += '<a href="https://passport.9wee.com/secure" target="_blank" style="color:red">密碼保護</a>|';
		html += '<a href="javascript:;" onclick="this.href = \'' + passportLogoutUrl + '\'; delCookie(\'passportTokenCookie\')">退出通行證</a>';
	}
	else
	{
		html += '<form action="https://passport.9wee.com/login" method="post" onsubmit="delCookie(\'passportTokenCookie\')">';
		html += '	<ul>';
		html += '		<li>九维通行證：</li>';
		html += '		<li><input type="text" class="header_input" name="username" value="" style="width:100px; height:14px;" /></li>';
		html += '		<li>密碼：</li>';
		html += '		<li><input type="password" class="header_input" name="password" value="" style="width:100px; height:14px;" /></li>';
		html += '		<li class="c"><input name="" type="image" src="../script/images/9wee_login.gif"></li>';
		html += '		<li><a href="https://passport.9wee.com/register" target="_blank">註冊通行證</a>|<a href="https://passport.9wee.com/getpsw" target="_blank">找回密碼</a></li>';
		html += '	</ul>';
		html += '</form>';
	}
	html += '</span>';

	document.getElementById('login_form').innerHTML = html;
	setCookie ( 'passportTokenCookie', token );
};

showTopHtml ();
var passportTokenCookie = getCookie ( 'passportTokenCookie' );
if ( passportTokenCookie )
{
	initLoginForm ( passportTokenCookie );
}
else
{
	document.write ( '<script type="text/javascript" src="https://passport.9wee.com/api/js_user.php?callback=initLoginForm"><' + '/script>' );
}