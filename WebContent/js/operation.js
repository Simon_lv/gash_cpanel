/******* 前台操作类 *******/
function Operation ( objSelf )
{
	this.objSelf = objSelf;

	// 選擇头像
	this.avatarSelect = function ( target, obj, queryString )
	{
		if ( typeof (obj) == 'string' )
		{
			obj = document.getElementById ( obj );
		}
		if ( typeof (target) == 'string' )
		{
			target = document.getElementById ( target );
		}

		// 獲取當前鼠标坐标
		if ( queryString == null )
		{
			moveDivHere ( obj );
			queryString = '';
		}
		else
		{
			queryString = '?' + queryString;
		}
		obj.style.width = "580px";
		var optAjax = new Ajax ( '/avatarselect' + queryString, 'target=' + target.id + '&obj=' + obj.id, ajaxCallback, obj );
		optAjax.post ();
	}

	// 選中條目
	this.checkItem = function ( itemId, chkbox, list, addClass )
	{
		if ( typeof (chkbox) == 'string' )
		{
			chkbox = document.getElementById ( chkbox );
		}
		if ( typeof (list) == 'string' )
		{
			list = document.getElementById ( list );
		}

		if ( chkbox.value == '' )
		{
			chkbox.value = itemId;
			list.className += ' ' + addClass;
		}
		else
		{
			chkbox.value = '';
			list.className = list.className.replace ( addClass, '' );
		}
	}

	// 全選/全消
	this.checkAllItems = function ( objForm, chkboxPrefix, listPrefix, addClass )
	{
		if ( typeof (objForm) == 'string' )
		{
			objForm = document.getElementById ( objForm );
		}

		for ( var i = 0; i < objForm.elements.length; i ++ )
		{
			if ( objForm.elements[i].id.substring ( 0, chkboxPrefix.length ) == chkboxPrefix )
			{
				itemId = objForm.elements[i].id.substring ( chkboxPrefix.length, objForm.elements[i].id.length );
				this.checkItem ( itemId, chkboxPrefix + itemId, listPrefix + itemId, addClass );
			}
		}
	}

	// 显示预览图
	this.showPreview = function ( src, obj, width, height )
	{
		message = '<img alt="" src="../common/script/' + src + '" width="' + width + '" />';
		this.showMessage ( message, obj, width, height );
	}

	// 显示提示
	this.showMessage = function ( message, obj, width, height )
	{
		if ( typeof (obj) == 'string' )
		{
			obj = document.getElementById ( obj );
		}
		if ( obj.style.display != 'block' )
		{
			moveDivHere ( obj );
			obj.innerHTML = message;
			if ( width != null ) obj.style.width = width + 'px';
			if ( height != null ) obj.style.height = height + 'px';
			showItem ( obj );
		}
	}

	// 根据域名獲取 IP
	this.getDomainIP = function ( prefixDomain, prefixIP, Index, SystemIP )
	{
		var oDomain = document.getElementById ( prefixDomain + Index );
		if ( !oDomain )
		{
			return false;
		}
		document.getElementById ( prefixIP + Index ).innerHTML = "<span>正在獲取 IP 地址...</span>";

		var arrParams = new Array ();
		for ( var i = 0; i < this.getDomainIP.arguments.length; i ++ )
		{
			arrParams.push ( this.getDomainIP.arguments[i] );
		}
		arrParams.push ( eval ( this.objSelf ) );
		var optAjax = new Ajax ( '/main?mod=operation&act=gethostip', 'domain=' + oDomain.innerHTML, this.callbackGetDomainIP, arrParams );
		optAjax.post ();
	}

	this.callbackGetDomainIP = function ( Ret, arrParams )
	{
		var prefixDomain = arrParams[0];
		var prefixIP = arrParams[1];
		var Index = arrParams[2];
		var SystemIP = arrParams[3];
		var obj = arrParams[4];

		if ( Ret != SystemIP )
		{
			Ret = "<span class='checkError'>" + Ret + " ( 未正確解析 )</span>";
		}
		else
		{
			Ret = "<span class='checkRight'>" + Ret + " ( 已正確解析 )</span>";
		}
		document.getElementById ( prefixIP + Index++ ).innerHTML = Ret;
		obj.getDomainIP ( prefixDomain, prefixIP, Index, SystemIP );
	}
}

var clsOperation = new Operation ( 'clsOperation' );