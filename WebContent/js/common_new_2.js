<!--
/*var arrHost = window.location.host.split ( '.' );
var domain = arrHost[arrHost.length - 2] + '.' + arrHost[arrHost.length - 1];
if ( document.domain != domain )
{
	document.domain = domain;
}*/

function $ ( objId )
{
	return document.getElementById ( objId );
}

// 保存 Cookie
function setCookie ( name, value, expiresTime )
{
	expires = new Date();
	expires.setTime(expires.getTime() + (1000 * 86400 * 365));
	if (expiresTime != null)
	{
	    document.cookie = name + "=" + escape(value) + "; path=/";  
	}
	else
	{
		document.cookie = name + "=" + escape(value) + "; expires=" + expires.toGMTString() +  "; path=/";  
	}
}

// 獲取 Cookie
function getCookie ( name )
{
	cookie_name = name + "=";
	cookie_length = document.cookie.length;
	cookie_begin = 0;
	while (cookie_begin < cookie_length)
	{
		value_begin = cookie_begin + cookie_name.length;
		if (document.cookie.substring(cookie_begin, value_begin) == cookie_name)
		{
			var value_end = document.cookie.indexOf ( ";", value_begin);
			if (value_end == -1)
			{
				value_end = cookie_length;
			}
			return unescape(document.cookie.substring(value_begin, value_end));
		}
		cookie_begin = document.cookie.indexOf ( " ", cookie_begin) + 1;
		if (cookie_begin == 0)
		{
			break;
		}
	}
	return null;
}

// 清除 Cookie
function delCookie ( name )
{
	var expireNow = new Date();
	document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT" +  "; path=/";
}

// 獲取 URL 变量
function getURLVar ( queryVar, url )
{
	var url = url == null ? window.location.href : url;
	var re = new RegExp ( ".*[\?|&]" + queryVar + "=([^&#]+).*", "g" );
	if ( url.match ( re ) )
	{
		var queryVal = url.replace ( re, "$1" );
		return queryVal;
	}
	return '';
}

// 獲取遊戲域名
function getGameHost ()
{
	var host = window.location.host
	return ( 's1.' + host );
}

// 獲取當前域名
function getURLHost ( url )
{
	if ( url == null )
	{
		return window.location.host;
	}

	var re = new RegExp ( "([^\/]*)\/\/([^\/]*)\/.*", "g" );
	if ( url.match ( re ) )
	{
		var urlHost = url.replace ( re, "$1//$2" );
		return urlHost;
	}
	return '';
}

function setSelectOptions ( the_form, the_select, do_check )
{
    var selectObject = document.forms[the_form].elements[the_select];
    var selectCount  = selectObject.length;

    for (var i = 0; i < selectCount; i++) {
        selectObject.options[i].selected = do_check;
    }
    return true;
}

function setChecked ( val, obj, name )
{
	len = obj.elements.length;
	var i=0;
	for( i=0 ; i<len ; i ++ )
	{
		if ( obj.elements[i].name == name )
		{
			obj.elements[i].checked = val;
		}
	}
}

// 在指定對象显示信息
function showMessage ( msg, objName )
{
	var obj;
	if ( typeof (objName) == 'string' )
	{
		obj = document.getElementById ( objName );
	}
	else
	{
		obj = objName;
	}

	process.finish ();

	if ( obj )
	{
		obj.innerHTML = msg;
		if ( obj.className.indexOf ( 'dialog_box' ) >= 0 )
		{
			obj.style.zIndex = gzIndex;
			if ( msg.indexOf ( 'dialog.close' ) < 0 )
			{
				appendCloseButton ( obj );
			}
		}
		try { obj.scrollTop = 0; } catch (e) {}
	}
	else
	{
		showDialogBox ( msg, objName );
	}

	// 解析 Script
	execScript ( msg );
}

// 添加關閉按钮
function appendCloseButton ( obj, closeScript )
{
	var closeButton = document.createElement ( 'div' );
	closeButton.className = 'dialog_box_close';
	closeButton.onclick = function () {
		if ( closeScript != null ) eval ( closeScript );
		dialog.close(this);
	}
	closeButton.onmouseover = function () { this.style.marginTop = '1px'; }
	closeButton.onmouseout = function () { this.style.marginTop = '0px'; }
	obj.appendChild ( closeButton );
}

// 解析 Script
function execScript ( msg )
{
	var _re = /<script[^>]*>([^\x00]+)$/i
	var _msgs = msg.split ( "<\/script>" );
	for ( var _i in _msgs )
	{
		var _strScript;
		if ( _strScript = _msgs[_i].match ( _re ) )
		{
			var _strEval = _strScript[1].replace ( /<!--/, '' );
			try
			{
				eval ( _strEval );
			}
			catch (e) {}
		}
	}
}

// 移除對話框
function removeDialogBox ( objName )
{
	if ( objName == null )
	{
		objName = 'dialog_box' + mask.maskIndex;
	}
	mask.hide ();
	removeItems ( objName );
	gzIndex --;
}

// 移除物件
function removeItems ( objName )
{
	var obj;
	objs = getObjects ( objName );
	for ( var i in objs )
	{
		try
		{
			obj = objs[i];
			obj.parentNode.removeChild ( obj );
		}
		catch (e) {}
	}
}

// 显示提示窗口
var gzIndex = 999;
function showDialogBox ( msg, objId, noClose )
{
	var objClass = 'dialog_box';
	
	if ( objId == null )
	{
		objId = objClass + mask.maskIndex;
	}

	var msgBox = document.getElementById ( objId );
	if ( msgBox )
	{
		msgBox.parentNode.removeChild ( msgBox );
		mask.hide ();
	}
	if ( msg != '' ) mask.show ();
	msgBox = document.createElement ( 'div' );
	msgBox.id = objId;
	msgBox.className = objClass;
	var objBody = document.getElementsByTagName('body').item(0);
	objBody.appendChild ( msgBox );
	msgBox.style.zIndex = gzIndex ++;

	msgBox.innerHTML = msg;

	if ( noClose == null ) noClose = false;
	if ( !noClose && msg.indexOf ( 'dialog.close' ) < 0 )
	{
		appendCloseButton ( msgBox );
	}

	centerDiv ( msgBox );
}

// 字數限制函數
function lengthLimit ( obj, Limit, objShow, objAlert )
{
	var Len = obj.value.length;
	if ( Len > Limit )
	{
		obj.value = obj.value.substring ( 0, Limit );
		Len = Limit;
		showMessage ( String.sprintf ( "字數超出限制, 最多 %d 字!", Limit ), objAlert );
	}
	if ( objShow = document.getElementById ( objShow ) )
	{
		objShow.innerHTML = Len;
	}	
}

// 列表搜索
function clientSearch ( value, obj )
{
	if ( value != '' )
	{
		for ( var i = obj.selectedIndex + 1; i < obj.options.length; i ++ )
		{
			if ( obj.options[i].text.indexOf ( value, 0 ) >= 0 )
			{
				obj.selectedIndex = i;
				return true;
			}		
		}
	}
	obj.selectedIndex = 0;
}

// 列表項目移動
function moveOptions ( objFrom, objTo, errMsg, moveList )
{
	moveList = moveList != null ? moveList : '';
	moveList = ',' + moveList + ',';
	if ( objFrom.selectedIndex == -1 && errMsg != null )
	{
		alert ( errMsg );
	}
	for ( var i = 0; i < objFrom.options.length; i ++ )
	{
		if ( moveList.match ( ',-1,' ) || moveList.match ( ',' + objFrom.options[i].value + ',' ) || objFrom.options[i].selected )
		{
			objTo.options.add ( new Option ( objFrom.options[i].text, objFrom.options[i].value ) );
			objFrom.options[i--] = null;
		}
	}
}

// 設置状态栏
function setStatus ( w, id )
{
	window.status = w;
	return true;
}
function clearStatus ()
{
	window.status = '';
}

// 显示物件
function showItem ( obj, posX, posY )
{
	if ( typeof (obj) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		obj.style.display = '';
		if ( posX != null ) obj.style.left = posX + 'px';
		if ( posY != null ) obj.style.top = posY + 'px';
		obj.focus ();
	}
	catch (e)
	{
	}
}

// 隐藏物件
function hideItem ( obj )
{
	if ( typeof (obj) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		obj.style.display = 'none';
	}
	catch (e)
	{
	}	
}

// 交替显示/隐藏物件
function itemShowHide ( obj )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		if ( obj.style.display != '' )
		{
			showItem ( obj );
		}
		else
		{
			hideItem ( obj );			
		}
	}
	catch (e)
	{
	}
}

// 獲取错误信息
function getError ( string )
{
	var errorFlag = '<ERROR>';
	if ( string.substring ( 0, errorFlag.length ) == errorFlag )
	{
		return string.substring ( errorFlag.length, string.length );
	}
	else
	{
		return false;
	}
}

// 根据标签獲取物件
function getTag ( obj, tagName, index )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	var tags = obj.getElementsByTagName ( tagName );
	if ( index != null )
	{
		return tags[index];
	}
	else
	{
		return tags;
	}
}

// Ajax 通用回调函數
function ajaxCallback ( ret, obj )
{
	var errorMsg = getError ( ret );
	if ( errorMsg )
	{
		if ( !window.onErrorMsg )
		{
			window.onErrorMsg = true;
			showMessage ( errorMsg );
		}
	}
	else if ( ret != '' )
	{
		showMessage ( ret, obj );
	}
}

// 固定 Div
function stayDiv ( obj, top, left )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	top = top != null ? top : 0;
	left = left != null ? left : 0;
	obj.style.top = top + document.documentElement.scrollTop + 'px';
	obj.style.left = left + document.documentElement.scrollLeft + 'px';
	setTimeout ( "stayDiv('" + obj.id + "'," + top + "," + left + ")", 100 );
}


// Div 居中
function centerDiv ( obj )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	if ( obj )
	{
		obj.style.top = '50%';
		obj.style.left = '50%';
		try
		{
			obj.style.marginLeft = ( 0 - obj.scrollWidth / 2 + document.documentElement.scrollLeft ) + 'px';
			obj.style.marginTop = ( 0 - obj.scrollHeight / 2 + document.documentElement.scrollTop ) + 'px';
		}
		catch (e)
		{
		}
		setTimeout ( "centerDiv('" + obj.id + "')", 100 );
	}
}

// 設置背景色
function setBg ( obj, color )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	try
	{
		obj.style.backgroundColor = color;
	}
	catch (e)
	{
	}
}

// html 特殊字符
function htmlchars ( string )
{
	string = string.replace ( /\"/g, '&quot;' );
	string = string.replace ( /\'/g, '&#039;' );

	return string;
}

// 格式化輸出
function sprintf ()
{
	if ( sprintf.arguments.length < 2 )
	{
		return;
	}
	var data = sprintf.arguments[ 0 ];
	for( var k=1; k<sprintf.arguments.length; ++k )
	{
		switch( typeof( sprintf.arguments[ k ] ) )
		{
			case 'string':
			data = data.replace( /%s/, sprintf.arguments[ k ] );
			break;
			case 'number':
			data = data.replace( /%d/, sprintf.arguments[ k ] );
			break;
			case 'boolean':
			data = data.replace( /%b/, sprintf.arguments[ k ] ? 'true' : 'false' );
			break;
			default:
			break;
		}
	}
	return( data );
}

 if ( !String.sprintf ) String.sprintf = sprintf;

// 图层定位
function moveDivHere ( obj, loadingMsg )
{
	try
	{
		if ( obj != null )
		{
			if ( loadingMsg != null ) obj.innerHTML = loadingMsg;

			// 獲取當前鼠标坐标
			var posX = clsMouseCoords.x + 5;
			var posY = clsMouseCoords.y + 5;
			obj.style.left = posX + 'px';
			obj.style.top = posY + 'px';
		}
	}
	catch (e)
	{
	}
}

// 复制URL地址
function setCopy (_sTxt)
{
	if ( navigator.userAgent.toLowerCase().indexOf ( 'ie' ) > -1 )
	{
		clipboardData.setData ( 'Text', _sTxt );
		alert ( '網址“' + _sTxt + '”\n已經复制到您的剪贴板中\n您可以使用Ctrl+V快捷键粘贴到需要的地方' );
	}
	else
	{
		prompt ( '請复制網站地址:', _sTxt ); 
	}
}

// 設為首頁
function setHomePage ( url )
{
	var obj = getSrcElement ();

	if ( document.all )
	{
		obj.style.behavior='url(#default#homepage)';
		obj.setHomePage ( url );
	}
	else
	{
		if ( window.netscape )
		{
			try
			{  
				netscape.security.PrivilegeManager.enablePrivilege ( "UniversalXPConnect" );
			}
			catch (e)  
			{  
				alert ( "您的瀏覽器不支持自動設置首頁，請自行手動設置。");
			}
		}

		var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
		prefs.setCharPref ( 'browser.startup.homepage', url );
	}
}

// 加入收藏
function addBookmark ( site, url )
{
	if ( document.all )
	{
		window.external.addFavorite(url,site);
	}
	else if ( window.sidebar )
	{
		window.sidebar.addPanel ( site, url, "" )
	}
	else if ( navigator.userAgent.toLowerCase().indexOf ( 'opera' ) > -1 )
	{
		alert ( '請使用 Ctrl+T 將本頁加入收藏夹' );
	}
	else
	{
		alert ( '請使用 Ctrl+D 將本頁加入收藏夹' );
	}
}

// 语言包支持
function lang ()
{
	var strInput = langPackage[lang.arguments[0]];
	var strParams = '';

	for( var k=1; k < lang.arguments.length; ++k )
	{
		switch( typeof( lang.arguments[ k ] ) )
		{
			case 'string':
				strParams += ", '" + lang.arguments[ k ] + "'";
			break;
			case 'number':
				strParams += ", " + lang.arguments[ k ] + "";
			break;
		}
	}
	if ( strParams != '' )
	{
		strEval = "strOutput = String.sprintf ( strInput" + strParams + " );";
		eval ( strEval );
	}
	else
	{
		strOutput = strInput;
	}	
	return ( strOutput );
}

// 獲取鼠标坐标
function mouseCoords ()
{
	this.x = 0;
	this.y = 0;

	this.getMouseCoords = function ( ev )
	{
		try
		{
			ev = ev || window.event;
			if ( ev.pageX || ev.pageY )
			{
				this.x = ev.pageX;
				this.y = ev.pageY;
			}
			else
			{
				this.x = ev.clientX + document.documentElement.scrollLeft - document.documentElement.clientLeft;
				this.y = ev.clientY + document.documentElement.scrollTop  - document.documentElement.clientTop;
			}
		}
		catch (e)
		{
		}
	}
}

var clsMouseCoords = new mouseCoords ();
document.onmousemove = function ( ev )
{
	clsMouseCoords.getMouseCoords ( ev );
}

// 生成递归列表
function listRecursion ( DataArray, Template, Iconexpand, IconContract )
{
	this.DataArray = DataArray; // 數据數组
	this.Template = Template; // 内容模板
	this.Iconexpand = Iconexpand; // 展開按钮
	this.IconContract = IconContract; // 收缩按钮

	this.List = function ( Parent )
	{
		var ret = '';
		for ( var i = 0; i < this.DataArray.length; i ++ )
		{
			if ( this.DataArray[i][2] == Parent )
			{
				var Sub = this.List ( this.DataArray[i][0] );
				var Content = this.Template;
				re = /%SUB%/g
				if ( !Sub )
				{
					var Icon = this.IconContract;
					Content = Content.replace ( re, '' );
				}
				else
				{
					var Icon = this.Iconexpand;
					Content = Content.replace ( re, Sub );
				}

				re = /%ICON%/g
				Content = Content.replace ( re, Icon );
				re = /%KEY%/g
				Content = Content.replace ( re, i );
				re = /%0%/g
				Content = Content.replace ( re, this.DataArray[i][0] );
				re = /%1%/g
				Content = Content.replace ( re, this.DataArray[i][1] );
				re = /%2%/g
				Content = Content.replace ( re, this.DataArray[i][2] );
				re = /%3%/g
				Content = Content.replace ( re, this.DataArray[i][3] );
				re = /%4%/g
				Content = Content.replace ( re, this.DataArray[i][4] );
				re = /%5%/g
				Content = Content.replace ( re, this.DataArray[i][5] );
				ret += Content
			}
		}
		return ( ret );
	}

	this.expand = function ( Img, Sub, Flag )
	{
		var Img = document.getElementById ( Img );
		var Sub = document.getElementById ( Sub );

		if ( Sub.innerHTML != '' )
		{
			if ( ( Flag == null || Flag == false ) && Img.flag )
			{
				Img.src = "/common/images/icon-expand.gif";
				Img.flag = 0;
				hideItem ( Sub );
			}
			else if ( ( Flag == null || Flag == true ) && !Img.flag )
			{
				Img.src = "/common/images/icon-contract.gif";
				Img.flag = 1;
				showItem ( Sub );
			}
		}
	}
	
	this.expandAll = function ( ImgPrefix, SubPrefix, Flag )
	{
		var i = 0;
		var Img;
		while ( Img = document.getElementById ( ImgPrefix + i ) )
		{
			var Sub = document.getElementById ( SubPrefix + i );
			this.expand ( ImgPrefix + i, SubPrefix + i, Flag );
			i ++;
		}
	}
}

// 更改所有名稱為 objName 的物件的 innerHTML
function setValue ( objName, value )
{
	try
	{
		objs = getObjects ( objName );
		for ( var i in objs )
		{
			objs[i].innerHTML =value;
		}
	}
	catch (e){}
}

// 獲取物件
function getObjects ( objName )
{
	var objs = new Array ();

	if ( idObjs = document.getElementById ( objName ) )
	{
		objs.push ( idObjs );
	}

	if ( document.all ) // IE
	{
		var objTypes = new Array ( 'table', 'tr', 'td', 'div', 'li', 'span', 'a' );
		for ( var tagType in objTypes )
		{
			var typeObjs = document.getElementsByTagName ( objTypes[tagType] );
			for ( var i in typeObjs )
			{
				if ( typeObjs[i].name == objName )
				{
					objs.push ( typeObjs[i] );
				}
			}
		}
	}
	else // 其他瀏覽器
	{
		nameObjs = document.getElementsByName ( objName );
		for ( var i in nameObjs )
		{
			objs.push ( nameObjs[i] );
		}
	}

	return objs;
}

// 獲得焦點
function setFocus ( objName )
{
	var obj;

	var objs = document.getElementsByName ( objName );
	if ( objs.length > 0 )
	{
		obj = objs.item(0);
	}
	else
	{
		obj = document.getElementById ( objName );
	}
	if ( obj )
	{
		obj.focus ();
	}
}

// 高亮物件
function highlight ( obj, highlightClass )
{
	if ( typeof ( obj ) == 'string' )
	{
		obj = document.getElementById ( obj );
	}
	if ( highlightClass == null )
	{
		highlightClass = 'highlight';
	}
	try 
	{
		for ( var i in obj.parentNode.childNodes )
		{
			if ( obj.parentNode.childNodes[i].className != null )
			{
				var re = new RegExp ( "[ ]*" + highlightClass );
				obj.parentNode.childNodes[i].className = obj.parentNode.childNodes[i].className.replace ( re, '' );
			}
		}
		obj.className += ' ' + highlightClass;
	}
	catch ( e ) {}
}

// 载入物件
function objLoader ()
{
	this.timeStamp = null;
	this.loadedJs = '';
	this.loadedCss = '';

	// 载入指定頁面到指定物件
	/*
		loadUrl : 载入頁面的 URL
		targetObj : 目标容器物件 ID
		queryString : 附加送出变量
		loadJs : 附加 Js 文件
		loadingMsg : 载入中提示文字
	*/
	this.get = function ( loadUrl, targetObj, queryString, loadingMsg, callbackFunc, loadJs )
	{
		this.load ( 'GET', loadUrl, targetObj, queryString, loadingMsg, callbackFunc, loadJs );
	}

	this.post = function ( loadUrl, targetObj, queryString, loadingMsg, callbackFunc, loadJs )
	{
		this.refreshCache ();
		this.load ( 'POST', loadUrl, targetObj, queryString, loadingMsg, callbackFunc, loadJs );
	}

	this.load = function ( method, loadUrl, targetObj, queryString, loadingMsg, callbackFunc, loadJs )
	{
		if ( !loadUrl ) return;
		var obj;
		if ( typeof ( targetObj ) == 'string' )
		{
			obj = document.getElementById ( targetObj );
		}
		else
		{
			obj = targetObj;
		}
		if ( obj )
		{
			if ( loadingMsg != null ) obj.innerHTML = loadingMsg;
		}
		if ( callbackFunc == null )
		{
			callbackFunc = ajaxCallback;
		}

		this.getTimeStamp ();
		loadUrl += loadUrl.indexOf ( '?' ) == -1 ? '?' : '&';
		loadUrl += 'timeStamp' + this.timeStamp + '=' + this.timeStamp;

		if ( queryString == null ) queryString = '';
		var clsAjax = new Ajax ( loadUrl, queryString, callbackFunc, targetObj );

		if ( loadJs != null )
		{
			this.loadJs ( loadJs );
		}
		if ( method.toLowerCase () == 'post' )
		{
			clsAjax.post ();
		}
		else
		{
			clsAjax.get ();
		}
	}

	// 载入 Js
	this.loadJs = function ()
	{
		if ( !document.getElementById )
		{
			return;
		}
		for ( var i=0; i < arguments.length; i