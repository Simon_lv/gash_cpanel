/*
Ajax 类
sUrl : 目标 URL
sQueryString : 送出变量
callbackFunc : 回调函數
callbackParams : 回调函數参數
sRecvType : 返回值格式 ( 0: 文本, 1: XML );
*/

function Ajax ( sUrl, sQueryString, callbackFunc, callbackParams, sRecvType )
{
	this.Url = sUrl;
	this.QueryString = sQueryString != null ? sQueryString : '';
	this.response; // 返回值

	this.XmlHttp = this.createXMLHttpRequest ();
	if ( this.XmlHttp == null )
	{
		alert ( "網路連結出错, 請重試!" );
		return;
	}
	var objxml = this.XmlHttp;
	objxml.onreadystatechange = function ()
	{
		Ajax.handleStateChange ( objxml, sRecvType, callbackFunc, callbackParams )
	}
}

Ajax.prototype.createXMLHttpRequest = function ()
{
	try
	{
		return new ActiveXObject ( "Msxml2.XMLHTTP" );
	}catch(e) {}

	try
	{
		return new ActiveXObject ( "Microsoft.XMLHTTP" );
	} catch(e) {}

	try
	{
		return new XMLHttpRequest ();
	} catch(e) {}

	return null;
}

Ajax.prototype.createQueryString = function ()
{
	var queryString = '';
	if ( this.QueryString != null && typeof ( this.QueryString ) != 'string' )
	{
		var elements = this.QueryString.elements;
		var pairs = new Array();
		for(var i=0;i<elements.length;i++){
			if((name=elements[i].name)&&(value = elements[i].value)){
				pairs.push(name + "=" + encodeURIComponent(value));
			}
		}
		queryString = pairs.join ("&");
	}
	else
	{
		queryString = this.QueryString;
	}
	return queryString;
}

Ajax.prototype.get = function ()
{
	sUrl = this.Url;
	var queryString = sUrl + ( sUrl.indexOf ('?') > 0 ? '&' : '?' ) + this.createQueryString();
	this.XmlHttp.open ( "GET", queryString, true );
	this.XmlHttp.send ( null );
}

Ajax.prototype.post = function ()
{
	var sUrl = this.Url;
	var queryString = this.createQueryString ();
	this.XmlHttp.open ( "POST", sUrl, true );
	this.XmlHttp.setRequestHeader ( "Content-Type","application/x-www-form-urlencoded" );
	this.XmlHttp.send ( queryString );
}

Ajax.handleStateChange = function ( XmlHttp, sRecvType, callbackFunc, callbackParams )
{
	if ( XmlHttp.readyState == 4 )
	{
		if ( XmlHttp.status == 200  )
		{
			Response = sRecvType ? XmlHttp.responseXML : XmlHttp.responseText;
			if ( callbackFunc != null )
			{
				callbackFunc ( Response, callbackParams );
			}
		}
		else
		{
			// alert ( "您还没有登入或者登入已經超時, 請登入後重試!" );
		}
	}
} 
