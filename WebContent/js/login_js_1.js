check_flag='action';
var login = {
	'flag' : false,
	'info' : {
		'user_nickname' : ''
	}
};

var Passport9Way = {
	'setCookie' : function(name, value) {
		expires = new Date();
		expires.setTime(expires.getTime() + (1000 * 86400 * 365));
		//document.cookie = name + "=" + escape(value) + "; expires=" + expires.toGMTString() +  "; path=/";
		document.cookie = name + "=" + escape(value) + "; path=/";
	},
	'getCookie' : function (name) {
		cookie_name = name + "=";
		cookie_length = document.cookie.length;
		cookie_begin = 0;
		while (cookie_begin < cookie_length) {
			value_begin = cookie_begin + cookie_name.length;
			if (document.cookie.substring(cookie_begin, value_begin) == cookie_name) {
				var value_end = document.cookie.indexOf (";", value_begin);
				if (value_end == -1) {
					value_end = cookie_length;
				}
				return unescape(document.cookie.substring(value_begin, value_end));
			}
			cookie_begin = document.cookie.indexOf(" ", cookie_begin) + 1;
			if (cookie_begin == 0) {
				break;
			}
		}
		return null;
	},
	'delCookie' : function (name) {
		var expireNow = new Date();
		document.cookie = name + "=" + "; expires=Thu, 01-Jan-70 00:00:01 GMT" +  "; path=/";
	},
	'loginOP' : function (token) {
		eval ( 'var ret = ' + token + ';' );
		//登入成功
		if ( ret && ret.loginFlag ) {
			//var html = ret.nickname + ' [ <a href="https://passport.9wee.com/logout" onclick="delCookie(\'passportTokenCookie\')">退出</a> ]';
			login.flag=true;
			login.info.user_nickname=ret.nickname;
		}
		this.setCookie ( 'passportTokenCookie', token );
	},
	'init' : function () {
		var passportTokenCookie = this.getCookie ( 'passportTokenCookie' );
		if ( passportTokenCookie ) {
			//alert ( 'Now is using Cookie...' );
			this.loginOP ( passportTokenCookie );
		}
		else{
			//alert ( 'Now is requesting for Passport...' );
			document.write ( '<script type="text/javascript" src="https://passport.9wee.com/api/js_user.php?callback=Passport9Way.loginOP"></script>' );
		}		
	}
};

Passport9Way.init();