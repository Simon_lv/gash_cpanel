<!--
function clsChkRegister ( form, objSelf )
{
	if ( objSelf == null ) objSelf = 'clsChkRegister';
	this.objSelf = objSelf;
	this.passed = false;
	this.objForm = document.getElementById (form);
	this.className = [ 'boxNormal', 'boxError', 'boxOK', 'boxWaiting' ];
	this.checkItems = [ "Username", "Nickname", "Password", "PasswordConfirm", "Question", "Answer", "Email", "Code" ];
	this.currentUser = null;
	this.checkScript = '/check';

	this.messages = {
		'Username'				: '使用汉字，英文、數字，总字符數在 4 - 20 之间。',
		'Nickname'				: '使用汉字，英文、數字，总字符數在 4 - 20 之间。',
		'Password'				: '使用數字和字母，长度為 6 - 20 個字符。',
		'PasswordConfirm'	: '再次輸入密碼以保證密碼无误。',
		'Email'					: '<span style="color:#FF6600">請認真填寫您的真实郵箱，以便我們發放奖品和提供更好的服務！</span>推荐填寫：<a href="http://mail.qq.com" target="_blank">QQ 郵箱</a>、<a href="http://mail.163.com" target="_blank">163 郵箱</a>、<a href="http://cn.mail.yahoo.com/" target="_blank">Yahoo 郵箱</a>',
		'Question'				: '如不修改請留空',
		'Answer'	 				: '如不修改請留空',
		'Code'					: '輸入上图中的數字，若无法辨别請點擊图片更换。',
		'PasswordOld'			: '請輸入您當前的登入密碼。',
		'Dump'					: ''
	};

	// 送出註冊信息時驗證
	this.checkForm = function ()
	{
		this.passed = true;
		for ( var i = 0; i < this.checkItems.length; i ++ )
		{
			obj = document.getElementById ( "tip" + this.checkItems[i] );
			if ( obj.className != this.className[2] )
			{
				var check = eval ( "this.check" + this.checkItems[i] + "()" );
				if ( check == 'checking' )
				{
					this.passed = false;
				}
				else
				{
					this.passed = this.passed && check;
				}
			}
		}
		return this.passed;
	}

	// 显示所有提示
	this.showAllTips = function ()
	{
		for ( var i in this.checkItems )
		{
			var obj;
			if ( obj = document.getElementById ( 'tip' + this.checkItems[i] ) )
			{
				obj.innerHTML = eval ( "this.messages." + this.checkItems[i] );
			}
		}
	}

	this.showTip = function ( chkItem, tipType, message )
	{
		var obj;
		if ( obj = document.getElementById ( 'tip' + chkItem ) )
		{
			if ( tipType == null ) tipType = 0;
			if ( message == null ) message = eval ( "this.messages." + chkItem );
			obj.style.display = "block";
			if ( tipType > 0 || obj.className != this.className[2] || obj.className == '' )
			{
				obj.innerHTML = message;
				obj.className = this.className[tipType];
			}
		}
	}
	
	this.hideTip = function ( chkItem )
	{
		var obj;
		if ( obj = document.getElementById ( 'tip' + chkItem ) )
		{
			if ( obj.className != this.className[2] )
			{
				obj.style.display = "none";
			}
		}
	}

	/****** 检驗部分 ******/

	// 检查用户名
	this.checkUsername = function ( checkSystem )
	{
		var message = '';
		if ( checkSystem == null ) checkSystem = true;
		if ( this.objForm.username.value.length == 0 )
		{
			message = "請填寫用户名";
		}
		else if ( this.objForm.username.value.indexOf (' ') >= 0 || this.objForm.username.value.indexOf ('\\') >= 0 || this.objForm.username.value.indexOf ('\'') >= 0 || this.objForm.username.value.indexOf ('\"') >= 0 )
		{
			message = '用户名包含非法字符';
		}		
		if ( message != '' )
		{
			this.showTip ( 'Username', 1, message );
			return false;
		}
		else if ( checkSystem )
		{
			this.checkUsernameSystem ();
			return 'checking';
		}
		return true;
	}

	// Ajax 检查用户名
	this.checkUsernameSystem = function ()
	{
		this.showTip ( 'Username', 3, '正在检测用户名...' );
		var registerAjax = new Ajax ( this.checkScript, "act=user&username=" + escape ( this.objForm.username.value ), this.callbackCheckUsername, eval(this.objSelf) );
		registerAjax.get ();
	}

	// 检查用户名回调函數
	this.callbackCheckUsername = function ( ret, obj )
	{
		if ( ret == 'OK' )
		{
			obj.showTip ( 'Username', 2, '您填寫的用户名可以成功註冊' );
			return true;
		}
		else if ( ret != '' )
		{
			obj.showTip ( 'Username', 1, ret );
			return false;
		}
		else
		{
			obj.hideTip ( 'Username' );
		}
	}

	// 检查暱稱
	this.checkNickname = function ( checkSystem )
	{
		var message = '';
		if ( checkSystem == null ) checkSystem = true;
		if ( this.objForm.nickname.value.length == 0 )
		{
			message = "請填寫暱稱";
		}
		else if ( this.objForm.nickname.value.indexOf (' ') >= 0 || this.objForm.nickname.value.indexOf ('\\') >= 0 || this.objForm.nickname.value.indexOf ('\'') >= 0 || this.objForm.nickname.value.indexOf ('\"') >= 0 )
		{
			message = '暱稱包含非法字符';
		}		
		if ( message != '' )
		{
			this.showTip ( 'Nickname', 1, message );
			return false;
		}
		else if ( checkSystem )
		{
			this.checkNicknameSystem ();
			return 'checking';
		}
		return true;
	}

	// Ajax 检查暱稱
	this.checkNicknameSystem = function ()
	{
		this.showTip ( 'Nickname', 3, '正在检测暱稱...' );
		var registerAjax = new Ajax ( this.checkScript, "act=nickname&nickname=" + escape ( this.objForm.nickname.value ), this.callbackCheckNickname, eval(this.objSelf) );
		registerAjax.get ();
	}

	// 检查暱稱回调函數
	this.callbackCheckNickname = function ( ret, obj )
	{
		if ( ret == 'OK' )
		{
			obj.showTip ( 'Nickname', 2, '您填寫的暱稱可以成功註冊' );
			return true;
		}
		else if ( ret != '' )
		{
			obj.showTip ( 'Nickname', 1, ret );
			return false;
		}
		else
		{
			obj.hideTip ( 'Nickname' );
		}
	}

	// 检查原密碼
	this.checkPasswordOld = function ( checkSystem )
	{
		var message = '';
		if ( this.objForm.passwordold.value.length < 6 || this.objForm.passwordold.value.length > 20 )
		{
			message = '密碼长度必須為 6 - 20 字符';
		}
		if ( message != '' )
		{
			this.showTip ( 'PasswordOld', 1, message );
			return false;
		}
		else
		{
			this.showTip ( 'PasswordOld', 3, '等待驗證' );
			return true;
		}
	}

	// 检查密碼
	this.checkPassword = function ()
	{
		var message = '';
		if ( this.objForm.password.value.length < 6 || this.objForm.password.value.length > 20 )
		{
			message = '密碼长度必須為 6 - 20 字符';
		}
		else if ( this.objForm.password.value != '' && this.objForm.password.value == this.objForm.username.value )
		{
			message = '為了您的帳號安全，密碼請不要與用户名相同';
		}
		/*
		else if ( get_strong_level ( this.objForm.password.value, 6 ) < 2 )
		{
			message = '您的密碼强度太弱';
		}
		*/

		if ( message != '' )
		{
			this.showTip ( 'Password', 1, message );
			return false;
		}
		else
		{
			this.showTip ( 'Password', 2, '填寫正確' );
			return true;
		}	
	}

	// 检查密碼確認
	this.checkPasswordConfirm = function ()
	{
		var message = '';
		if ( this.objForm.passwordcfm.value != this.objForm.password.value )
		{
			message = '兩次填寫的密碼不一致, 請检查並重新填寫';
		}
		else if ( this.objForm.password.value == '' )
		{
			message = '請填寫密碼';
		}
		
		if ( message != '' )
		{
			this.showTip ( 'PasswordConfirm', 1, message );
			return false;
		}
		else
		{
			this.showTip ( 'PasswordConfirm', 2, '填寫正確' );
			return true;
		}
	}

	this.checkEmail = function ( username, checkSystem )
	{
		var message = '';
		checkSystem = checkSystem != null ? checkSystem : true;
		if ( this.objForm.email.value.length == 0 )
		{
			message = "請填寫您的郵箱地址";
		}
		if ( message != '' )
		{
			this.showTip ( 'Email', 1, message );
			return false;
		}
		else if ( checkSystem )
		{
			this.checkEmailSystem ( username );
			return 'checking';
		}
		return true;
	}

	this.checkEmailSystem = function ( username )
	{
		if ( username == null ) username = this.currentUser;
		this.showTip ( 'Email', 3, '正在检测郵箱地址...' );
		var registerAjax = new Ajax ( this.checkScript, "act=email&email=" + escape ( this.objForm.email.value ) + "&username=" + escape ( username ), this.callbackCheckEmail, eval(this.objSelf) );
		registerAjax.get ();
	}

	this.callbackCheckEmail = function ( ret, obj )
	{
		if ( ret == 'OK' )
		{
			obj.showTip ( 'Email', 2, '填寫正確' );
			return true;
		}
		else if ( ret != '' )
		{
			obj.showTip ( 'Email', 1, ret );
			return false;
		}
		else
		{
			obj.hideTip ( 'Email' );
		}
	}

	this.checkQuestion = function ()
	{
		var message = '';
		if ( this.objForm.user_question.value == '' )
		{
			message = '如不修改請留空';
			ret = true;
		}
		else
		{
			message = '填寫正確';
			ret = true;
		}

		this.showTip ( 'Question', ret ? 2 : 1, message );
		return ret;
	}

	this.checkAnswer = function ()
	{
		var message = '';
		if ( this.objForm.user_answer.value == '' )
		{
			message = '如不修改請留空';
			ret = true;
		}
		else if ( this.objForm.user_answer.value == this.objForm.user_question.value )
		{
			message = '為了您的帳號安全，密碼提示答案請不要與问题相同';
			ret = false;
		}
		else
		{
			message = '填寫正確';
			ret = true;
		}

		this.showTip ( 'Answer', ret ? 2 : 1, message );
		return ret;
	}

	this.checkCode = function ()
	{
		var Code = GetCookie ( 'verifyCode' );
		if ( this.objForm.code.value.toUpperCase () != Code.toUpperCase () )
		{
			this.showTip ( 'Code', 1, '驗證碼不正確, 請检查並重新填寫' );
			return false;
		}
		else
		{
			this.showTip ( 'Code', 2, '填寫正確' );
			return true;
		}
	}
}

// 检查密碼强度
function check_password_strong ( password )
{
	var strong = get_strong_level ( password, 6 );

	var objS1 = document.getElementById ( 'password_strong_1' );
	var objS2 = document.getElementById ( 'password_strong_2' );
	var objS3 = document.getElementById ( 'password_strong_3' );

	if ( strong == 1 )
	{
		objS1.className = 'pass_password_s1';
		objS2.className = 'pass_password_s0';
		objS3.className = 'pass_password_s0';
	}
	else if ( strong == 2 )
	{
		objS1.className = 'pass_password_s2';
		objS2.className = 'pass_password_s2';
		objS3.className = 'pass_password_s0';
	}
	else if ( strong >= 3 )
	{
		objS1.className = 'pass_password_s3';
		objS2.className = 'pass_password_s3';
		objS3.className = 'pass_password_s3';
	}
	else
	{
		objS1.className = 'pass_password_s0';
		objS2.className = 'pass_password_s0';
		objS3.className = 'pass_password_s0';
	}
}

function get_strong_level ( string, minLength )
{
	if ( minLength == null ) minLength = 1;
	if ( string.length < minLength )
	{
		return 0;
	}

	var ls = 0;
	if ( string.match(/[a-z]/ig ) )
	{
		ls ++;
	}
	if ( string.match(/[0-9]/ig ) )
	{
		ls ++;
	}
	if ( string.match(/(.[^a-z0-9])/ig ) )
	{
		ls ++;
	}
	if ( string.length >= 10 )
	{
		ls ++;
	}

	var firstChar = string.substr ( 0, 1 );
	var repeat = 1;
	for ( var i = 1; i < string.length; i ++ )
	{
		if ( string.substr ( i, 1 ) == firstChar )
		{
			repeat ++;
		}
		else
		{
			break;
		}
	}
	if ( string.length == repeat )
	{
		ls --;
	}

	var seq = '01234567890abcdefghijklmnopqrstuvwxyz';
	if ( seq.indexOf ( string.toLowerCase () ) >= 0 )
	{
		ls --;
	}

	ls = Math.max ( ls, 1 );

	return ls;
}

// 獲取手機驗證密钥
function get_phone_code ( form, confirmMsg )
{
	if ( typeof ( form ) == 'string' ) form = $ ( form );
	if ( !check_phone ( form ) ) return false;
	if ( confirmMsg == null ) confirmMsg = '請確認您的手機號碼填寫无误，點擊“確定”系统將發送驗證密钥到您指定的手機號碼！';

	var phone = form.bind_value.value;
	if ( confirm ( confirmMsg ) )
	{
		var registerAjax = new Ajax ( '/bind', 'act=get_phone_code&phone=' + phone, callback_get_phone_code, phone );
		registerAjax.get ();
	}
}

function bind_set_step ( step )
{
	if ( step == 'code' )
	{
		var form = $('formBind');
		if ( !check_phone ( form ) ) return false;
		$('bindingPhoneNumber').innerHTML = form.bind_value.value;
		var arrDisplay = [ 'none', '', 'none' ];
	}
	else if ( step == 'cancel' )
	{
		var arrDisplay = [ 'none', 'none', '' ];
	}
	else
	{
		var arrDisplay = [ '', 'none', 'none' ];
	}

	$('stepSetPhone').style.display = arrDisplay[0];
	$('stepSetCode').style.display = arrDisplay[1];
	$('stepCancelPhone').style.display = arrDisplay[2];
}

function check_phone ( form )
{
	var phone = form.bind_value.value;
	if ( phone == '' )
	{
		showMessage ( '請填寫手機號碼！' );
		form.bind_value.focus ();
		return false;
	}
	if ( !phone.match ( /^[0-9]{11}$/ ) )
	{
		showMessage ( '您的手機號碼格式无效，請重新填寫！' );
		form.bind_value.select ();
		return false;
	}
	return true;
}

function check_phone_code ( form )
{
	var phoneCode = form.bind_code.value;
	if ( phoneCode == '' )
	{
		showMessage ( '請填寫驗證密钥！如果您尚未獲取或者遗忘了驗證密钥，請點擊“重新獲取密钥”按钮獲取！' );
		form.bind_code.focus ();
		return false;
	}
	if ( !phoneCode.match ( /^[a-zA-Z0-9]{6}$/ ) )
	{
		showMessage ( '您的驗證密钥格式无效，請重新填寫！\n如果您尚未獲取或者遗忘了驗證密钥，請點擊“重新獲取密钥”按钮獲取！' );
		form.bind_code.select ();
		return false;
	}
	return true;
}

function callback_get_phone_code ( ret, phone )
{
	if ( ret == 'OK' )
	{
		bind_set_step ( 'code' );
		showMessage ( '驗證密钥已經發往您的手機 [ ' + phone +  ' ]，您將在几分钟後收到短信。\n您只需輸入您收到的驗證密钥即可完成手機绑定。如果您长時间未收到密钥，請尝試重新獲取！' );
	}
	else if ( ret != '' )
	{
		showMessage ( ret );
	}
	else
	{
		showMessage ( '驗證密钥發送失敗，請稍候重試！' );
	}
}
//-->