package tw.tw360.web.constants;

import java.util.HashMap;

public class Config {
	
	public final static String PLATFORM = "tccgash";
	
	public final static String DRIVER_SMS_CONTENT = "乘客點卡費%s元,司機信用額度%s元,未繳交款項%s元\n完成收款請簡訊回覆密碼\n司機客服專線:02-2748-2559";
	public final static String USER_SMS_CONTENT = "感謝購買大車隊%s元Gash點卡\n密碼:%s\n傾國科技客服專線:02-2748-2559";
	
	public final static String DRIVER_UPDATE_PWD_CONTENT = "請簡訊回覆新的收款密碼(4碼數字)\n司機客服專線:02-2748-2559";
	
	public static final String RETURN_SUCCESS = "0000";
	public static final String RETURN_NOT_FOUND = "0001";
	public static final String RETURN_CARD_REMAINDER_NOT_ENOUGH = "0002";
	public static final String RETURN_CREDIT_NOT_ENOUGH = "0003";
	public static final String RETURN_SEND_DRIVER_SMS_FAIL = "0004";
	public static final String RETURN_SEND_USER_SMS_FAIL = "0005";
	public static final String RETURN_TOKEN_FAIL = "0006";
	public static final String RETURN_EMAIL_FORMATE_ERROR = "0007";
	public static final String RETURN_POST_ERROR = "0008";
	public static final String RETURN_SYSTEM_ERROR = "9999";
	public static final HashMap RETURN_RESULT=new HashMap();
	static{
		RETURN_RESULT.put(RETURN_SUCCESS, "success");
		RETURN_RESULT.put(RETURN_NOT_FOUND, "資料不存在");
		RETURN_RESULT.put(RETURN_CARD_REMAINDER_NOT_ENOUGH, "可販售點卡剩餘數量不足");
		RETURN_RESULT.put(RETURN_CREDIT_NOT_ENOUGH, "本車點卡銷售完畢");
		RETURN_RESULT.put(RETURN_SEND_DRIVER_SMS_FAIL, "發送司機簡訊失敗");
		RETURN_RESULT.put(RETURN_SEND_USER_SMS_FAIL, "send user sms fail");
		RETURN_RESULT.put(RETURN_TOKEN_FAIL, "傳送資料檢核錯誤");
		RETURN_RESULT.put(RETURN_EMAIL_FORMATE_ERROR, "email format error");
		RETURN_RESULT.put(RETURN_POST_ERROR, "post data error");
		RETURN_RESULT.put(RETURN_SYSTEM_ERROR, "system error");
	}
	
	public static final String MAIL_HOST="8888Play網絡";
	public static final String FORGET_PASSWORD_SUBJECT="會員帳號密碼修改（重要）！";
	public static final String FORGET_PASSWORD_CONTENT="<br>&nbsp;您好，這是一封重要郵件，您的新密碼為  %s ，請您妥善保管好新密碼！<br><br>如您有其他問題，請聯繫我們：cs@360tw.tw";
}
