package tw.tw360.web.card;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.InvoiceCardCount;
import tw.tw360.service.TccInvoiceManager;
import tw.tw360.util.PrintUtil;

@SuppressWarnings("serial")
public class InvoiceAction extends CRUDActionSupport<InvoiceCardCount> {
	@Autowired
	private TccInvoiceManager tccInvoiceManager;
	private static final String TAG="InvoiceAction";

	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public InvoiceCardCount getModel() {
		return null;
	}

	@Override
	public String delete() throws Exception {
		return null;
	}

	@Override
	public String list() throws Exception {
		Calendar calendar = Calendar.getInstance();
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		int cardType = 1;
		int soldStatus = 1;
		if(StringUtils.isNotBlank(map.get("cardType"))) 
			cardType = new Integer(map.get("cardType"));
		if(StringUtils.isNotBlank(map.get("soldStatus")))
			soldStatus = new Integer(map.get("soldStatus"));
		
		String startTime = map.get("startTime");
		String endTime = map.get("endTime");
		boolean query = true;
		if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
			Date start = format.parse(startTime);
			Date end = format.parse(endTime);
			calendar.setTime(start);
			calendar.add(Calendar.MONTH, 1);
			if(end.after(calendar.getTime())) {//起迄超過一個月
				end = calendar.getTime();
				map.put("endTime", format.format(end.getTime()));
			}
		}
		else if(StringUtils.isNotBlank(endTime)) {
			Date end = format.parse(endTime);
			if(end.after(new Date())) {//不可選擇未發生的時間點
				end = new Date();
				map.put("endTime", format.format(end));
			}
			calendar.setTime(end);
			calendar.add(Calendar.MONTH, -1);
			map.put("startTime", format.format(calendar.getTime()));
		}
		else if(StringUtils.isBlank(startTime)) {//未輸入任何資料不得查詢
			query = false;
		}
		
		if(query) {
			List<InvoiceCardCount> results = tccInvoiceManager.findAll(map);
			request.setAttribute("results", results);
		}
		request.setAttribute("map", map);
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
