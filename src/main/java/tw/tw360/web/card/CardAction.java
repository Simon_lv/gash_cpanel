package tw.tw360.web.card;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Card;
import tw.tw360.dto.Driver;
import tw.tw360.dto.PayedCardQuery;
import tw.tw360.service.CardManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ReportExcel;

/**
 * @author jenco
 * time：2015-2-4 下午3:21:46
 */
@SuppressWarnings("serial")
public class CardAction extends CRUDActionSupport<Card> {
	
	private static final String TAG = "CardAction";
	private Calendar calendar = Calendar.getInstance();
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	private CardManager cardManager;
	
	@Override
	public Card getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		int cardType = 1;
		int soldStatus = 1;
		if(StringUtils.isNotBlank(map.get("cardType"))) 
			cardType = new Integer(map.get("cardType"));
		if(StringUtils.isNotBlank(map.get("soldStatus")))
			soldStatus = new Integer(map.get("soldStatus"));
		
		String startTime = map.get("startTime");
		String endTime = map.get("endTime");
		boolean query = true;
		if(StringUtils.isNotBlank(startTime) && StringUtils.isNotBlank(endTime)) {
			Date start = format.parse(startTime);
			Date end = format.parse(endTime);
			calendar.setTime(start);
			calendar.add(Calendar.MONTH, 1);
			if(end.after(calendar.getTime())) {//起迄超過一個月
				end = calendar.getTime();
				map.put("endTime", format.format(end.getTime()));
			}
		}
		else if(StringUtils.isNotBlank(endTime)) {
			Date end = format.parse(endTime);
			if(end.after(new Date())) {//不可選擇未發生的時間點
				end = new Date();
				map.put("endTime", format.format(end));
			}
			calendar.setTime(end);
			calendar.add(Calendar.MONTH, -1);
			map.put("startTime", format.format(calendar.getTime()));
		}
		else if(StringUtils.isBlank(startTime)) {//未輸入任何資料不得查詢
			query = false;
		}
		
		List<Map<String,Integer>> remains = cardManager.queryRemiansGroupByPrice(soldStatus, cardType);
		request.setAttribute("remains", remains);
		if(query) {
			List<PayedCardQuery> results = cardManager.findAll(map);
			request.setAttribute("results", results);
		}
		request.setAttribute("map", map);
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	/*
     * function : 匯出excel
     * description : 傳入搜尋條件，將結果寫入excel
     */
    public void export() {
        HttpServletRequest request = Struts2Utils.getRequest();
        HttpServletResponse response = Struts2Utils.getResponse();
        HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
                "export", request);
        String[] dataTitles = { "面額","賣出張數","已回存金額", "未回存金額", "是否退款"};
        List<PayedCardQuery> payedCardQuerys = cardManager.findAll(map);
        List<Object[]> list = new ArrayList<Object[]>();

        if (payedCardQuerys != null && !payedCardQuerys.isEmpty()) {
            for (PayedCardQuery payedCardQuery : payedCardQuerys) {
                Object[] o = new Object[5];
                o[0] = payedCardQuery.getPrice();
                o[1] = payedCardQuery.getCount();
                o[2] = payedCardQuery.getRestoreSum();
                o[3] = payedCardQuery.getNotRestoreSum();
                o[4] = (7 == payedCardQuery.getOrderStatus() ? "是" : "否");
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "點卡數量管理列表" + System.currentTimeMillis(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
