package tw.tw360.web.payment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Card;
import tw.tw360.dto.GiftCardPointCount;
import tw.tw360.dto.Payment;
import tw.tw360.dto.Position;
import tw.tw360.dto.SmsRecord;
import tw.tw360.query.PaymentQuery;
import tw.tw360.service.CardManager;
import tw.tw360.service.GiftCardManager;
import tw.tw360.service.PaymentManager;
import tw.tw360.service.SMSRecordManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.constants.Config;
import tw.tw360.web.util.DateUtil;
import tw.tw360.web.util.ReportExcel;
import tw.tw360.web.util.SMSUtils;
import tw.tw360.web.util.SessionUtil;

public class PaymentAction extends CRUDActionSupport<Payment> {

	private static final String TAG = "PaymentAction";
	private static final Logger LOG = LoggerFactory.getLogger(PaymentAction.class);

	@Autowired
	private GiftCardManager gcManager;
	@Autowired
	private PaymentManager paymentManager;
	@Autowired
	private CardManager cardManager;
	@Autowired
	private SMSRecordManager smsRecordManager;
	@Autowired
	private String canChargebackUser;

	private Page<PaymentQuery> page = new Page<PaymentQuery>(15);

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

	private static final String SMS_BODY = "親愛用戶,感謝您購買台灣大車隊Gash點數,恭喜獲得活動贈送%s點\n"
	// + "傾國客服專線:02-2748-2559\n"
			+ "序號:%s";

	private List<GiftCardPointCount> availableGiftPoints = new ArrayList<GiftCardPointCount>();
	private List<String> cantGiftPointsPaymentId = new ArrayList<String>();

	public List<String> getCantGiftPointsPaymentId() {
		return cantGiftPointsPaymentId;
	}

	public void setCantGiftPointsPaymentId(List<String> cantGiftPointsPaymentId) {
		this.cantGiftPointsPaymentId = cantGiftPointsPaymentId;
	}

	public List<GiftCardPointCount> getAvailableGiftPoints() {
		return availableGiftPoints;
	}

	public void setAvailableGiftPoints(List<GiftCardPointCount> availableGiftPoints) {
		this.availableGiftPoints = availableGiftPoints;
	}

	@Override
	public Payment getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		availableGiftPoints = gcManager.availableGiftPoints(1);
		request.setAttribute("availableGiftPoints", availableGiftPoints);
		cantGiftPointsPaymentId = gcManager.getCantGiftPointsPaymentId(1);
		request.setAttribute("cantGiftPointsPaymentId", cantGiftPointsPaymentId);
		// if (map.get("orderId") != null) {
		Admin ad = (Admin) request.getSession().getAttribute("LoginSession");
		// page = paymentManager.queryForPage(page, map);
		// Double sum = paymentManager.querySumForPayment(map);
		// request.setAttribute("priceSum", sum);
		// request.setAttribute("map", map);
		//
		if (ad != null && canChargebackUser.equalsIgnoreCase(ad.getUsername())) {
			map.put("canChargeback", "1");
		} else {
			map.put("canChargeback", "2");
		}
		// }

		Double sum = 0.0;

		request.setAttribute("map", map);
		Position p = SessionUtil.getRoleSession();
		LOG.info("Position ={},", p);
		if (p != null) {
			LOG.info("Position id={},", p.getId());
			if (p.getId().intValue() == 11) { // 合作廠商 (For Gash)
				map.put("orderStatus", "4");
				page = paymentManager.queryForPage(page, map);
				sum = paymentManager.querySumForPayment(map);
				LOG.info("gash sum={},", sum);
				request.setAttribute("priceSum", sum == null ? 0.0 : sum);
				return "gash";
			}
			// 關閉限制大車隊開啟 Excel 功能
			// else if (p.getId().intValue() == 6 || p.getId().intValue() == 7
			// || p.getId().intValue() == 8 || p.getId().intValue() == 9) {
			// page = paymentManager.queryForPage(page, map);
			// sum = paymentManager.querySumForPayment(map);
			// LOG.info("other sum={},", sum);
			// request.setAttribute("priceSum", sum==null?0.0:sum);
			// return "other";
			// }
		}
		page = paymentManager.queryForPage(page, map);
		sum = paymentManager.querySumForPayment(map);
		LOG.info("list sum={},", sum);
		request.setAttribute("priceSum", sum == null ? 0.0 : sum);
		return "list";
	}

	/*
	 * function : 匯出excel description : 傳入搜尋條件，將結果寫入excel
	 */
	public void export() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "export", request);
		String[] dataTitles = { "訂單編號", "隊編", "司機手機", "用戶手機", "訂單時間", "面額", "訂單狀態", "回存狀態", "統一發票號碼", "是否測試賬務" };
		List<PaymentQuery> payments = paymentManager.queryALLPayment(map);
		List<Object[]> list = new ArrayList<Object[]>();

		if (payments != null && !payments.isEmpty()) {
			for (PaymentQuery p : payments) {
				Object[] o = new Object[10];
				o[0] = p.getOrderId();
				o[1] = p.getCarId();
				o[2] = p.getDriverMobile();
				o[3] = p.getUserMobile();
				o[4] = DateUtil.format(p.getCreateTime(), "yyyy-MM-dd HH:mm:ss");
				o[5] = p.getPrice();
				switch (p.getOrderStatus()) {
				case 1:
					o[6] = "用戶請求";
					break;
				case 2:
					o[6] = "發送司機簡訊";
					break;
				case 3:
					o[6] = "收到司機回覆";
					break;
				case 4:
					o[6] = "發送點卡";
					break;
				case 5:
					o[6] = "司機事實並未收款";
					break;
				case 6:
					o[6] = "用戶未收到點卡簡訊";
					break;
				case 7:
					o[6] = "用戶已收到點卡簡訊";
					break;
				case 8:
					o[6] = "";
					break;
				case 9:
					o[6] = "退款";
					break;
				}
				switch (p.getRestoreStatus()) {
				case 1:
					o[7] = "未回存";
					break;
				case 2:
					o[7] = "已回存";
					break;
				}
				o[8] = p.getInvoiceNo();
				o[9] = (1 == p.getIsTest() ? "是" : "否");
				list.add(o);
			}
		}

		try {
			// reportExcel.reportExcel(request, response,
			// "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
			ReportExcel.reportExcel(request, response, "儲值列表" + System.currentTimeMillis(), "", dataTitles, list);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String detail() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "detail", request);
		long id = Long.parseLong(map.get("id"));
		Payment payment = paymentManager.findById(id);
		request.setAttribute("payment", payment);
		return "detail";
	}

	public void updatePayment() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "updatePayment", request);
		long id = Long.parseLong(map.get("id"));
		Payment payment = paymentManager.findById(id);

		payment.setInvoiceType(Integer.parseInt(map.get("invoiceType")));
		payment.setUserEmail(map.get("userEmail"));
		payment.setInvoiceAddress(map.get("invoiceAddress"));
		payment.setInvoiceNo(map.get("invoiceNo"));
//		payment.setOrderStatus(Integer.parseInt(map.get("orderStatus")));
		
		paymentManager.updateInvoiceData(payment);
		try {
			response.sendRedirect(request.getContextPath() + "/payment/payment!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendSms() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "sendSms", request);
		long id = Long.parseLong(map.get("id"));

		PaymentQuery payment = paymentManager.findById(id);
		if (payment == null) {
			return;
		}

		Card card = cardManager.findById(payment.getCardId());
		if (card == null) {
			return;
		}

		String toPhone = payment.getUserMobile();
		String content = String.format(Config.USER_SMS_CONTENT, card.getPrice(), card.getPw());
		System.out.println("sendSms content====" + content);
		String sno = "U" + payment.getCardId();

		HashMap<String, String> ret = sendSMS(Config.PLATFORM, toPhone, content, sno);
		String retStatus = ret.get("status");

		// 重新发送
		try {
			PrintWriter out = response.getWriter();
			out.print(retStatus);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public Page<PaymentQuery> getPage() {
		return page;
	}

	public void setPage(Page<PaymentQuery> page) {
		this.page = page;
	}

	private HashMap<String, String> sendSMS(String platform, String mobile, String content, String sno) {
		SMSUtils sms = new SMSUtils();
		String smsRetStr = sms.sendSMS(false, // 測試環境請設 true
				mobile, content, platform, sno + sdf.format(new Date()));
		System.out.println("smsTestRet:" + smsRetStr);
		HashMap<String, String> retHM = SMSUtils.getHashMapFromStr(smsRetStr);

		String retStatus = retHM.get("status");
		if (retStatus != null) {
			System.out.println("sendSMSToMobile retStatus=" + retStatus);
			if (retStatus.equals("0")) {
				SmsRecord smsRecord = new SmsRecord();
				smsRecord.setMessageId(retHM.get("MessageID"));
				smsRecord.setUsedCredit(retHM.get("UsedCredit"));
				smsRecord.setMemberId(retHM.get("MemberID"));
				smsRecord.setCredit(retHM.get("Credit"));
				smsRecord.setMobileNo(retHM.get("MobileNo"));
				smsRecord.setStatus(retHM.get("status"));
				smsRecord.setCreateTime(new Timestamp(System.currentTimeMillis()));

				smsRecordManager.add(smsRecord);
			}
		}

		return retHM;
	}

	public void chargeback() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		Admin ad = (Admin) request.getSession().getAttribute("LoginSession");
		page = paymentManager.queryForPage(page, map);
		request.setAttribute("map", map);

		if (ad != null && canChargebackUser.equalsIgnoreCase(ad.getUsername())) {
			long id = Long.parseLong(request.getParameter("id"));
			paymentManager.updateOrderStatus(id, 9);
		}

		HttpServletResponse response = Struts2Utils.getResponse();

		PrintWriter out;
		out = response.getWriter();
		out.print(0);
		out.flush();
		out.close();

	}

	public void giftPoints() throws Exception {
		HttpServletResponse response = Struts2Utils.getResponse();
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "giftPoints", request);
		String ph = "gift-";
		Set<String> keys = map.keySet();
		int allPoint = 0;
		List<GiftCardPointCount> availableGiftPoints = gcManager.availableGiftPoints(1);
		String selectedPaymentId = map.get("selectedPaymentId");
		String selectedUserMobile = map.get("selectedUserMobile");
		map.remove("selectedPaymentId");
		map.remove("selectedUserMobile");
		String msg = "";
		for (String key : keys) {
			String price = key.substring(ph.length());
			int value = Integer.parseInt(map.get(key));
			boolean canChange = (availableGiftPoints.size() != 0);
			int availableCount = 0;
			for (GiftCardPointCount cp : availableGiftPoints) {
				if (String.valueOf(cp.getPrice()).equals(price)) {
					if (cp.getAvailableCount() >= value) {
						cp.setIntendedUseCount(value);
						canChange = true;
						allPoint += cp.getPrice() * value;
					} else {
						availableCount = cp.getAvailableCount();
						canChange = false;
						break;
					}
				}
			}
			if (!canChange) {
				msg = price + "的數量只有" + availableCount + ",請重新操作";
				break;
			}
		}

		if (msg.length() == 0) {
			// Admin ad = (Admin) request.getSession().getAttribute(
			// "LoginAdminSession");
			Admin ad = SessionUtil.getLoginSession();
			if (allPoint > 3000) {
				msg = "贈送點數超過3000點";
			} else if (ad == null) {
				msg = "登入憑證異常請重新登入";
			} else if (msg.length() == 0) {
				// 雖然giftcard是adminid,但是存username
				msg = giftPoints(availableGiftPoints, selectedUserMobile, selectedPaymentId, 1, ad.getUsername());
			}
		}

		JSONObject js = new JSONObject();
		availableGiftPoints = gcManager.availableGiftPoints(1);
		js.accumulate("msg", msg);
		for (GiftCardPointCount cp : availableGiftPoints) {
			js.accumulate("availableCount-" + cp.getPrice(), cp.getAvailableCount());
		}
		writeJasonMsg(response, js.toString());
	}

	private void writeJasonMsg(HttpServletResponse response, String msg) throws IOException {
		PrintWriter out;
		out = response.getWriter();
		out.print(msg);
		out.flush();
		out.close();
	}

	private String giftPoints(List<GiftCardPointCount> availableGiftPoints, String selectedUserMobile, String selectedPaymentId, int type, String adminId) {
		boolean existPaymentId = gcManager.existPaymentId(selectedPaymentId);
		if (existPaymentId) {
			return "已贈送";
		}
		String updateResult = gcManager.updateAdminIdPayment(availableGiftPoints, selectedPaymentId, type, adminId);
		if (updateResult != null) {
			gcManager.updateAdminIdPaymentToNull(selectedPaymentId, type, adminId);
			return updateResult;
		}

		List<Map<String, String>> pws = gcManager.queryPwd(selectedPaymentId, type, adminId);

		// send SMS
		for (Map<String, String> pw : pws) {
			sendSMS("giftcard", selectedUserMobile, String.format(SMS_BODY, pw.get("price"), pw.get("pw")), "G");
		}
		gcManager.updateSoldTime(selectedPaymentId, type, adminId);

		return "贈點成功";
	}
}
