package tw.tw360.web.driver;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.common.encrypt.MD5Util;
import tw.tw360.dto.Driver;
import tw.tw360.query.PaymentQuery;
import tw.tw360.service.DriverManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.DateUtil;
import tw.tw360.web.util.ReportExcel;

@SuppressWarnings("serial")
public class DriverAction extends CRUDActionSupport<Driver> {

	private static final String TAG = "DriverAction";
	@Autowired
	private DriverManager driverManager;
	
	private Page<Driver> page = new Page<Driver>(15);
	
	
	@Override
	public Driver getModel() {
		
		return null;
	}

	@Override
	public String delete() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "delete", request);
		if(StringUtils.isNotBlank(map.get("type")) && StringUtils.isNotBlank(map.get("id"))){
			String carId = map.get("id");
			Driver d = driverManager.findBycarId(carId);
			long id = d.getId();
			driverManager.updateStatus(id, 2);
		}
		try {
			PrintWriter out=response.getWriter();
			out.print(map.get("id"));
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String edit() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "edit", request);
		String carId = map.get("carId");
		Driver driver = driverManager.findBycarId(carId);;
		if(StringUtils.isBlank(map.get("submit"))) {
			request.setAttribute("driver", driver);
			request.setAttribute("map", map);
			return "edit";
		}
		if(StringUtils.isNotBlank("name")) {
			driver.setName(map.get("name"));
		}
		if(StringUtils.isNotBlank(map.get("mobile"))) {
			String mobile = map.get("mobile");
			driver.setMobile(mobile);
			driver.setPassword(MD5Util.crypt(mobile.substring(mobile.length()-4)));
		}
		
		boolean forceSellValue = StringUtils.isNotBlank(map.get("changeSell"));
		if(forceSellValue) {
			int sellValue = Integer.parseInt(map.get("canSell"));
			driver.setCanSell(sellValue);
		}
		if(StringUtils.isNotBlank("credit")) {
			int newCredit = Integer.parseInt(map.get("credit"));
			driver.setCredit(newCredit);
			
			if(!forceSellValue) {
				int nonRestore = driver.getNonRestore();
				if(nonRestore >= newCredit) {//當 non_restore >= credit則 can_sell=2, 反之則 can_sell=1
					driver.setCanSell(2);
				}
				else {
					driver.setCanSell(1);
				}
			}
		}
		if(StringUtils.isNotBlank(map.get("shortCode"))) {
            driver.setShortCode(map.get("shortCode"));
        }
		driverManager.update(driver);
		try {
			response.sendRedirect(request.getContextPath()+ "/driver/driver!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		
		page = driverManager.queryUserList(page, map);
		request.setAttribute("map", map);

		return "list";
	}
	
	public String toAdd(){
		return "add";
	}
	
	@Override
	public String save() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "save", request);
		if(StringUtils.isBlank(map.get("carId"))){
			return "add";
		}
		
		Driver driver = driverManager.findBycarId(map.get("carId"));
		if(driver != null) {
			request.setAttribute("message", "隊編對應的資料已經錄入！");
			return "add";
		}
		driver = new Driver();
		driver.setCarId(map.get("carId"));
		driver.setName(map.get("name"));
		driver.setMobile(map.get("mobile"));
		driver.setCredit(Integer.parseInt(map.get("credit")));
		driver.setCreateTime(new Timestamp(System.currentTimeMillis()));
		driver.setStatus(1);
		driver.setCanSell(1);
		driver.setShortCode(map.get("shortCode"));
		driverManager.add(driver);

		try {
			response.sendRedirect(request.getContextPath()+ "/driver/driver!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
	}

	

	public Page<Driver> getPage() {
		return page;
	}

	public void setPage(Page<Driver> page) {
		this.page = page;
	}
	
	/*
     * function : 匯出excel
     * description : 傳入搜尋條件，將結果寫入excel
     */
    public void export() {
        HttpServletRequest request = Struts2Utils.getRequest();
        HttpServletResponse response = Struts2Utils.getResponse();
        HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
                "export", request);
        String[] dataTitles = { "隊編","姓名","手機號碼", "信用額度", "具販售資格","司機簡碼"};
        List<Driver> drivers = driverManager.queryALLUserList(map);
        List<Object[]> list = new ArrayList<Object[]>();

        if (drivers != null && !drivers.isEmpty()) {
            for (Driver driver : drivers) {
                Object[] o = new Object[6];
                o[0] = driver.getCarId();
                o[1] = driver.getName();
                o[2] = driver.getMobile();
                o[3] = driver.getCredit();
                o[4] = (1 == driver.getCanSell() ? "是" : "否");
                o[5] = driver.getShortCode();
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "司機管理列表" + System.currentTimeMillis(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
    	String mobile = "0912345678";
    	System.out.println("mobile="+MD5Util.crypt(mobile.substring(mobile.length()-4)));
    	System.out.println(MD5Util.crypt("5678"));
    }
}
