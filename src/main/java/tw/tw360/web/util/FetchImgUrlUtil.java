package tw.tw360.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class FetchImgUrlUtil {
	
	private static SimpleDateFormat sdf_img = new SimpleDateFormat("yyyyMMddHHmmss");
	
	
	public static String fetchUrl(HttpServletRequest request,String imgFileName,String IMAGES_PATH,File imgFile){	
		int idx = imgFileName.lastIndexOf("/")>0 ?imgFileName.lastIndexOf("/"): imgFileName.lastIndexOf("\\")+1;
		StringBuffer imageName = new StringBuffer();
		imageName.append(sdf_img.format(new Date())).append("_").append(imgFileName.substring(idx));
		
		File dirPath =new File(request.getSession().getServletContext().getRealPath("/")+ IMAGES_PATH);
		if(!dirPath.exists()){
			dirPath.mkdirs();
		}
		
		StringBuffer newPath = new StringBuffer(); 
		newPath.append(dirPath).append("/").append(imageName.toString());   
		try {
			copy(imgFile, newPath.toString());
		} catch (Exception e1) {
			e1.printStackTrace();
		}   
   
        String url = "http://" + request.getServerName() + ":"+ request.getServerPort() + request.getContextPath() 
        		+ IMAGES_PATH + imageName.toString();   
        return url;
	}
	private static void copy(File upload, String newPath) throws Exception {   
        FileOutputStream fos = new FileOutputStream(newPath);   
        FileInputStream fis = new FileInputStream(upload);   
        byte[] buffer = new byte[1024];   
        int len = 0;   
        while ((len = fis.read(buffer)) > 0) {   
            fos.write(buffer, 0, len);   
        }   
        fos.close();   
        fis.close();   
    } 
}
