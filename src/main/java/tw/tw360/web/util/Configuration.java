package tw.tw360.web.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author jenco
 * time：2015-2-6 下午5:49:45
 * 通用屬性配置，可以把key-value寫在configuration.properties裏
 * 通过Configuration.getInstance().getProperty(key)使用
 */
public class Configuration {
	String config = "configuration.properties";
	private  Properties properties = new Properties();
	private static Configuration instance = new Configuration();
	private  Configuration() {
		try {
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(config);
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Configuration getInstance() {
		return instance;
	}
	
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	public void setProperty(String key, String value) {
		properties.setProperty(key, value);
	}
}
