package tw.tw360.web.util;

import javax.servlet.http.HttpSession;

import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Position;

public class SessionUtil {

	private static final String LONGIN_SESSION = "LoginSession";
	private static final String ROLE_SESSION = "RoleSession";
	
	
	
	public static Position getRoleSession(HttpSession session){
		Object obj = session.getAttribute(ROLE_SESSION);
		if(obj != null){
			return (Position) session.getAttribute(ROLE_SESSION);
		}
		return null;
	}
	public static Position getRoleSession(){
		HttpSession session =Struts2Utils.getSession();
		Object obj = session.getAttribute(ROLE_SESSION);
		if(obj != null){
			return (Position) session.getAttribute(ROLE_SESSION);
		}
		return null;
	}
	
	public static Admin getLoginSession(HttpSession session){
		Object obj = session.getAttribute(LONGIN_SESSION);
		if(obj != null) {
			return (Admin) session.getAttribute(LONGIN_SESSION);
		}
		return null;
	}
	public static Admin getLoginSession(){
		HttpSession session =Struts2Utils.getSession();
		Object obj = session.getAttribute(LONGIN_SESSION);
		if(obj != null) {
			return (Admin) session.getAttribute(LONGIN_SESSION);
		}
		return null;
	}
}
