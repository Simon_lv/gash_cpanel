package tw.tw360.web.util.tccpay;

public enum TccpayPaymentPayStatus {
	待接單(0),
	不接單(1), 
	接單(2),
	已派遣(3),
	有收款(4),
	已發點(5),
	發點失敗(6),
	交易進行中取消(7),
	退單(9);
	
	int statusCode;
	TccpayPaymentPayStatus(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public int getStatusCode() {
		return statusCode;
	}
}
