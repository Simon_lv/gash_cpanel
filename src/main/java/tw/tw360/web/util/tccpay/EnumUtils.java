package tw.tw360.web.util.tccpay;

public class EnumUtils {
	/**
	 * 支付狀態值列表
	 * @author tink
	 *
	 */
	public enum PayStatus{
		待確認訂單,支付待確認,支付失敗,待請款,請款失敗,待發送鑽石,發送鑽石失敗,已支付,黑卡,已退款,待認證,google驗證錯誤_黑卡
	}
	/**
	 * 退点状态
	 * @author Administrator
	 *
	 */
	public enum RefundStatus{
		待退點,退點失敗,退點成功
	}
	
	/**
	 * 
	 * @author tink
	 *
	 */
	public enum FlagStatus{
		是,否
	}
	
	/**
	 * 金流商
	 * @author Administrator
	 *总表储值类型：bank 银行转账
	 */
	public enum PayType{
		mol,paypal,gash,googleplay,appstore,alipay,mycard
	}
	
	public enum ActivityParamter{
		ltpay,ltpayend,Activity
	}
	
	
	public enum VipLevel{
		VIP, //儲值就成為vip
		黃金VIP,//總儲值達2,990元以上
		白金VIP,//總儲值達10,000元以上
		鑽石VIP //總儲值達50,000元以上
	}
}
