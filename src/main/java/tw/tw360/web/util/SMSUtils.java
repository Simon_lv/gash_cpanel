package tw.tw360.web.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.security.KeyManagementException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import tw.tw360.common.net.HtmlUtil;

public class SMSUtils {
	final String account = "taxigash";
	final String password = "taxigash321";
	final static String yoyo8Url="https://www.yoyo8.com.tw/SMSBridge.php";
	
	public String sendSMS(boolean isTestMode, String toPhone, String sendMsg, String SourceProdID, String SourceMsgID ){
		if(isTestMode){
			return sendSMS_TestMode(toPhone, sendMsg, SourceProdID, SourceMsgID ,
					"0", // testErrStr, // 0 正常, etc 不正常
					"http://127.0.0.1:8288/sms/reportSMS.do");
		}else{
			return sendSMS(toPhone, sendMsg, SourceProdID, SourceMsgID ); 
		}
	}
	public String sendSMS(String toPhone, String sendMsg, String SourceProdID, String SourceMsgID ){
		String PasswordMd5 = md5(account+":"+password+":"+ SourceProdID+":"+SourceMsgID).toLowerCase();
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("MemberID",		this.account));
		urlParameters.add(new BasicNameValuePair("Password",		PasswordMd5));
		urlParameters.add(new BasicNameValuePair("MobileNo",		toPhone));
		urlParameters.add(new BasicNameValuePair("CharSet",			"U"));
		urlParameters.add(new BasicNameValuePair("SourceProdID",	SourceProdID));
		if (toPhone.startsWith("852") || toPhone.startsWith("853")) {
			urlParameters.add(new BasicNameValuePair("GlobalSms",		"Y"));
		}
		urlParameters.add(new BasicNameValuePair("SourceMsgID",		SourceMsgID));
		urlParameters.add(new BasicNameValuePair("SMSMessage",		sendMsg));
		try{
			
			return this.sendPost(yoyo8Url, urlParameters);
			
		}catch(Exception e){
			System.out.println("sendPostErr:"+e.toString());
			return "exception";
			
		}
	}
	
	static int testModeMsgId = 0;
	static int ststModeMsgId_b = 0;
	public String sendSMS_TestMode( 
			String toPhone,
			String sendMsg,
			String SourceProdID,
			String SourceMsgID, 
			String theTestRetStatus,
			String theReportUrl
			){
		if(theTestRetStatus.equals("0")){
// status=0&MemberID=taiwantaxi&MessageID=1421037153704855&UsedCredit=1&Credit=61&MobileNo=0953812885&retstr=Success
// getYoyo8Res MobileNo=0953812885&SourceProdID=TaiwanTaxiLuckyFarmCF_tst6&SourceMsgID=0953812885&MessageID=1421037153704855&status=0
			// 虛擬delay
			try {
			    Thread.sleep(1000);
			} catch (InterruptedException e) {
			    e.printStackTrace();
			}
			if(ststModeMsgId_b == 0){
				ststModeMsgId_b = (int)(Math.random()*1000000); 
			}
			String testModeMsgIdStr = String.format("tst%05X_%05d",ststModeMsgId_b,	testModeMsgId++);
			String retStr0 = 
					"status=0"
					+ "&MemberID=" + this.account 
					+ "&MessageID="+testModeMsgIdStr
					+ "&UsedCredit=1"
					+ "&Credit=61"
					+ "&MobileNo=" + toPhone;
			String retStr1 = 
					retStr0 
					+ "&retstr=Success";
			final String retStr2 = 
					retStr0 
					+ "&SourceProdID=" + SourceProdID
					+ "&SourceMsgID=" + SourceMsgID;
			final String theReportUrlFinal = theReportUrl;
			
			Thread thread1 = new Thread(new Runnable() {
                public void run() {
                        try { 
                            Thread.sleep(5000);
                            try{
                            	System.out.println("test mode get:" +theReportUrlFinal + "?" + retStr2);
                                HtmlUtil.getContent(
                                		theReportUrlFinal + "?" + retStr2, 
                                		"UTF-8");
                            }catch(Exception e){
                                e.printStackTrace(); 
                            }
                        } 
                        catch(InterruptedException e) { 
                            e.printStackTrace(); 
                        } 
                    
                }
			});
			thread1.start();
			
			
			return retStr1;
			
		}else{
			try {
			    Thread.sleep(200);
			} catch (InterruptedException e) {
			    e.printStackTrace();
			}
			return "status=" + theTestRetStatus; 
		}
		
	}
/**
 * 用來處理  yoyo8 回傳的內容,  為一個 post 的 param 內文格式
 *  ex: "status=0&MemberID=taiwantaxi&MessageID=1420634126421060&UsedCredit=1&Credit=67&MobileNo=0918452002&retstr=Success"	
 */
	public static HashMap<String, String> getHashMapFromStr(String smsRetStr){
//	      String smsRetStr="status=0&MemberID=taiwantaxi&MessageID=1420634126421060&UsedCredit=1&Credit=67&MobileNo=0918452002&retstr=Success"	;// for test
//	      String smsRetStr="status=14";	// for test

	      HashMap<String, String> hs=new HashMap<String,String>();
	      
	  	// --- get yoyo8Status String ---
			String[] ch1 = smsRetStr.split("&");
			if(ch1!=null){
			for(int ii=0;ii<ch1.length;ii++){
				String[] ch2 = ch1[ii].split("=");
					if(ch2!=null){
						if(ch2.length==2){
							String theKey = null;
							String theValue = null;
							try{
								theKey = URLDecoder.decode(ch2[0],"UTF-8");
								try{
									theValue = URLDecoder.decode(ch2[1],"UTF-8"); 
								} catch(Exception e){
								}
							} catch(Exception e){
								
							}
							
							if( theKey != null){
								if (theValue != null){
									hs.put(theKey, theValue);
								}
							}
						}
					}
				}
			}
			return hs;
	}

	private final String USER_AGENT = "Mozilla/5.0";
		// HTTP POST request
	private String sendPost(String url,List<NameValuePair> urlParameters) throws Exception {
		 
//		String url = "https://selfsolve.apple.com/wcResults.do";
 
		HttpClient client = new DefaultHttpClient();
		client.getConnectionManager().getSchemeRegistry().register(getScheme());
		HttpPost post = new HttpPost(url);
 
		// add header
		post.setHeader("User-Agent", USER_AGENT);
		post.setHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8"); 
		
//		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
//		urlParameters.add(new BasicNameValuePair("sn", "C02G8416DRJM"));
//		urlParameters.add(new BasicNameValuePair("cn", ""));
//		urlParameters.add(new BasicNameValuePair("locale", ""));
//		urlParameters.add(new BasicNameValuePair("caller", ""));
//		urlParameters.add(new BasicNameValuePair("num", "12345"));
 
		post.setEntity(new UrlEncodedFormEntity(urlParameters,"UTF-8"));
 
		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + 
                                    response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
 
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
 
		System.out.println(result.toString());
		return result.toString();
 
	}

	public static String md5(String str) {
		String md5=null;
		try {
			MessageDigest md=MessageDigest.getInstance("MD5");
		    byte[] barr=md.digest(str.getBytes());  //將 byte 陣列加密
		    StringBuffer sb=new StringBuffer();  //將 byte 陣列轉成 16 進制
		    for (int i=0; i < barr.length; i++) {
		    	sb.append(byte2Hex(barr[i]));
		    }
		    String hex=sb.toString();
		    md5=hex.toUpperCase(); //一律轉成大寫
		}catch(Exception e) {
			e.printStackTrace();
		}
		return md5;
	}
	public static String byte2Hex(byte b) {
		String[] h={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};
		int i=b;
		if (i < 0) {
			i += 256;
		}
		return h[i/16] + h[i%16];
	}
/*	
	public static HttpClient getHttpClient() {
	    try {
	        SSLSocketFactory sf = new SSLSocketFactory(new TrustStrategy(){
	            @Override
	            public boolean isTrusted(X509Certificate[] chain,
	                    String authType) throws CertificateException {
	                return true;
	            }
	        });
	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("https", 443, sf));
	        ClientConnectionManager ccm = new ThreadSafeClientConnManager(registry);
	        return new DefaultHttpClient(ccm);
	    } catch (Exception e) {
	        return new DefaultHttpClient();
	    }
	}
*/
	public static Scheme getScheme() {

    	SSLSocketFactory sf=null ;
        SSLContext sslContext = null;
        StringWriter writer;
        try {
            sslContext = SSLContext.getInstance("TLS")  ;
            sslContext.init(null,null,null);
        } catch (NoSuchAlgorithmException e) {
            //<YourErrorHandling>
        }  catch (KeyManagementException e){
            //<YourErrorHandling>
        }

        try{
            sf = new SSLSocketFactory(sslContext, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        } catch(Exception e) {
            //<YourErrorHandling>
        }
        return new Scheme("https",443,sf);
	    
	}
}
