package tw.tw360.web.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.security.springsecurity.SpringSecurityUtils;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Position;
import tw.tw360.service.AdminManager;
import tw.tw360.service.PositionManager;
import tw.tw360.util.PrintUtil;

public class AdminAction extends CRUDActionSupport<Admin>{
	private static final long serialVersionUID = 8504833727848367765L;
	
	private static final String TAG = "AdminAction";
	@Autowired
	private AdminManager adminManager;
	@Autowired
    private PositionManager positionManager;
	
	private Page<Admin> page = new Page<Admin>(300);
	
	public Page<Admin> getPage() {
		return page;
	}

	public void setPage(Page<Admin> page) {
		this.page = page;
	}

	public Admin getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,"list",request);
		
		adminManager.queryUserList(page, map);
		request.setAttribute("map", map);
		
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String addUser(){
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,"addUser",request);
		List<Position> list = positionManager.queryAllPosition();
		request.setAttribute("list", list);
		return "add";
	}
	
	public void saveUser(){
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,"saveUser",request);
		adminManager.saveUser(map);
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath()+ "/admin/admin!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void updateUserStatus(){
		HttpServletRequest request = Struts2Utils.getRequest();
		 HttpServletResponse response = Struts2Utils.getResponse();
		int id = new Integer(request.getParameter("id"));
		int status = new Integer(request.getParameter("status"));
		int row = adminManager.updateUserStatus(id, status);
		try {
			PrintWriter out=response.getWriter();
			if(row>0){
				out.print(status);
			}else{
				out.print(0);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 修改用戶密碼
	 * @author Donald
	 * time:2013-12-9 10:08:17
	 * */
	public void changePassword()throws Exception {
		
		HttpServletRequest request = Struts2Utils.getRequest();
		String newPwd = request.getParameter("password");
		Admin admin = (Admin)request.getSession().getAttribute("LoginSession");
		
		int status = adminManager.updatePwd(admin.getId(), newPwd);
		
		HttpServletResponse response = Struts2Utils.getResponse();
		PrintWriter out = response.getWriter();
		
		out.print(status);
		out.flush();
		out.close();
	}
	
	public String passwd(){
		return "passwd";
	}
	
	
	public String edit(){
		HttpServletRequest request = Struts2Utils.getRequest();
		List<Position> list = positionManager.queryAllPosition();
		int id = new Integer(request.getParameter("id"));
		
		request.setAttribute("list", list);
		Admin user = adminManager.queryUserinfo(id);
		
		request.setAttribute("u", user);
		
		return "edit";
	}
	
	public void editAdmin() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(null,"editAdmin",request);
		adminManager.updateAdmin(map);
		request.setAttribute("result", 1);
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath()+ "/admin/admin!user.action?result=1");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void updateUserInfo(){
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(null,"addHoleItem",request);
		adminManager.updateUserInfo(map);
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath()+ "/admin/admin!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** 检测用户名是否存在
	 * @author:jack
	 * time:2013-11-18  下午8:24:47
	 */
	public void checkSameUser(){
		
		HttpServletRequest request =  Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(null, "checkSameUser", request);
		int row = 0;
		if(null!=map.get("login_name") && !"".equals(map.get("login_name"))){
			row = adminManager.checkSameUser(map);
		}
		try {
			PrintWriter out = Struts2Utils.getResponse().getWriter();
				out.print(row);
				out.flush();
				out.close();
			
		} catch (Exception e) {
			
		}	
	}
	
	public String user(){
		HttpServletRequest request = Struts2Utils.getRequest();
		String userName = SpringSecurityUtils.getCurrentUserName();
		Admin admin = adminManager.findByUsername(userName);
		request.setAttribute("admin", admin);
		return "user";
	}
}
