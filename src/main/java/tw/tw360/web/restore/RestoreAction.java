package tw.tw360.web.restore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.security.springsecurity.SpringSecurityUtils;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Driver;
import tw.tw360.dto.Restore;
import tw.tw360.query.PaymentQuery;
import tw.tw360.service.AdminManager;
import tw.tw360.service.DriverManager;
import tw.tw360.service.PaymentManager;
import tw.tw360.service.RestoreManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ReportExcel;

/**
 * @author jenco
 * time：2015-2-4 上午10:47:25
 */
@SuppressWarnings("serial")
public class RestoreAction extends CRUDActionSupport<Restore> {
	
	private static final String TAG = "RestoreAction";
	
	@Autowired
	private RestoreManager restoreManager;
	@Autowired
	private PaymentManager paymentManager;
	@Autowired
	private AdminManager adminManager;
	@Autowired
	private DriverManager driverManager;
	
	private Page<Restore> page = new Page<Restore>(15);
	@Override
	public Restore getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		
		page = restoreManager.findAll(page,map);
        request.setAttribute("map", map);
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String toAdd() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String,String> map = PrintUtil.printAndReturnRequest(TAG, "queryCredit", request);
		request.setAttribute("map", map);
		String carId = map.get("carId");
		
		request.getSession().removeAttribute("paymentList");
		
		if(StringUtils.isBlank(carId)) {
			request.setAttribute("errMsg", "请先輸入隊編!");
			return "add";
		}
		
		List<PaymentQuery> paymentList = paymentManager.queryNeedRestoreByCarId(carId);
		if(paymentList == null || paymentList.size() == 0) {
			request.setAttribute("none", "true");
			return "add";
		}
		int sum = 0;
		for(PaymentQuery p : paymentList) {
			sum += p.getPrice();
		}
		request.setAttribute("sum", sum);
		request.setAttribute("paymentList", paymentList);
		request.getSession().setAttribute("paymentList", paymentList);
		
		return "add";
	}
	
	public void batchAddRestore() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		@SuppressWarnings("unchecked")
		List<PaymentQuery> paymentList = (List<PaymentQuery>) request.getSession().getAttribute("paymentList");
		
		if(paymentList == null || paymentList.size() == 0) {
			try {
				response.sendRedirect(request.getContextPath()+ "/restore/restore!list.action");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		String userName = SpringSecurityUtils.getCurrentUserName();
		Admin admin = adminManager.findByUsername(userName);
		restoreManager.batchAddRestore(paymentList,admin);
		
		long sum = 0;
		String carId = paymentList.get(0).getCarId();
		for(PaymentQuery p : paymentList) {
			sum += p.getPrice();
		}
		
		Driver dr = driverManager.findBycarId(carId);
		int nonRestore = dr.getNonRestore();
		int credit = dr.getCredit();
		if(nonRestore <= sum) {
			nonRestore = 0;
		}
		else {
		    nonRestore = (int)(sum - nonRestore);	
		}
		if(nonRestore >= credit) {//當 non_restore >= credit 則 can_sell=2
			dr.setCanSell(2);
		}
		else
		{
			dr.setCanSell(1);
		}
		dr.setNonRestore(nonRestore);
		driverManager.update(dr);
		
		try {
			response.sendRedirect(request.getContextPath()+ "/restore/restore!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Page<Restore> getPage() {
		return page;
	}

	public void setPage(Page<Restore> page) {
		this.page = page;
	}
	
	   /*
     * function : 匯出excel
     * description : 傳入搜尋條件，將結果寫入excel
     */
    public void export() {
        HttpServletRequest request = Struts2Utils.getRequest();
        HttpServletResponse response = Struts2Utils.getResponse();
        HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
                "export", request);
        String[] dataTitles = { "隊編","回存現金","回存時間", "後臺操作人員帳號"};
        List<Restore> restores = restoreManager.queryALLRestoreList(map);
        List<Object[]> list = new ArrayList<Object[]>();

        if (restores != null && !restores.isEmpty()) {
            for (Restore restore : restores) {
                Object[] o = new Object[4];
                o[0] = restore.getCarId();
                o[1] = restore.getMoney();
                o[2] = restore.getRestoreTime();
                o[3] = restore.getAdminName();
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "回存紀錄列表" + System.currentTimeMillis(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
