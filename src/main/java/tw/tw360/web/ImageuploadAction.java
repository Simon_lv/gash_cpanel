package tw.tw360.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

public class ImageuploadAction extends CRUDActionSupport<Object> {	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final String IMAGES_PATH = "/upload/images/";
	
	private File imgFile;
	private String imgFileName;  
	private String imgTitle; 
	private String imgWidth;  
	private String imgHeight; 
	private String align; 
	private String type;
		 	
	public File getImgFile() {
		return imgFile;
	}

	public void setImgFile(File imgFile) {
		this.imgFile = imgFile;
	}

	public String getImgFileName() {
		return imgFileName;
	}

	public void setImgFileName(String imgFileName) {
		this.imgFileName = imgFileName;
	}

	public String getImgTitle() {
		return imgTitle;
	}

	public void setImgTitle(String imgTitle) {
		this.imgTitle = imgTitle;
	}

	public String getImgWidth() {
		return imgWidth;
	}

	public void setImgWidth(String imgWidth) {
		this.imgWidth = imgWidth;
	}

	public String getImgHeight() {
		return imgHeight;
	}

	public void setImgHeight(String imgHeight) {
		this.imgHeight = imgHeight;
	}

	public String getAlign() {
		return align;
	}

	public void setAlign(String align) {
		this.align = align;
	}	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public void uploadImage(){
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();

		String saveImagePath = StringUtils.isBlank(getType()) ?IMAGES_PATH :(IMAGES_PATH + getType()+ "/");
		
		int idx = getImgFileName().lastIndexOf("/")>0 ?getImgFileName().lastIndexOf("/"): getImgFileName().lastIndexOf("\\")+1;
		StringBuffer imageName = new StringBuffer();
		imageName.append(sdf.format(new Date())).append("_").append(getImgFileName().substring(idx));
		
		File dirPath =new File(request.getSession().getServletContext().getRealPath("/")+ saveImagePath);
		if(!dirPath.exists()){
			dirPath.mkdirs();
		}
		
		StringBuffer newPath = new StringBuffer(); 
		newPath.append(dirPath).append("/").append(imageName.toString());   
		try {
			copy(imgFile, newPath.toString());
		} catch (Exception e1) {
			e1.printStackTrace();
		}   
		
		String id = "content";   
        String url = "http://" + request.getServerName() + ":"+ request.getServerPort() + request.getContextPath() 
        		+ saveImagePath + imageName.toString();   

		String border = "0";   
		PrintWriter out = null;
		try {
			out = encodehead(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		JSONObject obj = new JSONObject();
        obj.put("error", 0);
        obj.put("url", url);

		
		out.write(obj.toJSONString());
		out.flush();
		out.close(); 
	}	
	
	private void copy(File upload, String newPath) throws Exception {   
        FileOutputStream fos = new FileOutputStream(newPath);   
        FileInputStream fis = new FileInputStream(upload);   
        byte[] buffer = new byte[1024];   
        int len = 0;   
        while ((len = fis.read(buffer)) > 0) {   
            fos.write(buffer, 0, len);   
        }   
        fos.close();   
        fis.close();   
    }  
	
	private PrintWriter encodehead(HttpServletRequest request,HttpServletResponse response) throws IOException{
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		return response.getWriter();
	}
}
