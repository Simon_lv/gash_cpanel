package tw.tw360.web.smsbackrecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.BackSmsRecord;
import tw.tw360.dto.SmsRecord;
import tw.tw360.service.BackSmsRecordManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ReportExcel;

public class SmsbackrecordAction extends CRUDActionSupport<HashMap<String, String>> {
	private static final long serialVersionUID = 1L;

	private static final String TAG = SmsbackrecordAction.class.getName();

	@Autowired
	private BackSmsRecordManager backSmsRecordManager;

	private Page<HashMap<String, String>> page = new Page<HashMap<String, String>>(15);

	public Page<HashMap<String, String>> getPage() {
		return page;
	}

	public void setPage(Page<HashMap<String, String>> page) {
		this.page = page;
	}

	@Override
	public HashMap<String, String> getModel() {
		return null;
	}

	@Override
	public String delete() throws Exception {
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		if(map.get("mobileNo") == null) {
			return "list";
		}
		page = backSmsRecordManager.findByViewCondition(page, map.get("mobileNo"), map.get("startTime"), map.get("endTime"), false);
		request.setAttribute("map", map);
		System.out.println(page.getResult());
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {

	}

	@Override
	public String save() throws Exception {
		return null;
	}
	
	public void export() {
        HttpServletRequest request = Struts2Utils.getRequest();
        HttpServletResponse response = Struts2Utils.getResponse();
        HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
                "export", request);
        String[] dataTitles = { "帳務編號","手機號碼","簡訊內容", "訂單編號", "時間"};
        List<HashMap<String , String>> backSmsRecords = backSmsRecordManager.findByViewCondition(page, map.get("mobileNo"), map.get("startTime"), map.get("endTime"), true).getResult();
        List<Object[]> list = new ArrayList<Object[]>();

        if (backSmsRecords != null && !backSmsRecords.isEmpty()) {
            for (HashMap<String , String> smsRecord : backSmsRecords) {
                Object[] o = new Object[5];
                
                o[0] = smsRecord.get("messageId");
    			o[1] = smsRecord.get("mobileNo");
    			o[2] = smsRecord.get("smsMessage");
    			o[3] = smsRecord.get("orderIds");
    			o[4] = smsRecord.get("createTime");
                
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "司機回覆明細" + System.currentTimeMillis(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
