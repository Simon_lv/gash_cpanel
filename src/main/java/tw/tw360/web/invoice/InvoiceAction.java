package tw.tw360.web.invoice;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Driver;
import tw.tw360.service.InvoiceManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ExcelReader;

public class InvoiceAction extends CRUDActionSupport<Driver> {
	private static final String TAG = "InvoiceAction";
	/**
	 * 
	 */
	private static final long serialVersionUID = -8463166687654734194L;

	private File excelFile;

	private String excelFileName;

	@Autowired
	private InvoiceManager service;

	@Override
	public Driver getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public String save() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
				"save", request);
		ExcelReader er = new ExcelReader();
		FileInputStream fis = new FileInputStream(excelFile);
		Map<Integer, String> data = er.readExcelContent(fis);
		Map<String, Object> result = service.saveInvoice(data);
		for (Entry<String, Object> entry : result.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
		return "save";
	}

	public String getExcelFileName() {
		return excelFileName;
	}

	public void setExcelFileName(String excelFileName) {
		this.excelFileName = excelFileName;
	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

}
