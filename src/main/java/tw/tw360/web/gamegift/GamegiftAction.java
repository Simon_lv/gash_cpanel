package tw.tw360.web.gamegift;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.ActivityGameGift;
import tw.tw360.dto.GameGift;
import tw.tw360.service.ActivityGameGiftManager;
import tw.tw360.service.GameGiftManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.Configuration;
import tw.tw360.web.util.ReportExcel;

public class GamegiftAction extends CRUDActionSupport<ActivityGameGift> {
    
	private static final long serialVersionUID = 1L;
	
	private static final String TAG = "GamegiftAction";
	
	@Autowired
	ActivityGameGiftManager activityGameGiftManager;	
	@Autowired
	GameGiftManager gameGiftManager;
	
	private Page<ActivityGameGift> page = new Page<ActivityGameGift>(15);
	private Page<GameGift> pageMgr = new Page<GameGift>(30);

	public Page<ActivityGameGift> getPage() {
		return page;
	}

	public void setPage(Page<ActivityGameGift> page) {
		this.page = page;
	}
	
	public Page<GameGift> getPageMgr() {
		return pageMgr;
	}

	public void setPageMgr(Page<GameGift> pageMgr) {
		this.pageMgr = pageMgr;
	}

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.ModelDriven#getModel()
	 */
	@Override
	public ActivityGameGift getModel() {
		// Do Nothing
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springside.modules.web.struts2.CRUDActionSupport#delete()
	 */
	@Override
	public String delete() throws Exception {
		// Do Nothing
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springside.modules.web.struts2.CRUDActionSupport#list()
	 */
	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		page = activityGameGiftManager.queryForPage(page, map);
		request.setAttribute("map", map);
		List<Map<String,String>> calcExchangeList = activityGameGiftManager.queryCalcExchange();
		request.setAttribute("calcExchangeList", calcExchangeList);
		
		List<GameGift> giftList = gameGiftManager.queryAllGift();
		request.setAttribute("giftList", giftList);
		
		return "list";
	}

	/* (non-Javadoc)
	 * @see org.springside.modules.web.struts2.CRUDActionSupport#prepareModel()
	 */
	@Override
	protected void prepareModel() throws Exception {
		// Do Nothing		
	}

	/* (non-Javadoc)
	 * @see org.springside.modules.web.struts2.CRUDActionSupport#save()
	 */
	@Override
	public String save() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();		
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "save", request);
		
		if(StringUtils.isBlank(map.get("id"))) {		
			GameGift gameGift = new GameGift();
			
			gameGift.setName(map.get("name"));
			gameGift.setDescription(map.get("description"));
			gameGift.setDeadline(DateUtils.parseDate(map.get("deadline"), "yyyy-MM-dd"));
			gameGift.setImgUrl("/images/" + map.get("imgUrl"));
			gameGift.setPrice(StringUtils.isEmpty(map.get("id")) ? 0 : 1);
			gameGift.setStatus(1);
			gameGiftManager.add(gameGift);
		}
		else {
			long id = Long.parseLong(map.get("id"));
			GameGift gameGift = gameGiftManager.loadById(id);
			
			gameGift.setName(map.get("name"));
			gameGift.setDescription(map.get("description"));
			gameGift.setDeadline(DateUtils.parseDate(map.get("deadline"), "yyyy-MM-dd"));
			gameGift.setImgUrl("/images/" + map.get("imgUrl"));
			gameGift.setPrice(StringUtils.isEmpty(map.get("price")) ? 0 : 1);
			
			gameGiftManager.update(gameGift);
		}
		
		return manage();
	}
	
	/*
     * function : 匯出excel
     * description : 傳入搜尋條件，將結果寫入excel
     */
    public void export() {
        HttpServletRequest request = Struts2Utils.getRequest();
        HttpServletResponse response = Struts2Utils.getResponse();
        HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
                "export", request);
        String[] dataTitles = { "兌換序號","兌換遊戲名稱","用戶姓名", "用戶電話", "簡訊發送狀態","領取時間"};
        List<ActivityGameGift> activityGameGifts = activityGameGiftManager.queryAll(map);
        List<Object[]> list = new ArrayList<Object[]>();

        if (activityGameGifts != null && !activityGameGifts.isEmpty()) {
            for (ActivityGameGift activityGameGift : activityGameGifts) {
                Object[] o = new Object[6];
                o[0] = activityGameGift.getGiftSn();
                o[1] = activityGameGift.getGameName();
                o[2] = activityGameGift.getUserName();
                o[3] = activityGameGift.getUserMobile();
                o[4] = activityGameGift.getSmsStatus();
                o[5] = activityGameGift.getReceiveTime();
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "遊戲虛寶資料列表" + System.currentTimeMillis(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 管理頁
     * 
     * @return
     * @throws Exception
     */
    public String manage() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "manage", request);
		String gamecard = Configuration.getInstance().getProperty("gamecard_url");
		
		pageMgr = gameGiftManager.findAll(pageMgr, map);
		request.setAttribute("map", map);
		request.setAttribute("gamecard", gamecard);

		return "manage";
	}
    
    /**
     * 操作
     * 
     * @throws Exception
     */
    public void operation() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "operation", request);
		
		if(StringUtils.isNotBlank(map.get("type")) && StringUtils.isNotBlank(map.get("id"))) {
			int type = Integer.parseInt(map.get("type"));
			long id = Long.parseLong(map.get("id"));
			
			switch(type){
				case 1:
					gameGiftManager.updateToTop(id, 1);	 // 目前畫面暫無置頂
					break;
				case 2:  // 上移
				case 3:  // 下移
					GameGift gameGift = gameGiftManager.loadById(id);
					GameGift gameGift_offset = gameGiftManager.queryOffsetRecord(id, type > 2 ? 1 : -1);
					
					if(gameGift != null && gameGift_offset != null) {
						gameGiftManager.updateOrder(id, gameGift_offset.getOrder());
						gameGiftManager.updateOrder(gameGift_offset.getId(), gameGift.getOrder());	
					}
					else if(type == 2 && gameGift_offset == null && gameGift.getOrder() > 1) {
						gameGiftManager.updateOrder(id, gameGift.getOrder() - 1);
					}
					else if(type == 3 && gameGift_offset == null) {
						gameGiftManager.updateOrder(id, gameGift.getOrder() + 1);
					}
					
					break;
				case 4:
					gameGiftManager.updateStatus(id, 2);
					break;										
			}
		}
		
		try {
			PrintWriter out = response.getWriter();
			out.print(map.get("id"));
			out.flush();
			out.close();
		} 
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增
	 * 
	 * @return
	 * @throws Exception
	 */
	public String toAdd() throws Exception {
		return "add";
	}
	
	public String toEdit() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "toEdit", request);
		long id = Long.parseLong(map.get("id"));
		GameGift gameGift = gameGiftManager.loadById(id);
		gameGift.setImgUrl(gameGift.getImgUrl().replaceFirst("/images/", ""));
		request.setAttribute("gameGift", gameGift);
		
		return "add";
	}	
}