package tw.tw360.web.giftpoints;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.service.SendSMSForGiftPointsManager;
import tw.tw360.util.PrintUtil;

public class GiftpointsAction extends CRUDActionSupport<Object> {
	private static final String TAG = GiftpointsAction.class.getName();
	@Autowired
	private SendSMSForGiftPointsManager sendSMSForGiftPointsManager;
	@Override
	public Object getModel() {
		return null;
	}

	@Override
	public String delete() throws Exception {
		return null;
	}

	@Override
	public String list() throws Exception {
		return "view";
	}

	@Override
	protected void prepareModel() throws Exception {
		
	}

	@Override
	public String save() throws Exception {
		return null;
	}
	
	public String sendsms() throws Exception {
		String msg = null;
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "sendsms", request);
		
		try {
			int point = Integer.parseInt(map.get("point"));
			msg = sendSMSForGiftPointsManager.sendSMS(1,point);
		} catch (Exception e) {
			msg = "point="+map.get("point")+"不是數字";
		}
		
		request.setAttribute("map", map);
		request.setAttribute("msg", msg);
		return "view";
	}

}
