package tw.tw360.web.soldpoint;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Card;
import tw.tw360.dto.GiftCardPointCount;
import tw.tw360.dto.InvoiceCard;
import tw.tw360.dto.TccInvoice;
import tw.tw360.service.CardManager;
import tw.tw360.service.TccInvoiceManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ReportExcel;

public class SoldpointAction extends CRUDActionSupport<Card> {
	private static final long serialVersionUID = 1L;
	private static final String TAG="SoldpointAction";
	
	@Autowired
	private CardManager cardManager;
	@Autowired
	private TccInvoiceManager tccInvoiceManager;
	
	@Override
	public Card getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		return null;
	}

	@Override
	public String list() throws Exception {
		List<InvoiceCard> list = cardManager.findAllForSoldpoint();
		HttpServletRequest request = Struts2Utils.getRequest();
		request.setAttribute("list", list);
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		return null;
	}
	
	public void saveInvoice() {
		TccInvoice tccInvoice = new TccInvoice();
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
              "saveInvoice", request);
		
		tccInvoice.setTitle(map.get("title"));
		tccInvoice.setInvoiceNo(map.get("invoiceNo"));
		
		map.remove("title");
		map.remove("invoiceNo");
		
//		tccInvoice.setRemark("");
		tccInvoice.setStatus(1);
		tccInvoice.setCreateTime(new Timestamp(System.currentTimeMillis()));
		
		tccInvoice = tccInvoiceManager.insert(tccInvoice);
		
		ArrayList<InvoiceCard> used = parse(map);
		cardManager.updateForTccInvoice(used, tccInvoice.getId());
		
		
        HttpServletResponse response = Struts2Utils.getResponse();
        
        String[] dataTitles = { "點卡面額","序號","密碼"};
        List<Card> Cards = cardManager.queryByTccInvoiceId(tccInvoice.getId());
        List<Object[]> list = new ArrayList<Object[]>();

        if (Cards != null && !Cards.isEmpty()) {
            for (Card smsRecord : Cards) {
                Object[] o = new Object[dataTitles.length];
                o[0] = smsRecord.getPrice();
                o[1] = smsRecord.getSn();
                o[2] = smsRecord.getPw();
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "Gash點卡申請 發票號碼:"+tccInvoice.getInvoiceNo(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	private ArrayList<InvoiceCard> parse(HashMap<String, String> map) {
		Set<String> keys = map.keySet();
		ArrayList<InvoiceCard> result = new ArrayList<InvoiceCard>();
		for(String key:keys) {
			try {
				InvoiceCard e = new InvoiceCard();
				int ikey = Integer.parseInt(key);
				int value = Integer.parseInt(map.get(key));
				e.setPrice(ikey);
				e.setCount(value);
				result.add(e);
			} catch(Exception e) {
				
			}
		}
		return result;
	}
	
	public void checkInvoiceNo() throws IOException {
		HttpServletResponse response = Struts2Utils.getResponse();
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "checkInvoiceNo", request);
		
		JSONObject js = new JSONObject();
		
		boolean flag = tccInvoiceManager.checkInvoiceNo(map.get("invoiceNo") == null? "":map.get("invoiceNo"));
		map.remove("invoiceNo");
		ArrayList<InvoiceCard> parse = parse(map);
		boolean flag2 = cardManager.checkForTccInvoice(parse);
		
		js.accumulate("flag", flag);
		js.accumulate("flag2", flag2);
		writeJasonMsg(response, js.toString());
	}

	private void writeJasonMsg(HttpServletResponse response, String msg) throws IOException {
		PrintWriter out;
		out = response.getWriter();
		out.print(msg);
		out.flush();
		out.close();
	}

	
}
