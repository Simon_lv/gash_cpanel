package tw.tw360.web.smsrecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Driver;
import tw.tw360.dto.SmsRecord;
import tw.tw360.service.SMSRecordManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ReportExcel;

/**
 * @author jenco
 * time：2015-3-2 下午3:57:43
 */
public class SmsrecordAction extends CRUDActionSupport<SmsRecord> {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String TAG="SmsRecoredAction";
	
	@Autowired
	SMSRecordManager smsRecordManager;
	
	private Page<SmsRecord> page = new Page<SmsRecord>(15);

	public Page<SmsRecord> getPage() {
		return page;
	}

	public void setPage(Page<SmsRecord> page) {
		this.page = page;
	}

	@Override
	public SmsRecord getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		page = smsRecordManager.queryForPage(page, map);
		request.setAttribute("map", map);
		System.out.println(page.getResult());
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
     * function : 匯出excel
     * description : 傳入搜尋條件，將結果寫入excel
     */
    public void export() {
        HttpServletRequest request = Struts2Utils.getRequest();
        HttpServletResponse response = Struts2Utils.getResponse();
        HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG,
                "export", request);
        String[] dataTitles = { "帳務編號","簡訊所花點數","廠商登入帳號", "剩餘點數", "手機號碼","狀況"
                ,"建立時間","回報狀態","廠商自訂字串1","廠商自訂字串2","回報時間"};
        List<SmsRecord> smsRecords = smsRecordManager.queryNoPage(map);
        List<Object[]> list = new ArrayList<Object[]>();

        if (smsRecords != null && !smsRecords.isEmpty()) {
            for (SmsRecord smsRecord : smsRecords) {
                Object[] o = new Object[11];
                o[0] = smsRecord.getMessageId();
                o[1] = smsRecord.getUsedCredit();
                o[2] = smsRecord.getMemberId();
                o[3] = smsRecord.getCredit();
                o[4] = smsRecord.getMobileNo();
                o[5] = smsRecord.getStatus().equals("0") ? "用戶手機接收成功" : "用戶手機接收失敗";
                o[6] = smsRecord.getCreateTime();
                o[7] = smsRecord.getReportStatus();
                o[8] = smsRecord.getSourceProdId();
                o[9] = smsRecord.getSourceMessageId();
                o[10] = smsRecord.getReportTime();
                list.add(o);
            }
        }

        try {
            // reportExcel.reportExcel(request, response,
            // "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
            ReportExcel.reportExcel(request, response,
                    "簡訊發送明細列表" + System.currentTimeMillis(), "", dataTitles, list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
