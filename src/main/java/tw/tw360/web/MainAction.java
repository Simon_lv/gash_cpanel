package tw.tw360.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.security.springsecurity.SpringSecurityUtils;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Authorities;
import tw.tw360.dto.Position;
import tw.tw360.service.AdminManager;
import tw.tw360.service.AuthoritiesManager;
import tw.tw360.service.PositionAuthorityManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.SessionUtil;

/**
 * 用户登录
 * 
 * @author Donald time：2013-11-2 9:15:10
 * @version 1.3
 * */
@SuppressWarnings("serial")
@Results({
		@Result(name = CRUDActionSupport.ERROR, location = "fail.jsp"),
		@Result(name = CRUDActionSupport.RELOAD, location = "main.action", type = "redirect") })
public class MainAction extends CRUDActionSupport<Object> {

	private static final String TAG = "MainAction";
	private static final Logger LOG = LoggerFactory.getLogger(MainAction.class);
	@Autowired
	private AdminManager adminManager;
	@Autowired
	private PositionAuthorityManager positionAuthorityManager;
	@Autowired
	private AuthoritiesManager authoritiesManager;

	private Object entity;

	public Object getModel() {
		return entity;
	}

	@Override
	protected void prepareModel() throws Exception {

	}

	@Override
	@Action(results = { @Result(name = CRUDActionSupport.SUCCESS, location = "/main.jsp") })
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printRequest(TAG, "list", request);
		LOG.info("====================list==========================");
		String userName = SpringSecurityUtils.getCurrentUserName();
		if (userName != null) {
			Admin admin = adminManager.findByUsername(userName);
			if (admin != null) {
				HttpSession session = Struts2Utils.getSession();
				session.setAttribute("LoginSession", admin);
				Admin adminTmp = adminManager.queryUserinfo(Integer
						.parseInt(admin.getId().toString()));
				Position p = new Position();
				p.setId(new Long(adminTmp.getPositionId()));
				p.setName(adminTmp.getPositionName());
				LOG.info(
						"Position id={},name={}",
						new Object[] { adminTmp.getPositionId(),
								adminTmp.getPositionName() });
				session.setAttribute("RoleSession", p);
				String roleNames = "";
				if (adminTmp != null
						&& adminTmp.getPositionName().contains("(")) {
					roleNames = adminTmp.getPositionName().substring(0,
							adminTmp.getPositionName().indexOf("("));
				} else {
					roleNames = adminTmp.getPositionName();
				}
				session.setAttribute("roleNames", roleNames);

			}
		}
		return SUCCESS;
	}

	@Override
	public String input() throws Exception {

		return INPUT;
	}

	@Override
	public String save() throws Exception {

		return RELOAD;
	}

	@Override
	public String delete() throws Exception {

		return RELOAD;
	}

	/*
	 * public void login(){
	 * 
	 * HttpServletRequest request = Struts2Utils.getRequest();
	 * HttpServletResponse response = Struts2Utils.getResponse();
	 * 
	 * 
	 * String name = request.getParameter("username"); String password =
	 * request.getParameter("password"); password = MD5Util.MD5Encode(password);
	 * 
	 * Admin admin = adminManager.loginSystem(name, password);
	 * 
	 * if(admin != null){ HttpSession session =Struts2Utils.getSession();
	 * session.setAttribute("LoginAdminSession", admin);
	 * 
	 * try { request.getRequestDispatcher("/main.jsp").forward(request,
	 * response); } catch (ServletException e) { e.printStackTrace(); } catch
	 * (IOException e) { e.printStackTrace(); }
	 * 
	 * }else{ PrintUtil.outputContent("用户名或密码错误!"); try {
	 * request.getRequestDispatcher("/login.jsp").forward(request, response); }
	 * catch (ServletException e) { e.printStackTrace(); } catch (IOException e)
	 * { e.printStackTrace(); } }
	 * 
	 * }
	 */

	/**
	 * 查询当前登录用户的权限
	 * 
	 * @author Donald time:2013-11-4 11:55:20
	 * */
	public void left() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		PrintUtil.printRequest(TAG, "left", request);
		Admin admin = SessionUtil.getLoginSession();
		int positionId = positionAuthorityManager.queryAdminPosition(admin
				.getId());
		List<Authorities> list = authoritiesManager
				.queryPositionAuthorities(positionId);
		HashMap<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < list.size(); i++) {
			Authorities ath = new Authorities();
			ath = list.get(i);
			map.put(ath.getName(), ath.getName());
		}

		request.setAttribute("map", map);

		try {
			request.getRequestDispatcher("/left.jsp")
					.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 查询当前登录用户的权限
	 * 
	 * @author Donald time:2013-11-4 11:55:20
	 * */
	public void queyrAdminPositionAuthorities() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		PrintUtil.printRequest(TAG, "queyrAdminPositionAuthorities", request);
		Admin admin = SessionUtil.getLoginSession();
		int positionId = positionAuthorityManager.queryAdminPosition(admin
				.getId());
		List<Authorities> list = authoritiesManager
				.queryPositionAuthorities(positionId);
		HashMap<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < list.size(); i++) {
			Authorities ath = new Authorities();
			ath = list.get(i);
			map.put(ath.getName(), ath.getName());
		}

		request.setAttribute("map", map);

		try {
			request.getRequestDispatcher("/left.jsp")
					.forward(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
