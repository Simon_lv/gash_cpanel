package tw.tw360.web.tccpaypayment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.common.encrypt.MD5Util;
import tw.tw360.tccpay.model.Customer;
import tw.tw360.tccpay.model.Driver;
import tw.tw360.tccpay.model.Member;
import tw.tw360.tccpay.model.PaymentForView;
import tw.tw360.tccpay.service.CustomerManager;
import tw.tw360.tccpay.service.InvoiceManager;
import tw.tw360.tccpay.service.MemberManager;
import tw.tw360.tccpay.service.TccpayDriverManager;
import tw.tw360.tccpay.service.TccpayPaymentManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.ExcelReader;
import tw.tw360.web.util.ReportExcel;
import tw.tw360.web.util.tccpay.EnumUtils;
import tw.tw360.web.util.tccpay.TccpayConfiguration;
import tw.tw360.web.util.tccpay.TccpayPaymentPayStatus;

public class TccpaypaymentAction extends CRUDActionSupport<PaymentForView> {
	private static final long serialVersionUID = 1L;
	private final static String TAG = TccpaypaymentAction.class.getName();
	private static final Logger LOG = LoggerFactory.getLogger(TccpaypaymentAction.class);

	private final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	private static final String GASH_IN_REQ_KEY = TccpayConfiguration.getInstance().getProperty("GASH_IN_REQ_KEY");
	private static final String GASH_IN_REQ_URL = TccpayConfiguration.getInstance().getProperty("GASH_IN_REQ_URL");
	private static final String PAY_8888_API_URL = TccpayConfiguration.getInstance().getProperty("pay.8888.api.url");
	private static final String PAY_8888_APP_KEY = TccpayConfiguration.getInstance().getProperty("pay.8888.app.key");
	@Autowired
	private TccpayPaymentManager paymentManager;
	@Autowired
	private MemberManager memberManager;
	@Autowired
	private TccpayDriverManager driverManager;
	@Autowired
	private CustomerManager customerManager;

	private Page<PaymentForView> page = new Page<PaymentForView>(20);
	private ArrayList<String> payFrom = new ArrayList<String>();

	private static HashMap<Integer, Integer> mapPayStatus = new HashMap<Integer, Integer>();
	private static HashMap<Integer, Integer> mapGASHPayStatus = new HashMap<Integer, Integer>();

	static {
		// (0:待接單1:不接單 2:接單 3:已派遣 4:有收款 5:已發點 6:發點失敗 7:交易進行中取消 9:退單)
		mapPayStatus.put(TccpayPaymentPayStatus.待接單.getStatusCode(), EnumUtils.PayStatus.待確認訂單.ordinal());
		mapPayStatus.put(TccpayPaymentPayStatus.不接單.getStatusCode(), EnumUtils.PayStatus.支付失敗.ordinal());
		mapPayStatus.put(TccpayPaymentPayStatus.接單.getStatusCode(), EnumUtils.PayStatus.支付待確認.ordinal());
		mapPayStatus.put(TccpayPaymentPayStatus.已派遣.getStatusCode(), EnumUtils.PayStatus.待請款.ordinal());
		mapPayStatus.put(TccpayPaymentPayStatus.有收款.getStatusCode(), EnumUtils.PayStatus.待發送鑽石.ordinal());
		// mapPayStatus.put(5, EnumUtils.PayStatus.待發送鑽石.ordinal()); 5不傳update
		// payApi的訊息
		mapPayStatus.put(TccpayPaymentPayStatus.發點失敗.getStatusCode(), EnumUtils.PayStatus.發送鑽石失敗.ordinal());
		mapPayStatus.put(TccpayPaymentPayStatus.交易進行中取消.getStatusCode(), EnumUtils.PayStatus.支付失敗.ordinal());
		// mapPayStatus.put(TccpayPaymentPayStatus.退單.getStatusCode(),
		// EnumUtils.PayStatus.支付失敗.ordinal());

		mapGASHPayStatus.put(1, 1);
		mapGASHPayStatus.put(2, 2);
		mapGASHPayStatus.put(3, 3);
		mapGASHPayStatus.put(4, 4);
	}

	public Page<PaymentForView> getPage() {
		return page;
	}

	public void setPage(Page<PaymentForView> page) {
		this.page = page;
	}

	@Override
	public PaymentForView getModel() {
		return null;
	}

	@Override
	public String delete() throws Exception {
		return null;
	}

	public void queryPayment() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "queryPayment", request);
		HttpServletResponse response = Struts2Utils.getResponse();
		response.setHeader("Access-Control-Allow-Origin", "*");
		// "http://203.69.240.196:8060");
		int count = paymentManager.findUnHandlePayment();
		try {
			PrintWriter out = response.getWriter();
			JSONObject result = new JSONObject();
			result.put("result", count);
			out.print(result);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void authenticate() {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "authenticate", request);
		HttpServletResponse response = Struts2Utils.getResponse();
		String uid = map.get("uid");
		String sPayFrom = map.get("payFrom");
		String r = "";
		Member member = memberManager.findOne(uid, sPayFrom);
		if (member == null) {
			r = "not find";
		} else {
			r = "" + (memberManager.authenticate(uid, sPayFrom) > 0);
		}
		try {
			PrintWriter out = response.getWriter();
			JSONObject result = new JSONObject();
			result.put("result", r);
			out.print(result);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cancelPayStatus() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "cancelPayStatus", request);
		HttpServletResponse response = Struts2Utils.getResponse();

		long id = Long.parseLong(map.get("id"));
		int nowPayStatus = Integer.parseInt(map.get("nowPayStatus"));
		int newPayStatus = Integer.parseInt(map.get("newPayStatus"));
		PaymentForView paymentForView = paymentManager.findOneById(id);
		JSONObject result = new JSONObject();
		Customer customer = customerManager.findByPayFrom(paymentForView.getPayFrom());

		if (nowPayStatus != paymentForView.getPayStatus()) {
			result.put("message", "狀態與DB之紀錄狀態有異");
			returnJson(response, result);
			return;
		}
		/*
		 * if (nowPayStatus == TccpayPaymentPayStatus.已派遣.getStatusCode()) {
		 * driverManager.updateNonRestore(paymentForView.getCarId(), (-1)
		 * paymentForView.getMoney()); }
		 */
		int upd = 0;
		if (mappingPayApiPayStatus(paymentForView.getPayOrderId(), newPayStatus, paymentForView.getUserMobile(), sdf.format(new Date()),
				paymentForView.getPayFrom(), customer.getSendPoint(), paymentForView.getOrderId())) {
			upd = paymentManager.updatePayStatus(id, newPayStatus);
		} else if (newPayStatus == TccpayPaymentPayStatus.已發點.getStatusCode() && customer.getSendPoint() != 1) {
			result.put("message", paymentForView.getPayFrom() + "無需發點");
		} else {
			result.put("message", "修改pay8888的狀態失敗");
		}
		try {
			PrintWriter out = response.getWriter();
			result.put("result", upd > 0);
			out.print(result);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updatePayStatus() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "updatePayStatus", request);
		HttpServletResponse response = Struts2Utils.getResponse();
		JSONObject result = new JSONObject();
		long id = Long.parseLong(map.get("id"));
		int nowPayStatus = Integer.parseInt(map.get("nowPayStatus"));

		PaymentForView paymentForView = paymentManager.findOneById(id);
		Customer customer = customerManager.findByPayFrom(paymentForView.getPayFrom());
		if (paymentForView.getMemberStatus() != 1) {
			result.put("message", "用戶未認證");
			returnJson(response, result);
			return;
		}
		if (nowPayStatus != paymentForView.getPayStatus()) {
			result.put("message", "狀態與DB之紀錄狀態有異");
			returnJson(response, result);
			return;
		}

		int newPayStatus = TccpayPaymentPayStatus.待接單.getStatusCode();
		// Map<String, String> apiReturn = new HashMap<String, String>();

		if (nowPayStatus == TccpayPaymentPayStatus.發點失敗.getStatusCode() || nowPayStatus == TccpayPaymentPayStatus.有收款.getStatusCode()) {
			newPayStatus = TccpayPaymentPayStatus.已發點.getStatusCode();
			if (StringUtils.isNotBlank(paymentForView.getCarId()) && (paymentForView.getRestoreStatus() != 2)) {
				driverManager.addNonRestore(paymentForView.getCarId(), -paymentForView.getMoney());
			}
			if (paymentForView.getRestoreStatus() != 2) {
				paymentManager.updateRestore(new String[] { paymentForView.getOrderId() }, new Date(), 2);
			}
			// driverManager.addNonRestore(id, nonRestore);
			// if (StringUtils.isBlank(paymentForView.getCarId())
			// || (paymentForView.getRestoreStatus() == 2)) {
			// newPayStatus = 5;
			// } else {
			// result.put("message", "carId:" + paymentForView.getCarId()
			// + "未回存");
			// returnJson(response, result);
			// return;
			// }
		} else if (nowPayStatus == 0) {
			newPayStatus = TccpayPaymentPayStatus.接單.getStatusCode();
		}
		// else if (nowPayStatus == TccpayPaymentPayStatus.接單.getStatusCode()
		// && map.get("carId") != null && !map.get("carId").equals("")) {
		// if (gashInReq(paymentForView, map.get("carId"),
		// paymentForView.getMoney())) {
		// newPayStatus = TccpayPaymentPayStatus.已派遣.getStatusCode();
		// String updMember = checkAndModifyMember(map.get("carId"),
		// paymentForView.getMoney(), paymentForView);
		// if (!updMember.equals("修改成功")) {
		// result.put("message", updMember);
		// returnJson(response, result);
		// return;
		// }
		// } else {
		// result.put("message", "派遣失敗");
		// returnJson(response, result);
		// return;
		// }
		// }
		else if (nowPayStatus == TccpayPaymentPayStatus.接單.getStatusCode()) {
			newPayStatus = TccpayPaymentPayStatus.已派遣.getStatusCode();
		} else if (nowPayStatus == TccpayPaymentPayStatus.已派遣.getStatusCode()) {
			newPayStatus = TccpayPaymentPayStatus.有收款.getStatusCode();
		}

		if (!mappingPayApiPayStatus(paymentForView.getPayOrderId(), newPayStatus, paymentForView.getUserMobile(), sdf.format(new Date()),
				paymentForView.getPayFrom(), customer.getSendPoint(), paymentForView.getOrderId())) {
			if (newPayStatus == TccpayPaymentPayStatus.已發點.getStatusCode()) {
				newPayStatus = TccpayPaymentPayStatus.發點失敗.getStatusCode();
				result.put("message", "發點失敗");
			} else if (newPayStatus == TccpayPaymentPayStatus.已發點.getStatusCode() && customer.getSendPoint() != 1) {
				result.put("message", paymentForView.getPayFrom() + "無需發點");
			} else {
				result.put("message", "修改pay8888的狀態失敗");
				returnJson(response, result);
				return;
			}
		}
		paymentManager.updatePayStatus(id, nowPayStatus, newPayStatus);
		result.put("newPayStatus", newPayStatus);
		returnJson(response, result);
		return;
	}

	private void returnJson(HttpServletResponse response, JSONObject result) {
		try {
			PrintWriter out = response.getWriter();
			out.print(result);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 沒有使用
	 * 
	 * @param carId
	 * @param money
	 * @param paymentForView
	 * @return
	 * @throws Exception
	 */
	private String checkAndModifyMember(String carId, int money, PaymentForView paymentForView) throws Exception {
		Driver driver = driverManager.findBycarId(carId);
		if (driver == null) {
			return "車輛隊編不存在";
		}

		boolean result = driverManager.updateNonRestore(carId, money);
		if (result) {
			paymentManager.updateCarIdDerverID(paymentForView.getId(), carId, driver.getId(), driver.getIsTester());
			return "修改成功";
		} else {
			return "司機的額度不足";
		}
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		payFrom.clear();
		payFrom.addAll(customerManager.queryAllPayFrom());
		request.setAttribute("payFromList", payFrom);
		if (map.get("payOrderId") != null) {
			String payStatus, payOrderId, carId, sPayFrom, invoiceNo, channel;
			int restoreStatus, isTest, invoiceType;
			Timestamp startCreateTime, endCreateTime;

			sPayFrom = map.get("payFrom");
			payOrderId = map.get("payOrderId");
			payStatus = map.get("payStatus");
			restoreStatus = parseInt(map.get("restoreStatus"));
			carId = map.get("channel");
			startCreateTime = parseTimestamp(map.get("startCreateTime") + " 00:00:00");
			endCreateTime = parseTimestamp(map.get("endCreateTime") + " 23:59:59");
			isTest = parseInt(map.get("isTest"));

			invoiceType = parseInt(map.get("invoiceType"));
			invoiceNo = map.get("invoiceNo");
			channel = map.get("channel");

			List<PaymentForView> result = paymentManager.findByViewCondition(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime,
					isTest, sPayFrom, page.getPageNo(), page.getPageSize(), invoiceType, invoiceNo, channel);
			int totalCount = paymentManager.findByViewConditionCount(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest,
					sPayFrom, invoiceType, invoiceNo, channel);

			page.setResult(result);
			page.setTotalCount(totalCount);
			request.setAttribute("map", map);

			Double twdPriceSum = paymentManager.querySumForPayment(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest,
					sPayFrom, invoiceType, invoiceNo, channel, "TWD");
			LOG.info("list twdPriceSum={},", twdPriceSum);
			request.setAttribute("twdPriceSum", twdPriceSum == null ? 0.0 : twdPriceSum);

			Double hkdPriceSum = paymentManager.querySumForPayment(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest,
					sPayFrom, invoiceType, invoiceNo, channel, "HKD");
			LOG.info("list hkdPriceSum={},", hkdPriceSum);
			request.setAttribute("hkdPriceSum", hkdPriceSum == null ? 0.0 : hkdPriceSum);
		}
		return "list";
	}

	public String listGash() throws Exception {
		String result = "listGash";
		listThrid(result);
		return result;
	}

	public String listTcc() throws Exception {
		String result = "listTcc";
		listThrid(result);
		return result;
	}

	public String listCM() throws Exception {
		String result = "listCM";
		listThrid(result);
		return result;
	}

	public String listP2G() throws Exception {
		String result = "listP2G";
		listThrid(result);
		return result;
	}

	public String listBonushunter() throws Exception {
		String result = "listBonushunter";
		listThrid(result);
		return result;
	}

	public String listRC() throws Exception {
		String result = "listRC";
		listThrid(result);
		return result;
	}

	public String listSnsplus() throws Exception {
		String result = "listSnsplus";
		listThrid(result);
		return result;
	}

	public String listTomato() throws Exception {
		String result = "listTomato";
		listThrid(result);
		return result;
	}

	public String listLz() throws Exception {
		String result = "listLz";
		listThrid(result);
		return result;
	}

	public String listBoyaa() throws Exception {
		String result = "listBoyaa";
		listThrid(result);
		return result;
	}

	public void listThrid(String methodName) throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, methodName, request);
		payFrom.clear();
		payFrom.addAll(customerManager.queryAllPayFrom());
		request.setAttribute("payFromList", payFrom);
		if (map.get("payOrderId") != null) {
			String payStatus, payOrderId, carId, sPayFrom, invoiceNo, channel;
			int restoreStatus, isTest, invoiceType;
			Timestamp startCreateTime, endCreateTime;

			sPayFrom = map.get("payFrom");
			payOrderId = map.get("payOrderId");
			payStatus = map.get("payStatus");
			restoreStatus = parseInt(map.get("restoreStatus"));
			carId = map.get("carId");
			startCreateTime = parseTimestamp(map.get("startCreateTime") + " 00:00:00");
			endCreateTime = parseTimestamp(map.get("endCreateTime") + " 23:59:59");
			isTest = parseInt(map.get("isTest"));

			invoiceType = parseInt(map.get("invoiceType"));
			invoiceNo = map.get("invoiceNo");
			channel = map.get("channel");
			List<PaymentForView> result = paymentManager.findByViewCondition(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime,
					isTest, sPayFrom, page.getPageNo(), page.getPageSize(), invoiceType, invoiceNo, channel);
			int totalCount = paymentManager.findByViewConditionCount(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest,
					sPayFrom, invoiceType, invoiceNo, channel);

			page.setResult(result);
			page.setTotalCount(totalCount);
			request.setAttribute("map", map);
		}
	}

	public String edit() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "edit", request);

		if (map.get("id") != null) {
			long id = Long.parseLong(map.get("id"));

			PaymentForView paymentForView = paymentManager.findOneById(id);
			if (paymentForView != null) {
				Customer customer = customerManager.findByPayFrom(paymentForView.getPayFrom());
				request.setAttribute("paymentForView", paymentForView);
				request.setAttribute("sendPoint", customer.getSendPoint());
			}
			request.setAttribute("map", map);
		}

		return "edit";

	}

	private Timestamp parseTimestamp(String string) throws ParseException {
		if (StringUtils.isNotBlank(string) && string.length() > 9) {
			return new Timestamp(sdf.parse(string).getTime());
		} else {
			return null;
		}
	}

	private int parseInt(String string) {
		if (StringUtils.isNotBlank(string)) {
			return Integer.parseInt(string);
		} else {
			return -1;
		}
	}

	@Override
	protected void prepareModel() throws Exception {

	}

	public File getExcelFile() {
		return excelFile;
	}

	public void setExcelFile(File excelFile) {
		this.excelFile = excelFile;
	}

	public String getExcelFileName() {
		return excelFileName;
	}

	public void setExcelFileName(String excelFileName) {
		this.excelFileName = excelFileName;
	}

	private File excelFile;

	private String excelFileName;

	@Autowired
	@Qualifier("tccpayInvoiceManager")
	private InvoiceManager invoiceService;

	public String invoice() throws Exception {
		return "invoice";
	}

	public String saveInvoice() throws FileNotFoundException {
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "saveInvoice", request);
		if (excelFile != null && excelFileName != null) {
			ExcelReader er = new ExcelReader();
			FileInputStream fis = new FileInputStream(excelFile);
			Map<Integer, String> data = er.readExcelContent(fis);
			Map<String, Object> result = invoiceService.saveInvoice(data);
			for (Entry<String, Object> entry : result.entrySet()) {
				request.setAttribute(entry.getKey(), entry.getValue());
			}
		}
		return "saveInvoice";
	}

	@Override
	public String save() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "save", request);

		if (map.get("id") != null) {
			long id = Long.parseLong(map.get("id"));
			String remark = map.get("remark");
			String name = map.get("name");
			String mobile = map.get("mobile");
			String address = map.get("address");
			String isTest = map.get("isTest");
			String channel = map.get("channel");
			String colAddr = map.get("colAddr");
			String restoreStatus = map.get("restoreStatus");
			String invoiceNo = map.get("invoiceNo");
			String invoiceType = map.get("invoiceType");

			String invoiceName = map.get("invoiceName");
			String invoicePhone = map.get("invoicePhone");
			String invoiceAddress = map.get("invoiceAddress");
			String invoiceTitle = map.get("invoiceTitle");
			String taxId = map.get("taxId");

			PaymentForView paymentForView = paymentManager.findOneById(id);
			paymentManager.updateById(id, remark, name, mobile, address, isTest, channel, restoreStatus);
			memberManager.updateById(paymentForView.getUid(), name, mobile, address);
			invoiceService.saveInvoice(paymentForView.getOrderId(), colAddr, invoiceNo, invoiceType, invoiceName, invoicePhone, invoiceAddress, invoiceTitle,
					taxId);
			paymentForView = paymentManager.findOneById(id);

			map.remove("invoiceNo");
			request.setAttribute("map", map);
			request.setAttribute("paymentForView", paymentForView);

		}

		// return "list";
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath() + "/tccpaypayment/tccpaypayment!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean payFromPayStatus(String orderId, int status, String statusTime, String payFrom) throws IOException, InterruptedException {
		HashMap<String, Object> properties = new HashMap<String, Object>(1);
		Customer customer = customerManager.findByPayFrom(payFrom);
		properties.put("orderId", orderId);
		Integer istatus = mapGASHPayStatus.get(status);
		if (istatus == null) {
			return true;
		}
		properties.put("status", istatus);
		properties.put("statusTime", statusTime);
		properties.put("token", DigestUtils.md5Hex(orderId + status + statusTime + customer.getKey()));
		if (StringUtils.isNotBlank(customer.getCallbackUrl())) {
			for (int i = 0; i < 3; i++) {
				boolean result = postRequest(customer.getCallbackUrl(), properties);
				if (result) {
					return true;
				}
				Thread.sleep(5000);
			}
			return false;
		} else {
			return true;
		}
	}

	private boolean mappingPayApiPayStatus(String payOrderId, int payStatus, String mobile, String statusTime, String payFrom, int sendPoint, String efunOrderId)
			throws IOException, InterruptedException {
		if (payStatus == TccpayPaymentPayStatus.已發點.getStatusCode()) {
			if (sendPoint == 1) {
				return exchangeFor8888PayApi(payOrderId);
			} else {
				return false;
			}
		}
		if (payFrom.equalsIgnoreCase("evatar")) {
			if (mapPayStatus.get(payStatus) == null) {
				return true;
			}
			return update8888PayApiPayStatus(payOrderId, mapPayStatus.get(payStatus));
		} else {
			return payFromPayStatus(efunOrderId, payStatus, statusTime, payFrom);
		}
	}

	private boolean update8888PayApiPayStatus(String efunOrderId, int status) throws IOException {
		System.out.println("============== update PayApiPayStatus  efunOrderId = " + efunOrderId + ", status=" + status + " ====================");
		String url = MessageFormat.format(PAY_8888_API_URL, "changeStatus");

		HashMap<String, Object> properties = new HashMap<String, Object>(1);
		properties.put("efunOrderId", efunOrderId);
		properties.put("status", status);

		return postRequest(url, properties);
	}

	private boolean exchangeFor8888PayApi(String efunOrderId) throws IOException {
		System.out.println("============== do exchangeForPayApi  ====================");
		String url = MessageFormat.format(PAY_8888_API_URL, "exchange");
		String sign = DigestUtils.md5Hex(efunOrderId + PAY_8888_APP_KEY);

		HashMap<String, Object> properties = new HashMap<String, Object>(1);
		properties.put("efunOrderId", efunOrderId);
		properties.put("sign", sign);
		return postRequest(url, properties);
	}

	private boolean gashInReq(PaymentForView pay, String carId, int price) throws IOException { // price
																								// 是payment.money
		System.out.println("============== Sms SmsApi gashInReq ====================");
		System.out.println("payment=" + pay);
		System.out.println("carId=" + carId);
		System.out.println("price=" + price);
		if (pay == null) {
			return false;
		}
		String keyin_dt = sdf1.format(pay.getCreateTime());
		String grant_dt = sdf1.format(new Date()); // 填系統時間
		String token = genToken(pay.getOrderId() + carId);
		int amt = (int) ((1 * 4 * price) / 100);
		String xml = MessageFormat.format(getTempXml(), pay.getOrderId(), carId, keyin_dt, grant_dt, "" + price, "" + amt, token);

		System.out.println("xml=" + xml);
		HashMap<String, Object> properties = new HashMap<String, Object>(1);
		properties.put("xml", xml);
		return postRequest(GASH_IN_REQ_URL, properties);
		// postRequest(URL,xml);

	}

	private String genToken(String token) {
		return MD5Util.crypt(token + GASH_IN_REQ_KEY);
	}

	private String getTempXml() {
		StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"utf-8\" ?><gashInReq>");
		xml.append(TccpayConfiguration.getInstance().getProperty("api.gashInReq.xml"));
		xml.append("</Gash></gashInReq>");
		return xml.toString();
	}

	private boolean postRequest(String url, HashMap<String, Object> properties) throws IOException {
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		for (String key : properties.keySet()) {
			String val = properties.get(key) == null ? "" : properties.get(key).toString();
			urlParameters.add(new BasicNameValuePair(key, val));
		}
		HttpClientBuilder builder = HttpClientBuilder.create();
		CloseableHttpClient client = builder.build();
		HttpPost post = new HttpPost(url);
		// post.setHeader("Content-Type", "application/xml");
		// post.setEntity(new StringEntity(xml, "UTF-8"));
		post.setEntity(new UrlEncodedFormEntity(urlParameters, "UTF-8"));
		HttpResponse response = client.execute(post);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println("Return result : " + result);
		client.close();

		// return result.toString();
		return (result.toString().indexOf("0000") > -1);
	}

	/*
	 * function : 匯出excel description : 傳入搜尋條件，將結果寫入excel
	 */
	public void export() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "export", request);
		if (map.get("payOrderId") != null) {
			String payStatus, payOrderId, carId, payFrom = null, invoiceNo, channel, sPayFrom;
			int restoreStatus, isTest, invoiceType;
			Timestamp startCreateTime, endCreateTime;

			sPayFrom = map.get("payFrom");
			payOrderId = map.get("payOrderId");
			payStatus = map.get("payStatus");
			restoreStatus = parseInt(map.get("restoreStatus"));
			carId = map.get("carId");
			startCreateTime = parseTimestamp(map.get("startCreateTime") + " 00:00:00");
			endCreateTime = parseTimestamp(map.get("endCreateTime") + " 23:59:59");
			isTest = parseInt(map.get("isTest"));

			invoiceType = parseInt(map.get("invoiceType"));
			invoiceNo = map.get("invoiceNo");
			channel = map.get("channel");

			List<PaymentForView> payments = paymentManager.queryAll(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest,
					payFrom, invoiceType, invoiceNo, channel, sPayFrom);

			String[] dataTitles = { "ID", "傾國訂單號", "金流方訂單號", "金流方", "金流方用戶ID", "遊戲名稱", "渠道", "儲值金額", "幣種", "玩家姓名", "用戶手機", "用戶認證狀態", "用戶地址", "收款狀況", "回存狀態",
					"訂單時間", "發票種類", "發票號碼", "是否測試", "備註" };
			List<Object[]> list = new ArrayList<Object[]>();
			if (payments != null && !payments.isEmpty()) {
				for (PaymentForView payment : payments) {
					Object[] o = new Object[dataTitles.length];
					o[0] = payment.getId();
					o[1] = payment.getOrderId();
					o[2] = payment.getPayOrderId();
					o[3] = payment.getPayFrom();
					o[4] = payment.getUid();

					o[5] = payment.getGameName();
					channel = "";
					if ("1".equals(payment.getChannel())) {
						channel = "大車隊";
					}
					if ("0".equals(payment.getChannel())) {
						channel = "牛番茄";
					}
					o[6] = channel;
					o[7] = payment.getMoney();
					o[8] = payment.getCurrency();
					o[9] = payment.getName();
					o[10] = payment.getUserMobile();

					o[11] = 1 == payment.getMemberStatus() ? "認證" : "未認證";
					o[12] = payment.getUserAddress();
					String tempStr = "";
					switch (payment.getPayStatus()) {
					case 0:
						tempStr = "待接單";
						break;
					case 1:
						tempStr = "取消";
						break;
					case 2:
						tempStr = "接單";
						break;
					case 3:
						tempStr = "已派遣";
						break;
					case 4:
						tempStr = "已收款";
						break;
					case 5:
						tempStr = "已發點";
						break;
					case 6:
						tempStr = "發點失敗";
						break;
					case 7:
						tempStr = "交易進行中取消";
						break;
					case 9:
						tempStr = "退單";
						break;
					}
					o[13] = tempStr;
					o[14] = 2 == payment.getRestoreStatus() ? "已回存" : "未回存";
					o[15] = payment.getCreateTime();

					String tmpInvoiceType = "";
					switch (payment.getInvoiceType()) {
					case 0:
						tmpInvoiceType = "捐贈";
						break;
					case 2:
						tmpInvoiceType = "二聯";
						break;
					case 3:
						tmpInvoiceType = "三聯";
						break;
					}
					o[16] = tmpInvoiceType;
					o[17] = payment.getInvoiceNo();
					o[18] = 1 == payment.getIsTest() ? "是" : "";
					o[19] = payment.getRemark();
					list.add(o);
				}
			}
			try {
				// reportExcel.reportExcel(request, response,
				// "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
				ReportExcel.reportExcel(request, response, "購買紀錄列表" + System.currentTimeMillis(), "", dataTitles, list);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}/*
	 * function : 匯出excel description : 傳入搜尋條件，將結果寫入excel
	 */

	public void exportThird() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "export", request);
		if (map.get("payOrderId") != null) {
			String payStatus, payOrderId, carId, payFrom, invoiceNo, channel, sPayFrom;
			int restoreStatus, isTest, invoiceType;
			Timestamp startCreateTime, endCreateTime;

			sPayFrom = map.get("payFrom");
			payOrderId = map.get("payOrderId");
			payStatus = map.get("payStatus");
			restoreStatus = parseInt(map.get("restoreStatus"));
			carId = map.get("carId");
			startCreateTime = parseTimestamp(map.get("startCreateTime") + " 00:00:00");
			endCreateTime = parseTimestamp(map.get("endCreateTime") + " 23:59:59");
			isTest = parseInt(map.get("isTest"));
			payFrom = map.get("payFrom");

			invoiceType = parseInt(map.get("invoiceType"));
			invoiceNo = map.get("invoiceNo");
			channel = map.get("channel");

			List<PaymentForView> payments = paymentManager.queryAll(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest,
					payFrom, invoiceType, invoiceNo, channel, sPayFrom);
			String[] dataTitles = { "傾國訂單號", "GASH訂單編號", "儲值金額", "幣別", "玩家姓名", "用戶地址", "訂單狀態", "訂單時間", "是否測試" };
			List<Object[]> list = new ArrayList<Object[]>();
			if (payments != null && !payments.isEmpty()) {
				for (PaymentForView payment : payments) {
					Object[] o = new Object[dataTitles.length];
					o[0] = payment.getOrderId();
					o[1] = payment.getPayOrderId();
					o[2] = payment.getMoney();
					o[3] = payment.getCurrency();
					o[4] = payment.getName();
					o[5] = payment.getUserAddress();
					String tempStr = "";
					switch (payment.getPayStatus()) {
					case 0:
						tempStr = "待接單";
						break;
					case 1:
						tempStr = "取消";
						break;
					case 2:
						tempStr = "接單";
						break;
					case 3:
						tempStr = "已派遣";
						break;
					case 4:
						tempStr = "已收款";
						break;
					case 5:
						tempStr = "已發點";
						break;
					case 6:
						tempStr = "發點失敗";
						break;
					case 7:
						tempStr = "交易進行中取消";
						break;
					case 9:
						tempStr = "退單";
						break;
					}
					o[6] = tempStr;
					o[7] = payment.getCreateTime();
					o[8] = 1 == payment.getIsTest() ? "是" : "";
					list.add(o);
				}
			}
			try {
				// reportExcel.reportExcel(request, response,
				// "儲值列表"+System.currentTimeMillis(), "儲值列表", dataTitles, list);
				ReportExcel.reportExcel(request, response, "購買紀錄列表" + System.currentTimeMillis(), "", dataTitles, list);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
