package tw.tw360.web.position;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Authorities;
import tw.tw360.dto.Position;
import tw.tw360.dto.PositionAuthority;
import tw.tw360.service.AuthoritiesManager;
import tw.tw360.service.PositionAuthorityManager;
import tw.tw360.service.PositionManager;
import tw.tw360.util.PrintUtil;


public class PositionAction extends CRUDActionSupport<Position>{
	
	private static final long serialVersionUID = 46383765710223339L;
	private static final Logger LOG = LoggerFactory.getLogger(PositionAction.class);
	private static final String TAG="PositionAction";
	@Autowired
	private AuthoritiesManager authoritiesManager;
	@Autowired
	private PositionManager positionManager;
	@Autowired
	private PositionAuthorityManager  positionAuthorityManager;
	

	public Position getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {//查詢所有角色
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "list", request);
		List<Position> list = positionManager.queryAllPositionAndauthorities();
		
        request.setAttribute("list", list);
		
		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public String queryPositionAuthInfo(){
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "queryPositionAuthInfo", request);
		Long id = new Long(request.getParameter("id"));
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(null, "queryPositionAuthInfo", Struts2Utils.getRequest());
		String name = map.get("rolesName");

		List<Authorities> parentList = authoritiesManager.queryParentAuthorities();
		List<Authorities> childList = authoritiesManager.queryChildAuthorities();
		request.setAttribute("parentList", parentList);
		request.setAttribute("childList", childList);
		
		List<PositionAuthority> pa = positionAuthorityManager.queryPositionAuthorities(id);
	    
	    request.setAttribute("pa", pa);
		request.setAttribute("name", name);
		request.setAttribute("id", id);
		
		return "input";
	}
	
	public void updatePositionAuthInfo(){
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "updatePositionAuthInfo", request);
		Long id =  new Long(request.getParameter("id"));
		String[] authorityIds = request.getParameterValues("authority");
		positionAuthorityManager.updatePositionAuthInfo(id, authorityIds);
		
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath()+ "/position/position!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String addPosition(){//到添加角色頁面
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "addPosition", request);

		List<Authorities> parentList = authoritiesManager.queryParentAuthorities();
		List<Authorities> childList = authoritiesManager.queryChildAuthorities();
		request.setAttribute("parentList", parentList);
		request.setAttribute("childList", childList);
		return "add";
	}
	
	public void createPosition(){//創建角色並賦權限
		HttpServletRequest request = Struts2Utils.getRequest();
		String name = request.getParameter("positionName");
		String[] authorityIds = request.getParameterValues("authority");
		positionManager.createPosition(name, authorityIds);
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath()+ "/position/position!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updatePositionStatus(){//刪除角色
		HttpServletRequest request = Struts2Utils.getRequest();
		 HttpServletResponse response = Struts2Utils.getResponse();
		int id = new Integer(request.getParameter("id"));
		int status = new Integer(request.getParameter("status"));
		int row = positionManager.updatePositionStatus(id, status);
		try {
			PrintWriter out=response.getWriter();
			if(row>0){
				out.print(status);
			}else{
				out.print(0);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
