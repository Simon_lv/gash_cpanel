package tw.tw360.web.banner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import tw.tw360.dto.Banner;
import tw.tw360.service.BannerManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.Configuration;

public class BannerAction extends CRUDActionSupport<Banner>{
	
	private static final String TAG = "BannerAction";
	private static final String BANNER_IMAGES_PATH = Configuration.getInstance().getProperty("base_on_servletCtx_banner_path");
	
	private static final String BANNER_IMAGES_URL = Configuration.getInstance().getProperty("url.images.banner");
	
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat sdf_img = new SimpleDateFormat("yyyyMMddHHmmss");
	private Page<Banner> page = new Page<Banner>(10);
	private File imgFile;
	private String imgFileName; 
	
	@Autowired
	private BannerManager bannerManager;
	
	public Page<Banner> getPage() {
		return page;
	}

	public void setPage(Page<Banner> page) {
		this.page = page;
	}		

	public File getImgFile() {
		return imgFile;
	}

	public void setImgFile(File imgFile) {
		this.imgFile = imgFile;
	}
	
	public String getImgFileName() {
		return imgFileName;
	}

	public void setImgFileName(String imgFileName) {
		this.imgFileName = imgFileName;
	}

	public Banner getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "list", request);
		
		page = bannerManager.findAll(page, map);
		request.setAttribute("map", map);

		return "list";
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void operation() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "operation", request);
		
		if(StringUtils.isNotBlank(map.get("type")) && StringUtils.isNotBlank(map.get("id"))){
			int type = Integer.parseInt(map.get("type"));
			long id = Long.parseLong(map.get("id"));
			switch(type){
				case 1:
					bannerManager.updateToTop(id, 1);	//目前畫面暫無置頂
					break;
				case 2://上移
				case 3://下移
					Banner banner = bannerManager.loadById(id);
					Banner banner_offset = bannerManager.queryOffsetRecord(id, type>2 ?1 :-1);
					if(banner!=null && banner_offset!=null){
						bannerManager.updateOrderIndex(id, banner_offset.getOrderIndex());
						bannerManager.updateOrderIndex(banner_offset.getId(), banner.getOrderIndex());	
					}else if(type==2 && banner_offset==null && banner.getOrderIndex()>1){
						bannerManager.updateOrderIndex(id, banner.getOrderIndex()-1);
					}else if(type==3 && banner_offset==null){
						bannerManager.updateOrderIndex(id, banner.getOrderIndex()+1);
					}
					break;
				case 4:
					bannerManager.updateStatus(id, 2);
					break;										
			}
		}
		
		try {
			PrintWriter out=response.getWriter();
			out.print(map.get("id"));
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String toAdd() throws Exception {
		return "add";
	}
	

	
	public void addBanner() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "addBanner", request);
		
		Banner banner = new Banner();
		if(StringUtils.isNotBlank(getImgFileName())) {
			banner.setBannerUrl(fetchBannerUrl(request));
		}
		banner.setStatus(1);
		bannerManager.add(banner);

		try {
			response.sendRedirect(request.getContextPath()+ "/banner/banner!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	
	private String fetchBannerUrl(HttpServletRequest request){	
		int idx = getImgFileName().lastIndexOf("/")>0 ?getImgFileName().lastIndexOf("/"): getImgFileName().lastIndexOf("\\")+1;
		StringBuffer imageName = new StringBuffer();
		imageName.append(sdf_img.format(new Date())).append("_").append(getImgFileName().substring(idx));
		
		File dirPath =new File(request.getSession().getServletContext().getRealPath("/")+ BANNER_IMAGES_PATH);
		if(!dirPath.exists()){
			dirPath.mkdirs();
		}
		
		StringBuffer newPath = new StringBuffer(); 
		newPath.append(dirPath).append("/").append(imageName.toString());   
		try {
			copy(imgFile, newPath.toString());
		} catch (Exception e1) {
			e1.printStackTrace();
		}   
   
//        String url = "http://" + request.getServerName() + ":"+ request.getServerPort() + request.getContextPath() 
//        		+ BANNER_IMAGES_PATH + imageName.toString();
		String url = MessageFormat.format(BANNER_IMAGES_URL,imageName.toString());
        PrintUtil.outputContent("fetchBannerUrl:"+url);
        return url;
	}
		
	private void copy(File upload, String newPath) throws Exception {   
        FileOutputStream fos = new FileOutputStream(newPath);   
        FileInputStream fis = new FileInputStream(upload);   
        byte[] buffer = new byte[1024];   
        int len = 0;   
        while ((len = fis.read(buffer)) > 0) {   
            fos.write(buffer, 0, len);   
        }   
        fos.close();   
        fis.close();   
    } 

}
