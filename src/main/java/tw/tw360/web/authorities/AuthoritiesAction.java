package tw.tw360.web.authorities;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.userdetails.UserDetails;
import org.springside.modules.security.springsecurity.SpringSecurityUtils;
import org.springside.modules.web.struts2.CRUDActionSupport;
import org.springside.modules.web.struts2.Struts2Utils;

import com.sun.tools.jdi.LinkedHashMap;

import tw.tw360.dto.Authorities;
import tw.tw360.dto.Position;
import tw.tw360.service.AuthoritiesManager;
import tw.tw360.util.PrintUtil;
import tw.tw360.web.util.SessionUtil;

@SuppressWarnings("serial")
public class AuthoritiesAction extends CRUDActionSupport<Authorities> {
	
	private static final String TAG = "AuthoritiesAction";
	private static final Logger LOG = LoggerFactory.getLogger(AuthoritiesAction.class);
	@Autowired
	private AuthoritiesManager authoritiesManager;
	
	@Override
	public Authorities getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String delete() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String list() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		
		PrintUtil.printAndReturnRequest(TAG, "list", request);
		Map<Long,List<Authorities>> authoritDisplayMap = new LinkedHashMap();
		List<Authorities> list = authoritiesManager.queryAllAuthorities();
		List<Authorities> listParent = authoritiesManager.queryParentAuthorities();
		if(listParent !=null && listParent.size() > 0) {
			if(list != null && list.size() > 0) {
				for(Authorities pa : listParent){
					ArrayList<Authorities> authoritiesInMapList = new ArrayList<Authorities>();
					for(Authorities a : list) {
						if(pa.getId().intValue() == a.getPid()) {
							authoritiesInMapList.add(a);
						}
					}
					authoritDisplayMap.put(pa.getId(), authoritiesInMapList);
				}
			}
		}
		
		request.setAttribute("listParent", listParent);
		request.setAttribute("authoritDisplayMap", authoritDisplayMap);
		return "list";
		
	}
	
	public String toAdd() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printRequest(TAG, "toAdd", request);
		List<Authorities> parentList = authoritiesManager.queryParentAuthorities();
		LOG.info("parentList size:{}",parentList.size());
		request.setAttribute("parentList", parentList);
		return "add";
	}
	
	public void add() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		PrintUtil.printAndReturnRequest(TAG, "add", request);
		Authorities authorities = new Authorities();
		authorities.setDisplayName(request.getParameter("displayName"));
		authorities.setName(request.getParameter("name"));
		if(StringUtils.isNotBlank(request.getParameter("pid"))) {
			authorities.setPid(Integer.parseInt(request.getParameter("pid")));
		}
		
		authoritiesManager.add(authorities);
		try {
			response.sendRedirect(request.getContextPath()+ "/authorities/authorities!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String edit() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "edit", request);
		long id = Long.parseLong(request.getParameter("id"));
		Authorities au = authoritiesManager.queryAuthoritiesById(id);
		request.setAttribute("au", au);
		
		List<Authorities> parentList = authoritiesManager.queryParentAuthorities();
		request.setAttribute("parentList", parentList);
		return "edit";
	}
	
	public void operation() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		HttpServletResponse response = Struts2Utils.getResponse();
		PrintUtil.printAndReturnRequest(TAG, "operation", request);
		HashMap<String, String> map = PrintUtil.printAndReturnRequest(TAG, "operation", request);
		
		if(StringUtils.isNotBlank(map.get("type")) && StringUtils.isNotBlank(map.get("id"))){
			int type = Integer.parseInt(map.get("type"));
			long id = Long.parseLong(map.get("id"));
			switch(type){
				case 4:
					authoritiesManager.updateStatus(id, 2);
					break;										
			}
		}
		
		try {
			PrintWriter out=response.getWriter();
			out.print(map.get("id"));
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void prepareModel() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String save() throws Exception {
		HttpServletRequest request = Struts2Utils.getRequest();
		PrintUtil.printAndReturnRequest(TAG, "save", request);
		String uri = request.getParameter("name");
		String pid = request.getParameter("pid");
		String displayName = request.getParameter("displayName");
		Authorities authorities = new Authorities();
		if(StringUtils.isNotBlank(uri)) {
			authorities.setName(uri.trim());
		}
		if(StringUtils.isNotBlank(displayName)){
			authorities.setDisplayName(displayName.trim());
		}
		
		if(StringUtils.isNotBlank(pid)){
			authorities.setPid(Integer.parseInt(pid));
		}
		authorities.setStatus(1);
		authoritiesManager.add(authorities);
		try {
			Struts2Utils.getResponse().sendRedirect(Struts2Utils.getRequest().getContextPath()+ "/authorities/authorities!list.action");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
